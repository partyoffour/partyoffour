<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Item</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>

    <?php
      include('session.php'); // include the rest's session
      include('configr.php'); // include the database

      if($_SERVER["REQUEST_METHOD"] == "POST"){

        if(empty($_POST['hours'])){
        header('location: rest-home.php');
        }
        if(empty($_POST['minutes'])){
          header('location: rest-home.php');
        }
        if(empty($_POST['partySize'])){
          header('location: rest-home.php');
        }
        
        $hours = mysqli_real_escape_string($dbr, $_POST['hours']);
        $minutes = mysqli_real_escape_string($dbr, $_POST['minutes']);
        $partySize = mysqli_real_escape_string($dbr, $_POST['partySize']);

        $url = "http://localhost:2017?hours=" . $hours . "&minutes=" . $minutes . "&partySize=" . $partySize;

        $response = file_get_contents($url);

        // $url2 = "location: "
        // header("rest-home.php");
      }

    ?>


	<body>

	<div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
            <li><a href="rest-home.php">Home</a></li>
            
            <li><a href="logout.php">Logout</a></li>
          </ul>
          <label>Welcome, Owner!</label>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="select-acct.php">My Accounts</a></li>
                    <li><a href="rest-home.php">Restaurant Home</a></li>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <li><a href="logout.php" type="button" class="button">Logout</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

	<!-- ************************ HERO ***************************************** -->

          <section class="hero">

            <div class="wrap">
              <h1>PHP Form/Callback Test</h1>
			</div>

		  </section>

		
      	<section class="main">
      		<div class="wrap">
      			<form action="" method="POST" name="myform">
      				<!-- <legend>Lets ta go, wahh!</legend> -->
      				<label> Hours (Until Arrival)
      				<input type="text" name="hours" />
      				</label>
      				<label> Minutes (Until Arrival)
      				<input type="text" name="minutes" />
      				</label>
      				<label> Party Size
      				<input type="text" name="partySize" />
      				</label>
      				<input type="submit" name="Send" class="button" />
      			</form>
      		</div>

          <?php
            if(isset($response)){
              echo $response;
            }
          ?>

      	</section>
		
		<?php
			// if(isset($_SESSION['callbackResult']) && !empty($_SESSION['callbackResult'])) {
			// 	echo "<p> Result from previous callback: " . $_SESSION['callbackResult'] .'</p>';
			// 	unset($_SESSION['callbackResult']); // unset the callback result
				
			// }
		?>

		<section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <a href="index.php">Home</a>
                <a href="about.php">Services</a>
                <a href="contact-us.php">Contact Us</a>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="#"><span>Email</span> example@gmail.com</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>

          </section>

	</body>
</html>