<?php

	// test callback using POST

	include('session.php'); // include the rest's session
	include('configr.php'); // include the database

	if($_SERVER["REQUEST_METHOD"] == "POST"){

		if(empty($_POST['hours'])){
			header('location: rest-home.php');
		}
		if(empty($_POST['minutes'])){
			header('location: rest-home.php');
		}
		if(empty($_POST['partySize'])){
			header('location: rest-home.php');
		}
		// validate the input, check type is int and the value is positive
		if(gettype(!is_numeric($_POST['hours'])) {
			header('location: rest-home.php');
		}
		if(gettype(!is_numeric($_POST['minutes'])) {
			header('location: rest-home.php');
		}
		if(gettype(!is_numeric($_POST['partySize'])) {
			header('location: rest-home.php');
		}

		// validate the input, sanitize for injections
		$hours = mysqli_real_escape_string($dbr, $_POST['hours']);
		$minutes = mysqli_real_escape_string($dbr, $_POST['minutes']);
		$partySize = mysqli_real_escape_string($dbr, $_POST['partySize']);

		

		$pyscript = '"C:\\rests\\C17Austin.McWhirter\\Desktop\\PartyOfFour\\Austin Test Code\\web_callback.py"';
		$python = 'C:\\Python34\\python.exe';
		$args = "$val1,$val2,$val3";
		$cmd = "$python $pyscript $args";
		exec("$cmd", $results);

		// echo ($results[0]);
		$_SESSION['callbackResult'] = $results; // pass the results back in the session

	} else {
		header('location: rest-home.php'); // head back to random page if not recieved from POST
	}

?>