<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Home</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>
    <script type="text/javascript">
    function validate(num) {
      // validates the input with regular expressions and JS.
      switch(num) {
        case 0:
          var re = /^([a-zA-Z])+$/
          if(!re.test(myform.fname.value)) {
            document.getElementById('fname-error').innerHTML="Please enter a valid first name.";
          } 
          else {
            document.getElementById("fname-error").innerHTML="";
          } break;
        case 1:
          var re = /^[a-zA-Z]+-?[a-zA-Z]+$/
          if(!re.test(myform.lname.value)) {
            document.getElementById('lname-error').innerHTML="Please enter a valid last name.";
          } else {
            document.getElementById("lname-error").innerHTML="";
          } break;
        case 2:
          var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          if( !re.test(myform.email.value)) {
            document.getElementById('email-error').innerHTML="Invalid email address.";
          }
          else {
            document.getElementById('email-error').innerHTML="";
          } break;
        default:
          break;
        }
      }
  </script>

  <?php
    include('session.php');
    include('config.php');
    
    if($_SERVER["REQUEST_METHOD"] == "POST") {
      $err = 0; $updates = 0;
      $username = $_SESSION['login_user'];
      // validate and update the first name value, if required
      if(isset($_POST['fname']) && !empty($_POST['fname'])) {
        if(preg_match("/^([a-zA-Z])+$/", $_POST['fname'])){
          $fname = mysqli_real_escape_string($db, $_POST['fname']);
          if(!$db->query("CALL setUserFName('$username', '$fname')")){
            $err = 1;
          } else {
            $updates = $updates + 1;
          }
        } else {
          $err = 2;
        }
      }
      // validate and update the last name value, if required
      if(isset($_POST['lname'])  && !empty($_POST['lname'])) {
        if(preg_match("/^[a-zA-Z]+-?[a-zA-Z]+$/", $_POST['lname'])){
          $lname = mysqli_real_escape_string($db, $_POST['lname']);
          if(!$db->query("CALL setUserLName('$username', '$lname')")){
            $err = 1;
          } else {
            $updates = $updates + 1;
          }
        } else {
          $err = 2;
        }
      }

      // validate and update the email value, if requierd
      if(isset($_POST['email'])  && !empty($_POST['email'])) {
        if(preg_match('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $_POST['email'])){
          $email = mysqli_real_escape_string($db, $_POST['email']);
          if(!$db->query("CALL setUserEmail('$username', '$email')")){
            $err = 1;
          } else {
            $updates = $updates + 1;
          }
        } else {
          $err = 2;
        }
      }

      // pring out an error message if something went wrong. Only display a success message if something was updated.
      if($err == 0){
        if($updates >= 1){
          header("location: user-preferences.php?message=success");
        } else {
          header("location: user-preferences.php?message=nada");
        }
      } else {
        header("location: user-preferences.php?message=error");
      }
    }
  ?>
  <body>

    <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
          <li><a href="select-acct.php">My Accounts</a></li>
          <li><a href="user-home.php">User Home</a></li>
          <li><a href="logout.php" type="button" class="button">Logout</a></li>
          </ul>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="select-acct.php">My Accounts</a></li>
                    <li><a href="user-home.php">User Home</a></li>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <li><a href="logout.php" type="button" class="button">Logout</a></li>
                    </ul>
                    
                  </div>
                
              </div>
            </div>
          </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">

            <div class="wrap">

              <h1>Adjust Preferences</h1>

          </div>

        </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 

          <div class="wrap row">

          <?php
            if (isset($_GET['message'])){
              if($_GET['message'] == 'error'){
                echo "<p style='color:red'>Error, something went wrong. Please try again.</p>";
              } elseif ($_GET['message'] == 'error2'){
                echo "<p style='color:red'>Error, push error. Please try again.</p>";
              }
              elseif($_GET['message'] == 'success'){
                echo "<p>Success! Updates have been applied.</p>";
              } else {
                echo "<p style='color:red'>Error, nothing to change.</p>";
              }
            }
          ?>

          <?php
              include("config.php");
              // setup the table head
              $username = $_SESSION['login_user'];
              echo"
              <table id='user-info' class='hover'>
                <thead>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email Address</th>
                  <th>Username</th>
                </thead>
                <tbody>";

                $sql = "SELECT user.userFName, user.userLName, user.userEmail, user.userName FROM User WHERE user.userName = '$username'";

                $result = mysqli_query($db,$sql);

                while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                  echo "
                  <tr>
                    <td>{$row['userFName']}</td>
                    <td>{$row['userLName']}</td>
                    <td>{$row['userEmail']}</td>
                    <td>{$row['userName']}</td>
                  </tr>
                  ";
                  // <td>{$row['']}</td>
                }

              echo "</tbody>
              </table>"
            ?>

            <form name="myform" method="POST">

              <div class="row">
                <div class="large-6 columns">
                  <label>First Name
                    <input type="text" name="fname" placeholder="John" onkeyup="validate(0)">
                  </label>
                  <small class="error" id='fname-error'></small>
                </div>
                <div class="large-6 columns">
                  <label>Last Name
                    <input type="text" name="lname" placeholder="Doe" onkeyup="validate(1)">
                  </label>
                  <small class="error" id='lname-error'></small>   
                </div>
            
              </div>
              <div class="row">
                <div class="large-6 columns">
                  <label>Email
                    <input type="text" placeholder="mail@mail.com" name="email" onkeyup="validate(2)">
                  </label>
                  <small class="error" id="email-error"></small>
                </div>
              </div>

              <label></label>
              <input type="submit" class="button" value ="Update Changed Values"></input> 


            </form>


            <a class="button success" type="button" href="update-pw.php">Click Here to change your password.</a>
          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <?php
                  $user = $_SESSION['login_user'];
                  echo'<a href="index.php?user=' . $user . '">Home</a>' .
                  '<a href="about.php?user=' . $user . '">Services</a>' .
                  '<a href="contact-us.php?user=' . $user . '">Contact Us</a>';
                ?>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto:c17graham.johnson@usafa.edu"><span>Email</span> c17graham.johnson@usafa.edu</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>