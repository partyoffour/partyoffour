<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Party of Four</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>
  <body>

  <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <?php
            if (!isset($_GET['user'])){
              echo '<li><a href="index.php">Home</a></li>
              <li><a href="#">About</a>
                <ul class="menu vertical">
                  <li><a href="about-rest.php">Restaurant</a></li>
                  <li><a href="about.php">Consumer</a></li>
                </ul>
              </li>
              <li><a href="contact-us.php">Contact Us</a></li>
              <li><a href="signup.php">Signup</a></li>';
              echo "<li><a href='login.php'>Login</a></li>";
            } else {
              $user = $_GET['user'];
              echo '
              <li><a href="index.php?user=' . $user . '">Home</a></li>
              <li><a href="#">About</a>
                <ul class="menu vertical">
                  <li><a href="about-rest.php?user=' . $user . '">Restaurant</a></li>
                  <li><a href="about.php?user=' . $user . '">Consumer</a></li>
                </ul>
              </li>
              <li><a href="contact-us.php?user=' . $user . '">Contact Us</a></li>';
              echo "<li><a href='select-acct.php'>Accounts Home</a></li>";
              echo '<li><a href="logout.php" class="button" type="button">Logout</a></li>';
            }
          ?>        
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <?php
                      if (!isset($_GET['user'])){
                        echo '<li><a href="index.php">Home</a></li>
                        <li><a href="#">About</a>
                          <ul class="menu vertical">
                            <li><a href="about-rest.php">Restaurant</a></li>
                            <li><a href="about.php">Consumer</a></li>
                          </ul>
                        </li>
                        <li><a href="contact-us.php">Contact Us</a></li>
                        <li><a href="signup.php">Signup</a></li>';
                        echo "<li><a href='login.php'>Login</a></li>";
                      } else {
                        $user = $_GET['user'];
                        echo '
                        <li><a href="index.php?user=' . $user . '">Home</a></li>
                        <li><a href="#">About</a>
                          <ul class="menu vertical">
                            <li><a href="about-rest.php?user=' . $user . '">Restaurant</a></li>
                            <li><a href="about.php?user=' . $user . '">Consumer</a></li>
                          </ul>
                        </li>
                        <li><a href="contact-us.php?user=' . $user . '">Contact Us</a></li>';
                        echo "<li><a href='select-acct.php'>Accounts Home</a></li>";
                      }
                    ?>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <?php
                        if (isset($_GET['user'])) {
                          echo '<li><a href="logout.php" class="button" type="button">Logout</a></li>';
                        }
                      ?>
                    </ul>
                  </div>        
              </div>
            </div>
          </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">

            <div class="wrap">

              <h1>About - Party of Four</h1>

          </div>

        </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">
            <div class="medium-12">
            <div class="small-12 medium-6 column">
              <h1>Owners and Managers</h1>

              <p>Below are a list of features that Party of Four offer you to help manage your restaurant.</p>

              <h5>Online Menu Management</h5>

              <p>Party of Four allows the owner and manager to update the menu swiftly, immediately pushing new prices and menu items to servers and consumers, as well as easily categorize their restaurant's menu for easy access to the consumer.</p>
              
              <h5>Online Employee Management</h5>

              <p>Party of Four allows the owner and manager to easily manage employees, immediately granting new hires access to the restaurant's site, while also immediately removing access to fired employees. Party of Four also implements a privledged access system, so that users without proper privlidges cannot access certain managerial tools. Party of Four also gives each employee access to each other's contact information, so that swapping shifts can be handled without direct managerial involvement.</p>

              <h5>Interactive AI Predictions</h5>

              <p>Party of Four allows restaurant employees to access the local conditions of the restaurant and use a state of the art Artificial Intelligence tool to accurately predict wait times based on party size, restaurant capacity, and amount of food ordered. In the future, the AI will also help the host/manager with seating, stockinging, and other general restaurant management.</p>

              <h5>Interactive Seating and Ordering</h5>

              <p>In the future this will be supported by the AI, for now Party of Four allows the server/host to quickly assign a party to a specific table and allow all servers to pull the order if something goes wrong. Additionally, in the future a party will be able to order before they get to the restaurant, allowing for faster turn around times and a more efficient restaurant.</p>

              <h5>Provide a Review System that holds Reviews Accountable</h5>

              <p>Party of Four is currently experimenting with a review system where consumers must be confirmed as customers in our database before we will publish any reviews. That way, we can keep consumers honest and restaurants happy.</p>

              <h5>Cost</h5>

              <p>The first month is on us, free of charge. After that, Party of Four will cost 0.5% of all gross profits for a restaurant to continue to use our webapp. If using our app increases profits by even 1% then that's a steal!</p>

              <a href="restaurant-signup.php" class="button success">Signup Now!</a>

            </div>
            <div class="small-12 medium-6 column">
              <hr color=#fff>
              <img src="http://parkresto.com/wp-content/themes/parkrestaurant/images/11onlinereservationpark.jpg">
              <hr color=#fff>
              <img src="http://morestaurants.org/wp-content/uploads/2014/12/bigstock-Restaurant-manager-standing-in-52078486.jpg
              ">
              <hr color=#fff>
            </div>
          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <?php
                  if(!isset($_GET['user'])){
                    echo '<a href="index.php">Home</a>
                          <a href="about.php">About</a>
                          <a href="contact-us.php">Contact Us</a>';
                  } else {
                    $user = $_GET['user'];
                    echo '
                    <a href="index.php?user=' . $user . '">Home</a>
                    <a href="about.php?user=' . $user . '">About</a>
                    <a href="contact-us.php?user=' . $user . '">Contact Us</a>';
                  }
                ?>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto:c17graham.johnson@usafa.edu"><span>Email</span> c17graham.johnson@usafa.edu</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
