<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Menu</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">

    <?php
      include('session.php');

    ?>

    <script type="text/javascript">
    </script>

  </head>
  <body>

    <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
            <li><a href="select-acct.php">My Accounts</a></li>
            <li><a href="rest-home.php">Restaurant Home</a></li>
            <li><a href="logout.php" type="button" class="button">Logout</a></li>
          </ul>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="select-acct.php">My Accounts</a></li>
                    <li><a href="rest-home.php">Restaurant Home</a></li>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <li><a href="logout.php" type="button" class="button">Logout</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">

            <div class="wrap">

              <h1>Menu Management</h1>

          </div>

        </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">

            <?php
              include("configr.php");
              $restID = $_SESSION['restID']; // should get the rest id from the session
              // setup the table head
              $permission = $_SESSION['permissionID'];
              echo"
              <table id='employees' class='hover'>
                <thead>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Calories</th>
                  <th>Ingredients</th>
                  <th>Description</th>
                  ";
                  if($permission == 1){
                    echo "<th>Update?</th>";
                  }
                echo "</thead>
                <tbody>";

                $sql = "SELECT itemName, itemPrice, itemCalories, itemIngredients, itemDescript, menuItemID FROM MenuItem WHERE `Restaurant_restID` = $restID GROUP BY itemName";

                $result = mysqli_query($dbr,$sql);

                while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                  echo "
                  <tr>
                    <td>{$row['itemName']}</td>
                    <td>{$row['itemPrice']}</td>
                    <td>{$row['itemCalories']}</td>
                    <td>{$row['itemIngredients']}</td>
                    <td>{$row['itemDescript']}</td>";
                    if($permission == 1){
                      echo "<td><a href='update-menu.php?itemID={$row['menuItemID']}'>Update Item</a></td>";
                    }
                  echo "</tr>";
                }

              echo "</tbody>
              </table>";

              if($permission == 1){
                echo '<a href="add-menu.php" class="button" name="add">Add Menu Item</a>
            <a href="delete-menu.php" class="button" name="delete">Delete Menu Item</a>';
              }
            ?>

          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <?php
                  $user = $_SESSION['login_user'];
                  echo'<a href="index.php?user=' . $user . '">Home</a>' .
                  '<a href="about.php?user=' . $user . '">Services</a>' .
                  '<a href="contact-us.php?user=' . $user . '">Contact Us</a>';
                ?>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto:c17graham.johnson@usafa.edu"><span>Email</span> c17graham.johnson@usafa.edu</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>

          </section>
        </div>
      </div>
    </div>
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>