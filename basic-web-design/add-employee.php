<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Staff</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">

    <?php
      include('session.php');
      include('configr.php');

      if(isset($_SESSION['permissions'])){
        if($_SESSION['permissions'] != 1){
          header("location: staff-management.php?error=err1");
        }
      }

      if($_SERVER["REQUEST_METHOD"] == "POST") {
        $error = 0;
        
        if(empty($_POST['experience']) || empty($_POST['fname']) || empty($_POST['lname']) || empty($_POST['phone']) || empty($_POST['email']) || empty($_POST['pay'])) {
          $error = 1;
        }

        if($error === 1){
          header("location: add-employee.php?message=error1");
        }
        else {
          if(preg_match("/^[1-9]{1}\d?(\.\d{1})?$/", $_POST['experience'])){
            $experience = mysqli_real_escape_string($dbr,$_POST['experience']) . ' years';
          } else {
            $error = 2;
          }

          if(preg_match("/^\d{10}$/", $_POST['phone'])){
            $phone = mysqli_real_escape_string($dbr,$_POST['phone']);
          } else {
            $error = 2;
          }

          if(preg_match("/^[a-zA-Z]+-?[a-zA-Z]+$/", $_POST['lname'])){
            $lname = mysqli_real_escape_string($dbr,$_POST['lname']);
          } else {
            $error = 2;
          }

          if(preg_match("/^([a-zA-Z])+$/", $_POST['fname'])){
            $fname = mysqli_real_escape_string($dbr,$_POST['fname']);
          } else {
            $error = 2;
          }

          if(preg_match('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $_POST['email'])){
            $email = mysqli_real_escape_string($dbr,$_POST['email']);
          } else {
            $error = 2;
          }
          
          if(preg_match('/^\$\d+\.\d{2}$/', $_POST['pay'])){
            $pay = mysqli_real_escape_string($dbr,$_POST['pay']) . '/hr';
          } else {
            $error = 2;
          }

          $shift = mysqli_real_escape_string($dbr,$_POST['shift']);
          $job = intval($_POST['job']);
          $section = null;
          $rest = 1; // should get the rest id from the session

          if($error == 0){
            $restID = $_SESSION['restID'];
            if (!$dbr->query("CALL createNewStaff('$lname', '$fname', '$experience', '$phone', '$email', '$shift', '$pay', (SELECT jobID FROM `job` WHERE jobID='$job' LIMIT 1), NULL, $restID)")){
              header('location: add-employee.php?message=error3');
            } else {
              header('location: add-employee.php?message=success');
            }
          } else {
            header('location: add-employee.php?message=error2');
          }
        }

      }
    ?>

    <script type="text/javascript">

    function validate(num) {
      switch(num) {
        case 0:
          var re = /^[A-Z][a-zA-Z]+$/ // /^$/
          if(!re.test(signup.fname.value)) {
            document.getElementById('fname-error').innerHTML="Please enter a valid first name.";
          } 
          else {
            document.getElementById("fname-error").innerHTML="";
          } break;
        case 1:
          var re = /^[a-zA-Z]+-?[a-zA-Z]+$/
          if(!re.test(signup.lname.value)) {
            document.getElementById('lname-error').innerHTML="Please enter a valid last name.";
          } else {
            document.getElementById("lname-error").innerHTML="";
          } break;
        case 2:
          var re = /^\$\d+\.\d{2}$/
          if(!re.test(signup.pay.value)) {
            document.getElementById('pay-error').innerHTML="Enter a valid dollar amount format: $#+.##.";
          } else{
            document.getElementById('pay-error').innerHTML="";
          } break;
        case 3:
          var re = /^\d{10}$/
          if( !re.test(signup.phone.value) ) {
            document.getElementById('phone-error').innerHTML="Invalid phone number, format: XXXXXXXXXX.";
          }
          else {
            document.getElementById('phone-error').innerHTML="";
          } break;
        case 4:
          re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          if( !re.test(signup.email.value) ) {
              document.getElementById('email-error').innerHTML="Invalid email address.";
          }
          else {
            document.getElementById('email-error').innerHTML="";
          } break;
        case 5:
          re = /^[1-9]{1}\d?(\.\d{1})?$/
          if(!re.test(signup.experience.value) ) {
            document.getElementById('experience-error').innerHTML="Please input a valid number for years (<100) format: X, XX or XX.X.";
          } else {
            document.getElementById('experience-error').innerHTML="";
          }
        default:
          break;
        }
      }

    </script>

  </head>
  <body>

    <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
            <li><a href="rest-home.php">Home</a></li>
            <li><a href="rest-home.php">Cancel</a><li>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="rest-home.php">Home</a.</li>
                    <li><a href="rest-home.php">Cancel</a></li>
                </div>                
              </div>
            </div>
          </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">

            <div class="wrap">
              <h1>Add Employee</h1>
          </div>

        </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">

            <?php
            if(isset($_GET['message'])){
              if($_GET['message'] == 'error1'){
                echo "<p style='color:red'>Error, please fill in all fields.</p>";
              } elseif ($_GET['message'] == 'success'){
                echo "<p>Success!</p>";
              } elseif($_GET['message'] == 'error2'){
                echo "<p style='color:red'>Error, one of your values was invalid.</p>";
              } elseif($_GET['message'] == 'error3'){
                echo "<p style='color:red'>Error, insert failed.</p>";
              }
            }
          ?>

    				<form name="signup" id="signup" method="post">
    					<fieldset>
    						<legend>Add Employee</legend>

    						<div class="row">
    		    				<div class="large-6 medium-6 small-12 columns">
    						      <label color=>First Name (*)
    						        <input type="text" placeholder="John" name="fname" onkeyup="validate(0)" />
    						      </label>
                      <small class="error" id="fname-error"></small>
    						    </div>
    						  
    						    <div class="large-6 medium-6 small-12 columns">
    						      <label>Last Name (*)
    						        <input type="text" placeholder="Doe" name="lname" onkeyup="validate(1)"/>
    						      </label>
                      <small class="error" id="lname-error"></small>
    						    </div>
    						</div>

                <div class="row">
                  <div class="large-4 medium-4 columns">
                    <label>Job Type (*)
                      <select name="job">
                        <option value="1">Dish Washer</option>
                        <option value="2">Waiter/Waitress</option>
                        <option value="3">Host/Hostess</option>
                        <option value="4">Sous Chef</option>
                        <option value="5">Chef</option>
                        <option value="6">Personnel Manager</option>
                        <option value="7">Janitor</option>
                        <option value="8">Bus Boy</option>
                        <option value="9">Marketing Manager</option>
                        <option value="10">Logistics Manager</option>
                      </select>
                    </label>
                  </div>

                  <div class="large-4 medium-4 columns">
                    <label>Experience (Years) (*)
                      <input type="text" placeholder="0" name="experience" onkeyup="validate(5)">
                    </label>
                    <small class="error" id="experience-error"></small>
                  </div>

                  <div class="large-4 medium-4 columns">
                    <label>Employee Pay per Hour (*)
                      <input type="text" placeholder="$8.00" name="pay" onkeyup="validate(2)">
                    </label>
                    <small class="error" id="pay-error"></small>
                  </div>
                </div>

                <div class="row">
                  <div class="large-6 medium-6 columns">
        						<label>Phone Number (*)
        	      			<input type="text" placeholder="1234567890" name="phone" onkeyup="validate(3)">
        	    			</label>
                    <small class="error" id="phone-error"></small>
                  </div>

                  <div class="large-6 medium-6 columns">
        	    			<label>Email Address (*)
        	      			<input type="text" placeholder="mail@example.com" name="email" onkeyup="validate(4)">
        	    			</label>
                    <small class="error" id="email-error"></small>
                  </div>
                </div>

                <div class="row">
                  <div class="large-12 medium-12 columns">
                    <label>Employee Work Preferences (*)</label>
                    <select name="shift">
                      <option value="Shift 1">Weekdays Daytime (Shift 1)</option>
                      <option value="Shift 2">Weekdays Nighttime (Shift 2)</option>
                      <option value="Shift 3">Weekends Daytime (Shift 3)</option>
                      <option value="SHift 4">Weekends Nighttime (Shift 4)</option>
                    </select>
                  </div>
                  <small class="error" id="preferences-error"></small>
                </div>

                <div class="row">
                  <div class="large-12 medium-12 columns">
                    <label>Other Considerations</label>
                    <input id="checkbox6" type="checkbox"><label for="checkbox1">Employee Younger Than 18?</label>
                    <input id="checkbox7" type="checkbox"><label for="checkbox2">Employee Younger Than 16?</label>
                  </div>
                </div>
                <div class="row">
                  <div class="small-12 columns">
                    <label>Additional Comments</label>
                    <input type="text" id="comments">
                  </div>
                </div>

    					</fieldset>
              <input type="submit" class="button" value ="Add Employee"></input> 
              <a href="staff-management.php" class="button" id="cancel">Cancel</a>
    				</form>
			</div>

          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <a href="index.php">Home</a>
                <a href="about.php">Services</a>
                <a href="contact-us.php">Contact Us</a>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto:c17graham.johnson@usafa.edu"><span>Email</span> c17graham.johnson@usafa.edu</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>

          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>