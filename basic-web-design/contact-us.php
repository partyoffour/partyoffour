<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Party of Four</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>
  <body>

     <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <?php
            if (!isset($_GET['user'])){
              echo '<li><a href="index.php">Home</a></li>
              <li><a href="#">About</a>
                <ul class="menu vertical">
                  <li><a href="about-rest.php">Restaurant</a></li>
                  <li><a href="about.php">Consumer</a></li>
                </ul>
              </li>
              <li><a href="contact-us.php">Contact Us</a></li>
              <li><a href="signup.php">Signup</a></li>';
              echo "<li><a href='login.php'>Login</a></li>";
            } else {
              $user = $_GET['user'];
              echo '
              <li><a href="index.php?user=' . $user . '">Home</a></li>
              <li><a href="#">About</a>
                <ul class="menu vertical">
                  <li><a href="about-rest.php?user=' . $user . '">Restaurant</a></li>
                  <li><a href="about.php?user=' . $user . '">Consumer</a></li>
                </ul>
              </li>
              <li><a href="contact-us.php?user=' . $user . '">Contact Us</a></li>';
              echo "<li><a href='select-acct.php'>Accounts Home</a></li>";
              echo '<li><a href="logout.php" class="button" type="button">Logout</a></li>';
            }
          ?>        
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <?php
                      if (!isset($_GET['user'])){
                        echo '<li><a href="index.php">Home</a></li>
                        <li><a href="#">About</a>
                          <ul class="menu vertical">
                            <li><a href="about-rest.php">Restaurant</a></li>
                            <li><a href="about.php">Consumer</a></li>
                          </ul>
                        </li>
                        <li><a href="contact-us.php">Contact Us</a></li>
                        <li><a href="signup.php">Signup</a></li>';
                        echo "<li><a href='login.php'>Login</a></li>";
                      } else {
                        $user = $_GET['user'];
                        echo '
                        <li><a href="index.php?user=' . $user . '">Home</a></li>
                        <li><a href="#">About</a>
                          <ul class="menu vertical">
                            <li><a href="about-rest.php?user=' . $user . '">Restaurant</a></li>
                            <li><a href="about.php?user=' . $user . '">Consumer</a></li>
                          </ul>
                        </li>
                        <li><a href="contact-us.php?user=' . $user . '">Contact Us</a></li>';
                        echo "<li><a href='select-acct.php'>Accounts Home</a></li>";
                      }
                    ?>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <?php
                        if (isset($_GET['user'])) {
                          echo '<li><a href="logout.php" class="button" type="button">Logout</a></li>';
                        }
                      ?>
                    </ul>
                  </div>        
              </div>
            </div>
          </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">
            <div class="wrap">
              <h1>Contact Us - Party of Four</h1>
            </div>
          </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">
            <div class="small-12 medium-12 column">
              <div class="medium-6 medium-6 column">
                <h2>Project Leader - Casen Askew</h2>

                <p>The Glorius Leader of the Party of Four team, Casen loves nothing more than a fine scotch or rye whiskey. Often seen running around in the hills of Colorado, Casen is master of snow and sun, especially the hot Texas sun.</p>

                <h3>Background/Education</h3>
                <ul>
                  <li>High School Diploma</li>
                  <li>BS Computer Science, USAFA (tentative)</li>
                  <li>World class bull-wrestler and horse-rider</li>
                  <li>Former mercenary for the Ukranian mob.</li>
                </ul>

                <h3>Social Media</h3>
                <ul>
                  <li><a href="https://www.facebook.com/casen.ray">Facebook</a></li>
                  <li><a href="#">Twitter</a></li>
                  <li><a href="https://www.linkedin.com/in/casenaskew">LinkedIn</a></li>
                </ul>

                <p><a href="mailto:c17casen.askew@usafa.edu">Email: c17casen.askew@usafa.edu</a></p>
                <p><a href="#">Phone: 123-456-7890</a></p>

              </div>

              <div class="small-6 medium-6 column">
                <hr color=#fff>
                <img src="https://scontent.fapa1-1.fna.fbcdn.net/v/t1.0-1/12316343_1074388042601849_5244682733138068325_n.jpg?oh=a117282afe28f200c65a8947f5185198&oe=595FD0BA">
                <hr color=#fff>
              </div>
            </div>

            <div class="small-12 medium-12 column">

              <div class="small-6 medium-6 column">
                <hr color=#fff>
                <img src="https://img.discogs.com/BB-ziKdEp0DwVNvp8vBYP64AYC0=/582x404/smart/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/A-2175-1234962706.jpeg.jpg">
                <hr color=#fff>
              </div>
            

              <div class="medium-6 medium-6 column">
                <h2>Scrum Master - Kris Fortier</h2>

                <p>A master of 81 forms of unarmed combat (and two forms of armed combat), Kris is a master of both the arts of peace and the arts of war. Using his extensize knowledge of ancient Norwegian spellcraft (all inexplicibly written in Inaucian script), Kris can summon a whirpool of goat's milk and stinky cheese upon request. A hero in at least four minds, the group wouldn't function nearly as well without his due dilligence.</p>

                <h3>Background/Education</h3>
                <ul>
                  <li>League of Shadows, honored graduate.</li>
                  <li>BS Computer Science, USAFA (tentative)</li>
                  <li>Odin's Inspiration for Greatness</li>
                  <li>Master sandwich smith.</li>
                </ul>

                <h3>Social Media</h3>
                <ul>
                  <li><a href="https://www.facebook.com/UsAirForceAcademyCyberCompetitionTeam/posts/739387976117046">Facebook</a></li>
                  <li><a href="#">Twitter</a></li>
                  <li><a href="#">LinkedIn</a></li>
                </ul>

                <p><a href="mailto:c17kris.fortier@usafa.edu">Email: c17kris.fortier@usafa.edu</a></p>
                <p><a href="#">Phone: 123-456-7890</a></p>

              </div>

              <div class="small-12 medium-12 column">
              <div class="medium-6 medium-6 column">
                <h2>AI Hero - Austin McWhirter</h2>

                <p>Master of small hacks with large results (and big hacks with headbanging results for his enemies), Austin has vowed to always keep his hands on the keyboard. Often seen in the gym, lifting his keyboard for thousands of repetitions and running with it as his cyber 'rifle' during ruck runs, Austin isn't afraid to be a trend setter. Using his gymnastics skills, along with a lightweight, washable adhesive that he developed after reading Captain Underpants as a toddler, Austin has a wide range of gadgets that would make Q jealous and McGuyver proud.</p>

                <h3>Background/Education</h3>
                <ul>
                  <li>Honored guest, MakerFaire</li>
                  <li>BS Computer Science, USAFA (tentative)</li>
                  <li>Lead programmer, Stuxnet</li>
                  <li>USAFA hacker of the century, 1972</li>
                </ul>

                <h3>Social Media</h3>
                <ul>
                  <li><a href="https://www.facebook.com/austin.mcwhirter.3">Facebook</a></li>
                  <li><a href="https://quizlet.com/179297576/cs-39-squad-staff-usafa-2017-spring-flash-cards/">Quizlet</a></li>
                  <li><a href="https://www.linkedin.com/in/austinmcwhirter">LinkedIn</a></li>
                </ul>

                <p><a href="mailto:c17austin.mcwhirter@usafa.edu">Email: c17austin.mcwhirter@usafa.edu</a></p>
                <p><a href="#">Phone: 123-456-7890</a></p>

              </div>

              <div class="small-6 medium-6 column">
                <hr color=#fff>
                <img src="https://scontent.fapa1-1.fna.fbcdn.net/v/t1.0-9/189785_1900012943944_677038_n.jpg?oh=e42afc7e48b08eb0a7dff156d382cb53&oe=5958CA6A">
                <hr color=#fff>
              </div>
            </div>

            <div class="small-12 medium-12 column">

              <div class="small-6 medium-6 column">
                <hr color=#fff>
                <img src="http://3dshapes.org/images/stories/3dshapesclipart/cone1.png">
                <hr color=#fff>
              </div>
            

              <div class="medium-6 medium-6 column">
                <h5>Database Wizard - Daniel (Don't Contact Me) Kohn</h5>

                <p>Daniel is very private, so here are some cone facts:</p>

                <h3>Fun Facts!</h3>
                <ul>
                  <li>A cone is a three-dimensional, geometric shape that tapers smoothly from a flat base.</li>
                  <li>The volume of a cone is 1/3 * pi * r^3 * h</li>
                  <li>A cone's center of mass is 1/3 of its height above center of its base</li>
                  <li>Ice cream is often served in a cone, because that's the way it's served</li>
                </ul>

                <h3>Social Media</h3>
                <ul>
                  <li><a href="https://www.facebook.com/ConesLover/">Facebook</a></li>
                  <li><a href="https://twitter.com/Cone?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor">Twitter</a></li>
                  <li><a href="https://www.linkedin.com/company/cone_2">LinkedIn</a></li>
                </ul>

                <p><a href="#">Email: example@gmail.com</a></p>
                <p><a href="#">Phone: 123-456-7890</a></p>

              </div>

              <div class="small-12 medium-12 column">
              <div class="medium-6 medium-6 column">
                <h3>Web Master - Graham Johnson</h3>

                <p>Just here so he doesn't get fined, Graham is the tragic, sole survivor of a solo backpacking trip to his car from Sijan to two-degree parking during a snow storm, when he stopped at the sound of retreat. He lost three toes, but science has given him an artificial set of wheels, which unfortunately his roommate uses on a daily basis, giving him pinkie pain in his left hand. A cornucopia of spelling errors and random thoughts all jumbled together, he is only slightly less weird than Enzo Capone.</p>

                <h3>Background/Education</h3>
                <ul>
                  <li>Eagle Scout</li>
                  <li>BS Computer Science, USAFA (tentative)</li>
                  <li>Has a dad that Dan thinks does all of his work</li>
                  <li>Amateur arm wrestler, a Petaluma pastime</li>
                </ul>

                <h3>Social Media</h3>
                <ul>
                  <li><a href="https://www.facebook.com/graham.johnson.399">Facebook</a></li>
                  <li><a href="#">Twitter</a></li>
                  <li><a href="#">LinkedIn</a></li>
                </ul>

                <p><a href="mailto:c17graham.johnson@usafa.edu">Email: c17graham.johnson@usafa.edu</a></p>
                <p><a href="#">Phone: 123-456-7890</a></p>

              </div>

              <div class="small-6 medium-6 column">
                <hr color=#fff>
                <img src="https://scontent.fapa1-1.fna.fbcdn.net/v/t1.0-9/13882131_1260603407284195_4026882360917507329_n.jpg?oh=2a955a574d4c357febbbb6cd3f4fdb76&oe=5995D0A0">
                <hr color=#fff>
              </div>
            </div>
          
            </div>
          </div>
        </section>

        

        

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <?php
                  if(!isset($_GET['user'])){
                    echo '<a href="index.php">Home</a>
                          <a href="about.php">About</a>
                          <a href="contact-us.php">Contact Us</a>';
                  } else {
                    $user = $_GET['user'];
                    echo '
                    <a href="index.php?user=' . $user . '">Home</a>
                    <a href="about.php?user=' . $user . '">About</a>
                    <a href="contact-us.php?user=' . $user . '">Contact Us</a>';
                  }
                ?>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto:c17graham.johnson@usafa.edu"><span>Email</span> c17graham.johnson@usafa.edu</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>