<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Item</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">

    <?php
      include('session.php');
      include('configr.php');
      if($_SERVER["REQUEST_METHOD"] == "POST") {
        $error = 0;
        $restID = $_SESSION['restID']; // should get the rest id from the session
        
        if(empty($_POST['mname']) || empty($_POST['price']) || empty($_POST['calories'])
          || empty($_POST['desc']) || empty($_POST['ingredients']) || empty($_POST['cookTime']) || empty($_POST['deliverTime']) || empty($_POST['eatTime'])) {
          $error = 1;
        }


        if($error === 1){
          header("location: add-menu.php?message=error1");
        }
        else {
          if(preg_match("/^[a-zA-Z]([a-zA-Z]|\-|\d|\s)+$/", $_POST['mname'])){
            $mname = mysqli_real_escape_string($dbr,$_POST['mname']);
          } else {
            $error = 2;
          }

          if(preg_match("/^\d{1,4}\.\d{2}$/", $_POST['price'])){
            $price = floatval(mysqli_real_escape_string($dbr,$_POST['price']));
          } else {
            $error = 2;
          }

          if(preg_match("/^(([1-9]{1}\d{1,3})|([0]))$/", $_POST['calories'])){
            $calories = intval(mysqli_real_escape_string($dbr,$_POST['calories']));
          } else {
            $error = 2;
          }

          if(preg_match("/^([a-zA-Z]|\d|\(|\)|\;|\,|\-|\s)+$/", $_POST['desc'])){
            $desc = mysqli_real_escape_string($dbr,$_POST['desc']);
          } else {
            $error = 2;
          }

          if(preg_match("/^([a-zA-Z]|\d|\(|\)|\;|\,|\-|\s)+$/", $_POST['ingredients'])){
            $ingredients = mysqli_real_escape_string($dbr,$_POST['ingredients']);
          } else {
            $error = 2;
          }

          if(preg_match("/^\d{2}\:\d{2}\:\d{2}.\d{6}$/", $_POST['cookTime'])){
            $cookTime = mysqli_real_escape_string($dbr,$_POST['cookTime']);
          } else {
            $error = 2;
          }

          if(preg_match("/^\d{2}\:\d{2}\:\d{2}.\d{6}$/", $_POST['deliverTime'])){
            $deliverTime = mysqli_real_escape_string($dbr,$_POST['deliverTime']);
          } else {
            $error = 2;
          }

          if(preg_match("/^\d{2}\:\d{2}\:\d{2}.\d{6}$/", $_POST['eatTime'])){
            $eatTime = mysqli_real_escape_string($dbr,$_POST['eatTime']);
          } else {
            $error = 2;
          }

          if($error == 0) {

            $rest = 1; // should get the rest id from the session

            if (!$dbr->query("CALL createNewMenuItem('$mname', '$desc', '$price', '$calories', '$ingredients', '$eatTime', '$deliverTime', '$cookTime', (SELECT restID FROM Restaurant WHERE restID = '$restID' LIMIT 1))")){
              echo "CALL failed: (" . $dbr->errno . ") " . $dbr->error;
              // header("location: add-menu.php?message=error3");
            } else {
              header("location: add-menu.php?message=success");
            }
          } else {
            header("location: add-menu.php?message=error2");
          }
        }

      }
    ?>

    <script type="text/javascript">

    function validate(num) {
      switch(num) {
        case 0:
          var re = /^[a-zA-Z]([a-zA-Z]|\-|\d|\s)+$/
          if(!re.test(add.mname.value)) {
            document.getElementById('mname-error').innerHTML="Please enter a menu item name.";
          } 
          else {
            document.getElementById("mname-error").innerHTML="";
          } break;
        case 1:
          var re = /^([a-zA-Z]|\d|\(|\)|\;|\,|\-|\s)+$/
          if(!re.test(add.desc.value)) {
            document.getElementById('desc-error').innerHTML="Please enter a valid item description.";
          } else {
            document.getElementById("desc-error").innerHTML="";
          } break;
        case 2:
          var re = /^\d{1,4}\.\d{2}$/
          if(!re.test(add.price.value)){
            document.getElementById('price-error').innerHTML="Enter a valid price format: #+.##.";
          } else{
            document.getElementById('price-error').innerHTML="";
          } break;
        case 3:
          var re = /^([a-zA-Z]|\d|\(|\)|\;|\,|\-|\s)+$/
          if( !re.test(add.ingredients.value)) {
            document.getElementById('ingredients-error').innerHTML="Invalid ingredients list.";
          }
          else {
            document.getElementById('ingredients-error').innerHTML="";
          } break;
        case 4:
          var re = /^\d{2}\:\d{2}\:\d{2}.\d{6}$/
          if( !re.test(add.cookTime.value) ) {
              document.getElementById('cookTime-error').innerHTML="Invalid cook time, format: ##:##:##.######.";
          }
          else {
            document.getElementById('cookTime-error').innerHTML="";
          } break;
        case 5:
          var re = /^\d{2}\:\d{2}\:\d{2}.\d{6}$/
          if(!re.test(add.deliverTime.value)){
              document.getElementById('deliverTime-error').innerHTML="Invalid deliver time, format: ##:##:##.######.";
          } else {
            document.getElementById('deliverTime-error').innerHTML="";
          }
        case 6:
          var re = /^\d{2}\:\d{2}\:\d{2}.\d{6}$/
          if( !re.test(add.eatTime.value) ) {
              document.getElementById('eatTime-error').innerHTML="Invalid deliver time, format: ##:##:##.######.";
          } else {
            document.getElementById('eatTime-error').innerHTML="";
          }
        case 7:
          var re = /^(([1-9]{1}\d{1,3})|([0]))$/
          if( !re.test(add.calories.value) ) {
              document.getElementById('calories-error').innerHTML="Invalid number of calories. Either 0 or 10-9999";
          } else {
            document.getElementById('calories-error').innerHTML="";
          }
        default:
          break;
        }
      }

    </script>

  </head>
  <body>

    <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
            <li><a href="select-acct.php">My Accounts</a></li>
            <li><a href="rest-home.php">Restaurant Home</a></li>
            <li><a href="logout.php" type="button" class="button">Logout</a></li>
          </ul>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="select-acct.php">My Accounts</a></li>
                    <li><a href="rest-home.php">Restaurant Home</a></li>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <li><a href="logout.php" type="button" class="button">Logout</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">

            <div class="wrap">
              <h1>Add Menu Item</h1>
          </div>

        </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">

            <?php
            if(isset($_GET['message'])){
              if($_GET['message'] == 'error1'){
                echo "<p style='color:red'>Error, please fill in all fields.</p>";
              } elseif ($_GET['message'] == 'success'){
                echo "<p>Success!</p>";
              } elseif($_GET['message'] == 'error2'){
                echo "<p style='color:red'>Error, one of your values was invalid.</p>";
              } elseif($_GET['message'] == 'error3'){
                echo "<p style='color:red'>Error, insert failed.</p>";
              }
            }
          ?>

    				<form name="add" id="add" method="post">
    					<fieldset>
    						<legend>Add Menu Item</legend>

    						<div class="row">
    		    				<div class="large-4 medium-4 small-12 columns">
    						      <label >Item Name (*)
    						        <input type="text" placeholder="Fritters" name="mname" onkeyup="validate(0)" />
    						      </label>
                      <small class="error" id="name-error"></small>
    						    </div>
    						  
    						    <div class="large-4 medium-4 small-12 columns">
    						      <label>Item Price (*)
    						        <input type="text" placeholder="0.00" name="price" onkeyup="validate(2)"/>
    						      </label>
                      <small class="error" id="price-error"></small>
    						    </div>

                    <div class="large-4 medium-4 small-12 columns">
                      <label>Item Calories (*)
                        <input type="text" placeholder="0" name="calories" onkeyup="validate(7)"/>
                      </label>
                      <small class="error" id="calories-error"></small>
                    </div>
    						</div>

                <div class="row">
                  <div class="large-12 medium-12 small-12 columns">
                  <label> Item Description (*)
                    <input type="text" name="desc" placeholder="This is your item's description..." onkeyup="validate(1)">
                  </label>
                  <small class="error" id="desc-error"></small>
                  </div>
                </div>

                <div class="row">
                  <div class="large-12 medium-12 small-12 columns">
                  <label> Item Ingredients (*)
                    <input type="text" name="ingredients" placeholder="This is your item's ingredients..." onkeyup="validate(3)">
                  </label>
                  <small class="error" id="ingredients-error"></small>
                  </div>
                </div>

                <div class="row">
                  <div class="large-4 medium-4 columns">
                    <label> Cook Time (*)
                      <input type="text" name="cookTime" placeholder="0" onkeyup="validate(4)">
                    </label>
                    <small class="error" id="cookTime-error"></small>
                  </div>

                  <div class="large-4 medium-4 columns">
                    <label> Deliver Time (*)
                      <input type="text" placeholder="0" name="deliverTime" onkeyup="validate(5)">
                    </label>
                    <small class="error" id="deliverTime-error"></small>
                  </div>

                  <div class="large-4 medium-4 columns">
                    <label> Eat Time (*)
                      <input type="text" placeholder="0" name="eatTime" onkeyup="validate(6)">
                    </label>
                    <small class="error" id="eatTime-error"></small>
                  </div>
                </div>
                <p>Note: all times are formatted days:hours:minutes:seconds.</p>
    					</fieldset>

              <input type="submit" class="button" value ="Add Menu Item"></input> 
              <a href="manage-menu.php" class="button" id="cancel">Cancel</a>
    				</form>
			</div>

          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <?php
                  $user = $_SESSION['login_user'];
                  echo'<a href="index.php?user=' . $user . '">Home</a>' .
                  '<a href="about.php?user=' . $user . '">Services</a>' .
                  '<a href="contact-us.php?user=' . $user . '">Contact Us</a>';
                ?>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto:c17graham.johnson@usafa.edu"><span>Email</span> c17graham.johnson@usafa.edu</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>

          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>