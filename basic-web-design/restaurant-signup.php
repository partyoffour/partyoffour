<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Signup</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">

    <script type="text/javascript">
    function validate(num) {
      // validates the input with regular expressions and JS.
      switch(num) {
        case 0:
          var re = /^[A-Z]([a-zA-Z]|\s)+$/
          if(!re.test(myform.rname.value)) {
            document.getElementById('rname-error').innerHTML="Please enter a valid restaurant name (Only letters and spaces).";
          } 
          else {
            document.getElementById("rname-error").innerHTML="";
          } break;
        case 1:
          var re = /^[1-9]\d+(\s\w+){2,}$/
          if(!re.test(myform.addr.value)) {
            document.getElementById('addr-error').innerHTML="Please enter a valid address, format: Street Number Street Name.";
          } else {
            document.getElementById("addr-error").innerHTML="";
          } break;
        case 4:
          var re = /^\d{5}$/
          if(!re.test(myform.zip.value)){
            document.getElementById('zip-error').innerHTML="Enter a valid zip code.";
          } else{
            document.getElementById('zip-error').innerHTML="";
          } break;
        case 2:
          var re = /^[A-Z]([a-zA-Z]|\s)+$/
          if(!re.test(myform.city.value)) {
            document.getElementById('city-error').innerHTML="Invalid city name (only letters and spaces).";
          }
          else {
            document.getElementById('city-error').innerHTML="";
          } break;
        case 3:
          /^[A-Z]([a-zA-Z]|\s)+$/
          if( !re.test(myform.state.value)) {
            document.getElementById('state-error').innerHTML="Invalid state name (only letters and spaces).";
          }
          else {
            document.getElementById('state-error').innerHTML="";
          } break;
        case 5:
          var re = /^\d{10}$/
          if( !re.test(myform.phone.value)) {
            document.getElementById('phone-error').innerHTML="Invalid phone number. Format: XXXXXXXXXX";
          }
          else {
            document.getElementById('phone-error').innerHTML="";
          } break;
        default:
          break;
        }
      }
  </script>

  <?php
      include('session.php');
      include('configr.php');

      if($_SERVER["REQUEST_METHOD"] == "POST") {
        $err = 0; $updates = 0;
        $rest = 1; // should get the rest id from the session

        if(!empty($_POST['addr']) && !empty($_POST['city']) && !empty($_POST['state']) && !empty($_POST['zip'])
          && !empty($_POST['rname']) && !empty($_POST['phone'])) {

          
          if(!empty($_POST['addr'])) {
            if(preg_match("/^[1-9]\d+(\s\w+){2,}$/", $_POST['addr'])){
              $addr = mysqli_real_escape_string($dbr,$_POST['addr']);
            } else {
              $err = 2;
            }
          }

          if(!empty($_POST['city'])) {
            if(preg_match("/^[A-Z]([a-zA-Z]|\s)+$/", $_POST['city'])){
              $city = mysqli_real_escape_string($dbr,$_POST['city']);
            } else {
              $err = 2;
            }
          }

          if(!empty($_POST['state'])) {
            if(preg_match("/^[A-Z]([a-zA-Z]|\s)+$/", $_POST['state'])){
              $state = mysqli_real_escape_string($dbr,$_POST['state']);
            } else {
              $err = 2;
            }
          }

          if(!empty($_POST['zip'])) {
            if(preg_match("/^\d{5}$/", $_POST['zip'])){
              $zip = mysqli_real_escape_string($dbr,$_POST['zip']);
            } else {
              $err = 2;
            }
          }

          if(!empty($_POST['phone'])) {
            if(preg_match("/^\d{10}$/", $_POST['phone'])){
              $phone = mysqli_real_escape_string($dbr,$_POST['phone']);
            } else {
              $err = 2;
            }
          }

          if(!empty($_POST['rname'])) {
            if(preg_match("/^[A-Z]([a-zA-Z]|\s)+$/", $_POST['rname'])){
              $rname = mysqli_real_escape_string($dbr, $_POST['rname']);
            } else {
              $err = 2;
            }
          }

          if($err == 0){
            $address = $addr . ', ' . $city . ', ' . $state . ' ' . $zip;
            $address = preg_replace("/^\s{2,}$/", ' ', $address); // remove excess spaces
            if(!$dbr->query("CALL createNewRestaurant('$rname', '$address', '$phone')")){
              $err = 2;
            } else {
              $uname = $_SESSION['login_user'];
              $sql = "SELECT restID FROM Restaurant WHERE restName = '$rname' AND restAddress = '$address' AND restPhone = '$phone' LIMIT 1";
              $result = mysqli_query($dbr, $sql)->fetch_assoc()['restID'];
              include('config.php');
              $sql = "INSERT INTO permissions values('$uname', $result, 1)";
              if(!$db->query($sql)){

              } else {
                
              }
            }
          }
        }


          if($err == 0){
            header("location: select-acct.php?message=success");
          } else {
            header("location: restaurant-signup.php?message=error2");
          }
        } 
      // }
    ?>

  </head>
  <body>

    <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
            <li><a href="select-acct.php">Cancel</a><li>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <!-- <li><a href="index.php">Home</a.</li> -->
                    <li><a href="select-acct.php">Cancel</a></li>
                </div>                
              </div>
            </div>
          </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">

            <div class="wrap">

              <h1>Restaurant Signup Form</h1>

          </div>

        </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">
      		<form method="POST" name="myform">
              <label> Name
                <input type='text' name='rname' onkeyup="validate(0)">
                <small class="error" id='rname-error'></small>
              </label>

              <label> Address
                <input type='text' name='addr' onkeyup="validate(1)">
                <small class="error" id='addr-error'></small>
              </label>

              <label> City
                <input type='text' name='city' onkeyup="validate(2)">
                <small class="error" id='city-error'></small>
              </label>

              <label> State (US State or Country)
                <input type='text' name='state' onkeyup="validate(3)">
                <small class="error" id='state-error'></small>
              </label>

              <label> Zip Code
                <input type='text' name='zip' onkeyup="validate(4)">
                <small class="error" id='zip-error'></small>
              </label>

              <label> Phone Number
                <input type='text' name='phone' onkeyup="validate(5)">
                <small class="error" id='phone-error'></small>
              </label>

              <input type="submit" name="submit" value="Submit" class="button">

            </form>
			</div>

		<!-- 	<a href="#" class="button">Submit</a> -->

          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <a href="index.php">Home</a>
                <a href="about.php">Services</a>
                <a href="contact-us.php">Contact Us</a>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="#"><span>Email</span> example@gmail.com</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>

          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
