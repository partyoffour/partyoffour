<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Password</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>

  <script type="text/javascript">
    
    function validate(num, tag) {
      switch(num) {
        case 1:
          var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,20}$/
          if(!re.test(myform.pw_new.value)) {
            if(tag) {
              document.getElementById('pw-new-error').innerHTML="Passwords must contain 6-20 characters, 1 uppercase, 1 lowercase, 1 number, and 1 special character";
            } break;
          } else {
            document.getElementById('pw-new-error').innerHTML="";
          } break;
        case 2: 
          if(myform.pw_new2.value !== myform.pw_new.value) {
            if(tag) {
              document.getElementById('pw-new2-error').innerHTML="Please verify that your two paswords match.";
            } break;
          } else {
            document.getElementById('pw-new2-error').innerHTML="";
          } break;
        default:
          break;
      }
    }
  </script>

  <?php
    include('session.php');
    include('config.php');

    if($_SERVER["REQUEST_METHOD"] == "POST") {
      // validate that everything is filled in
      $error = 0;
      if(empty($_POST['pw_old'])){
        $error = 1;
      }
      if(empty($_POST['pw_new'])){
        $error = 1;
      }
      if(empty($_POST['pw_new2'])){
        $error = 1;
      }

      if($error==0){
        if($_POST['pw_new'] != $_POST['pw_new2']){
          $error = 2;
        } else if(!preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,20}$/", $_POST['pw_new'])){
          $error = 3;
        }

        if(!preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,20}$/", $_POST['pw_old'])){
          $error = 3;
        }

        if($error == 0){
          $pwOld = mysqli_real_escape_string($db, $_POST['pw_old']);
          $pwNew = mysqli_real_escape_string($db, $_POST['pw_new']);
          $username = $_SESSION['login_user'];
          // if()
          $result = mysqli_query($db, "CALL changePassword('$username', '$pwOld', '$pwNew')");
          $num = mysqli_num_rows($result);
          if($num == 1){
            header("location: update-pw.php?message=error4");
          } else {
            header("location: update-pw.php?message=success");
          }

        } else {
          if($error == 2){
            header("location: update-pw.php?message=error2");
          } else if ($error == 3){
            header("location: update-pw.php?message=error3");
          }
        }

      } 
      else{
        header("location: update-pw.php?message=error1");
      }

    }
  ?>

  <body>

    <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
          <li><a href="select-acct.php">My Accounts</a></li>
          <li><a href="user-home.php">User Home</a></li>
          <li><a href="" type="button" class="button">Logout</a></li>
          </ul>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="select-acct.php">My Accounts</a></li>
                    <li><a href="user-home.php">User Home</a></li>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <li><a href="index.php" type="button" class="button">Logout</a></li>
                    </ul>
                    
                  </div>
                
              </div>
            </div>
          </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">
            <div class="wrap">
              <h1>Update Password</h1>
            </div>
          </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 

          <div class="wrap row">
            <?php
              if (isset($_GET['message'])){
                if($_GET['message'] == 'error1'){
                  echo "<p style='color:red'>Error, please fill in all values.</p>";
                }
                else if($_GET['message'] == 'error2'){
                  echo "<p style='color:red'>Error, new password fields must match.</p>";
                }
                else if($_GET['message'] == 'error3'){
                  echo "<p style='color:red'>Error, new password was invalid. Passwords must be 6 or more characters long and contain 1 uppercase, 1 lowercase, 1 number, and 1 special character.</p>";
                }
                else if($_GET['message'] == 'error4'){
                  echo "<p style='color:red'>Error, old password was incorrect. Please try again.</p>";
                }
                else if($_GET['message'] == 'success'){
                  echo "<p>Success! Your password was updated.</p>";
                }
              }
            ?>
             <form name="myform" action="" method="POST">
                <!-- <fieldset> -->
                <legend>Password Info</legend>

                <div class="row">
                  <div class="large-4 columns">
                    <label>Old Password
                      <input type="password" placeholder="*****" name="pw_old" onkeyup="validate(0,true)">
                    </label>
                    <small class="error" id="pw-error"></small>
                  </div>
                
                  <div class="large-4 columns">
                  <label>New Password
                    <input type="password" placeholder="*****" name="pw_new" onkeyup="validate(1,true)"> 
                  </label>
                  <small class="error" id="pw-new-error"></small>
                </div>

                <div class="large-4 columns">
                  <label>New Password
                    <input type="password" placeholder="*****" name="pw_new2" onkeyup="validate(2,true)"> 
                  </label>
                  <small class="error" id="pw-new2-error"></small>
                </div>            
              </div>
              <input type="submit" class="button success" value ="Submit" />
            </form>

          </div>

        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <?php
                  $user = $_SESSION['login_user'];
                  echo'<a href="index.php?user=' . $user . '">Home</a>' .
                  '<a href="about.php?user=' . $user . '">Services</a>' .
                  '<a href="contact-us.php?user=' . $user . '">Contact Us</a>';
                ?>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="#"><span>Email</span> example@gmail.com</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>