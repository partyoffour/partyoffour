<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Party of Four</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>
  <body>

    <?php 
    ?>

    <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <?php
            if (!isset($_GET['user'])){
              echo '<li><a href="index.php">Home</a></li>
              <li><a href="#">About</a>
                <ul class="menu vertical">
                  <li><a href="about-rest.php">Restaurant</a></li>
                  <li><a href="about.php">Consumer</a></li>
                </ul>
              </li>
              <li><a href="contact-us.php">Contact Us</a></li>
              <li><a href="signup.php">Signup</a></li>';
              echo "<li><a href='login.php'>Login</a></li>";
            } else {
              $user = $_GET['user'];
              echo '
              <li><a href="index.php?user=' . $user . '">Home</a></li>
              <li><a href="#">About</a>
                <ul class="menu vertical">
                  <li><a href="about-rest.php?user=' . $user . '">Restaurant</a></li>
                  <li><a href="about.php?user=' . $user . '">Consumer</a></li>
                </ul>
              </li>
              <li><a href="contact-us.php?user=' . $user . '">Contact Us</a></li>';
              echo "<li><a href='select-acct.php'>Accounts Home</a></li>";
              echo '<li><a href="logout.php" class="button" type="button">Logout</a></li>';
            }
          ?>        
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <?php
                      if (!isset($_GET['user'])){
                        echo '<li><a href="index.php">Home</a></li>
                        <li><a href="#">About</a>
                          <ul class="menu vertical">
                            <li><a href="about-rest.php">Restaurant</a></li>
                            <li><a href="about.php">Consumer</a></li>
                          </ul>
                        </li>
                        <li><a href="contact-us.php">Contact Us</a></li>
                        <li><a href="signup.php">Signup</a></li>';
                        echo "<li><a href='login.php'>Login</a></li>";
                      } else {
                        $user = $_GET['user'];
                        echo '
                        <li><a href="index.php?user=' . $user . '">Home</a></li>
                        <li><a href="#">About</a>
                          <ul class="menu vertical">
                            <li><a href="about-rest.php?user=' . $user . '">Restaurant</a></li>
                            <li><a href="about.php?user=' . $user . '">Consumer</a></li>
                          </ul>
                        </li>
                        <li><a href="contact-us.php?user=' . $user . '">Contact Us</a></li>';
                        echo "<li><a href='select-acct.php'>Accounts Home</a></li>";
                      }
                    ?>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <?php
                        if (isset($_GET['user'])) {
                          echo '<li><a href="logout.php" class="button" type="button">Logout</a></li>';
                        }
                      ?>
                    </ul>
                  </div>        
              </div>
            </div>
          </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">

            <div class="wrap">

              <h1>Welcome to Party of Four!</h1>

              <p>Imagine being able to set reservations online, check whether or not your favorite restaurant is busy, or have your favorite drinks waiting for you when you arrive. Image one app, usable across all of your devices, that will save your favorite orders, your reservation times, and your seating preferences. On the restaurant side, imagine being able to accurately predict wait times and order times, so that you can effectively serve as many customers as possible. Party of Four enables all of this and more. -PO4 Webmaster</p>

              <?php
                if(!isset($_GET['user'])){
                  echo '<a href="about.php" class="button">Learn More</a>
                        <a href="contact-us.php" class="button success">Contact Us</a>';
                } else {
                  $user = $_GET['user'];
                  echo '<a href="about.php?user=' . $user . '" class="button">Learn More</a>
                        <a href="contact-us.php?user=' . $user . '" class="button success">Contact Us</a>';
                }
              ?>

          </div>

        </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">
            <div class="small-12 medium-6 column">
              <h1>For Restaurants</h1>

              <p>The Party of Four webapp platform provides a number of both unique and necessary services, including, but not limited to:</p>
              <ul>
                <li>Online Meun Management</li>
                <li>Online Employee Management</li>
                <li>Interactive AI Predictions</li>
                <li>Interactive Seating and Ordering</li>
                <li>Provide a Review System that holds Reviews Accountable</li>
              </ul>
              <p>For an in depth look at our services, click below.</p>
              <?php
                if(!isset($_GET['user'])){
                  echo '<a href="about.php" class="button">Click Here to Learn More</a>';
                } else {
                  $user = $_GET['user'];
                  echo '<a href="about.php?user=' . $user . '" class="button">Click Here to Learn More</a>';
                }
              ?>
              <!-- <a href="about.php" class="button">Click Here to Learn More</a> -->
            </div>
          <!-- </div> -->
          <div class="small-12 medium-6 column">
              <h1>For Consumers</h1>

              <p>The Party of Four webapp provides consumers with a number or unique services that you won't find anywhere else, including:</p>
              <ul>
                <li>Restuarant Preference Management. Never Forget your Favorite restaurant ever again</li>
                <li>Save Your Favorite Orders</li>
                <li>Pay on the Go, Like Uber</li>
                <li>Provide Restaurant Suggestions/Search Wherever Our Service is Avaliable</li>
                <li>Provide a Custom Review System for our Userbase</li>
              </ul>
              <?php
                if(!isset($_GET['user'])){
                  echo '<a href="about-rest.php" class="button">Click Here to Learn More</a>';
                } else {
                  $user = $_GET['user'];
                  echo '<a href="about-rest.php?user=' . $user . '" class="button">Click Here to Learn More</a>';
                }
              ?>
              <!-- <a href="about-rest.php" class="button">Click Here to Learn More</a> -->
            </div>
          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <?php
                  if(!isset($_GET['user'])){
                    echo '<a href="index.php">Home</a>
                          <a href="about.php">About</a>
                          <a href="contact-us.php">Contact Us</a>';
                  } else {
                    $user = $_GET['user'];
                    echo '
                    <a href="index.php?user=' . $user . '">Home</a>
                    <a href="about.php?user=' . $user . '">About</a>
                    <a href="contact-us.php?user=' . $user . '">Contact Us</a>';
                  }
                ?>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto:c17graham.johnson@usafa.edu"><span>Email</span> c17graham.johnson@usafa.edu</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
