<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Party of Four</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>
  <body>

<?php
   include("config.php"); // Configure to the server, uses the user db

  session_start();

  if(isset($_SESSION['login_user'])){
    header('select-acct.php');
  }
   
  if($_SERVER["REQUEST_METHOD"] == "POST") {

    $count = 0;

    if(empty($_POST['username'])) {
      $count = 1;
    } else if (!preg_match("/^(?:[a-zA-Z\d][a-zA-Z\d_-]{1,20}|[a-zA-Z\d._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})$/", $_POST['username'])){
      $count = 2;
    }
    if(empty($_POST['password'])) {
      $count = 1;
    } else if (!preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,20}$/", $_POST['password'])) {
      $count = 2;
    }

    if($count == 0) {
        $username = mysqli_real_escape_string($db,$_POST['username']);
        $password = mysqli_real_escape_string($db,$_POST['password']);

        $validateQuery = "SELECT validateLogin('$username', '$password') as login";

        $success = mysqli_query($db,$validateQuery)->fetch_assoc()['login'];
        
        if($success) {
          $sql = "SELECT permissions FROM permissions WHERE `User-userName`='$username'";
          $result = mysqli_query($db,$sql)->fetch_assoc()['permissions'];
          $_SESSION['permissionID'] = $result;
          $_SESSION['login_user'] = $username;
          $_SESSION['login_ip'] = getRealIpAddr();
          header("location: select-acct.php");
        }
        else {
          header("location: login.php?error=3");
        }
    } 
    else {
      if($count == 1){
        header("location: login.php?error=1");
      } else {
        header("location: login.php?error=2");
      }
    }

    if (count($_POST) > 0) {
      foreach ($_POST as $k=>$v) {
          unset($_POST[$k]);
      }
    }

   }

   function getRealIpAddr()
   {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
   }
?>

  <div class="off-canvas-wrap">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

      <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
        <ul>
          <li><a href="index.php">Cancel Login</a></li>
        </ul>
      </div>

      <div class="off-canvas-content" data-off-canvas-content>

  <!-- ******************* MOBILE NAVIGATION  ************************************-->
  <div class="title-bar nav-mobile"> 
    <div class="title-bar-left">
      <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
      <span class="title-bar-title">Party of Four</span>
    </div>
  </div>
          
  <!-- **************************** DESKTOP NAVIGATION ************************* -->
      
  <div class="top-bar nav-desktop">
    <div class="wrap">
      <div class="top-bar-title">
        <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
          <button class="menu-icon dark" type="button" data-toggle></button>
        </span>
        <h3 class="site-logo">Party of Four</h3>
      </div>
      <div id="responsive-menu">
        <div class="top-bar-left">
          <ul class="dropdown menu" data-dropdown-menu>
            <li><a href="index.php">Cancel Login</a></li>
          </ul>
        </div>
        
      </div>
    </div>
  </div>

  <!-- ************************ HERO ***************************************** -->

  <section class="hero">
    <div class="wrap">
      <h1 color="red">Login</h1>
   </div>
 </section>


  <!--  ************************ MAIN ****************************************** -->

   <section class="main"> 
      <div class="wrap row">
        <div class="row">
          <div class="medium-6 medium-centered large-4 large-centered columns">
            <div class="row column log-in-form">

              <?php
                if(isset($_GET['error'])){
                  if($_GET['error'] == 1){
                    echo "<p style='color:red'>Error, please fill in all values.</p>";
                  } else if ($_GET['error'] == 2){
                    echo "<p style='color:red'>Error, potential SQL injection detected. Please try again.</p>";
                  }
                  else {
                    echo "<p style='color:red'>Invalid username, password combination.</p>";
                  }
                }
              ?>
              
              <h4 class="text-center">Please enter your username and password.</h4>
              
              <form action="" method="post">   
                <label>Username</label>
                  <input type="text" name="username" placeholder="username">
                
                <label>Password</label>
                <input type="password" name="password" placeholder="password">
                
                <input type="submit" class="button expanded" value ="Submit"></input>
              </form>
                
                <!-- <p class="text-center"><a href="forgotten-password.php">Forgot your password?</a></p> -->
                <p class="text-center"><a href="user-signup.php">Need to signup?</a></p>
              </div>
            

          </div>
        </div>
      </div>
    </section>

        <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <a href="index.php">Home</a>
                <a href="about.php">Services</a>
                <a href="contact-us.php">Contact Us</a>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="#"><span>Email</span> example@gmail.com</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>

          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>

   </body>

</html>