<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Party of Four</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>
  <body>

   <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <?php
            if (!isset($_GET['user'])){
              echo '<li><a href="index.php">Home</a></li>
              <li><a href="#">About</a>
                <ul class="menu vertical">
                  <li><a href="about-rest.php">Restaurant</a></li>
                  <li><a href="about.php">Consumer</a></li>
                </ul>
              </li>
              <li><a href="contact-us.php">Contact Us</a></li>
              <li><a href="signup.php">Signup</a></li>';
              echo "<li><a href='login.php'>Login</a></li>";
            } else {
              $user = $_GET['user'];
              echo '
              <li><a href="index.php?user=' . $user . '">Home</a></li>
              <li><a href="#">About</a>
                <ul class="menu vertical">
                  <li><a href="about-rest.php?user=' . $user . '">Restaurant</a></li>
                  <li><a href="about.php?user=' . $user . '">Consumer</a></li>
                </ul>
              </li>
              <li><a href="contact-us.php?user=' . $user . '">Contact Us</a></li>';
              echo "<li><a href='select-acct.php'>Accounts Home</a></li>";
              echo '<li><a href="logout.php" class="button" type="button">Logout</a></li>';
            }
          ?>        
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <?php
                      if (!isset($_GET['user'])){
                        echo '<li><a href="index.php">Home</a></li>
                        <li><a href="#">About</a>
                          <ul class="menu vertical">
                            <li><a href="about-rest.php">Restaurant</a></li>
                            <li><a href="about.php">Consumer</a></li>
                          </ul>
                        </li>
                        <li><a href="contact-us.php">Contact Us</a></li>
                        <li><a href="signup.php">Signup</a></li>';
                        echo "<li><a href='login.php'>Login</a></li>";
                      } else {
                        $user = $_GET['user'];
                        echo '
                        <li><a href="index.php?user=' . $user . '">Home</a></li>
                        <li><a href="#">About</a>
                          <ul class="menu vertical">
                            <li><a href="about-rest.php?user=' . $user . '">Restaurant</a></li>
                            <li><a href="about.php?user=' . $user . '">Consumer</a></li>
                          </ul>
                        </li>
                        <li><a href="contact-us.php?user=' . $user . '">Contact Us</a></li>';
                        echo "<li><a href='select-acct.php'>Accounts Home</a></li>";
                      }
                    ?>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <?php
                        if (isset($_GET['user'])) {
                          echo '<li><a href="logout.php" class="button" type="button">Logout</a></li>';
                        }
                      ?>
                    </ul>
                  </div>        
              </div>
            </div>
          </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">
            <div class="wrap">
              <h1>About - Party of Four</h1>
          </div>
        </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">
            <div class="medium-12 column">
            <div class="small-12 medium-6 column">
                
                <img src="http://www.restaurantnews.com/wp-content/uploads/2014/09/The-Power-of-a-Happy-Customer.jpg">
                <hr color=#fff>
                <img src="http://tribalwomanmagazine.com/wp-content/uploads/2014/04/How-to-Drive-Success-in-Your-Restaurant-Business-this-Summer.jpg">
                <hr color=#fff>
              </div>
             <div class="small-12 medium-6 column">
                <h1>Consumers</h1>

                <p>Below are a list of features offered (or that will be offered) by Party of Four</p>

                <h5>Restuarant Preference Management. Never Forget your Favorite restaurant ever again</h5>

                <p>Have you ever gone to a great restaurant when you were travelling around, never to find it again? Always want to sit in a booth, if one is avaliable? Whatever your preferences and whatever restaurants you, Party of Four will help you make it easy to reserve again, have drinks waiting for you when you arrive, and more.</p>

                <h5>Pay on the Go, like Uber</h5>

                <p>Ever finish your meal and really want the check? Hate waiting 5, 10, even 15 minutes for your server to get back to you in a crowded restaurant? While not implemented yet, this feature will allow you to simply validate your ticket, leave a tip, and pay, all on the app. Like Uber, the payment will be simple and automatic.</p>

                <h5>Provide Restaurant Suggestions/Search Wherever Our Service is Avaliable</h5>

                <p>Party of Four offers a search feature for all participating restaurants. Party of four may also promote certain restaurants, but we promise that we will never display a restaurant if it doesn't conform to your search.</p>

                <h5>Provide a Custom Review System</h5>

                <p>One of the problems with other customer review services, like Yelp, is that there is no accountability. While we will display a restaurant's rating on other services, along with our own, our system offers accountability. Only users who have eaten at a restaurant will be able to leave a review for it, so you know that reviewers are real people.</p>

                <a href="user-signup.php" class="button success">Signup Now!</a>
                
              </div>
          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <?php
                  if(!isset($_GET['user'])){
                    echo '<a href="index.php">Home</a>
                          <a href="about.php">About</a>
                          <a href="contact-us.php">Contact Us</a>';
                  } else {
                    $user = $_GET['user'];
                    echo '
                    <a href="index.php?user=' . $user . '">Home</a>
                    <a href="about.php?user=' . $user . '">About</a>
                    <a href="contact-us.php?user=' . $user . '">Contact Us</a>';
                  }
                ?>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto:c17graham.johnson@usafa.edu"><span>Email</span> c17graham.johnson@usafa.edu</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
