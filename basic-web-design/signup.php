<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Signup</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>

  <?php
    include("config.php");

    session_start();

    if($_SERVER["REQUEST_METHOD"] == "POST") {
      $count = 0;
      if(empty($_POST['username']) || empty($_POST['lname']) || empty($_POST['password']) ||
        empty($_POST['fname']) || empty($_POST['email'])){
        $count = 1;
      }

      if ($count == 0) {

        if(preg_match("/^([a-zA-Z])+$/", $_POST['fname'])){
          $fname = mysqli_real_escape_string($db, $_POST['fname']);
        } else {
          $count = 2;
        }

        if(preg_match("/^[a-zA-Z]+-?[a-zA-Z]+$/", $_POST['lname'])){
          $lname = mysqli_real_escape_string($db, $_POST['lname']);
        } else {
          $count = 2;
        }

        if(preg_match('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $_POST['email'])){
          $email = mysqli_real_escape_string($db, $_POST['email']);
        } else {
          $count = 2;
        }

        if($_POST['password'] != $_POST['password2']){
          $count = 2;
        } else if (!preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,20}$/", $_POST['password'])) {
          $count = 2;
        } else {
          $password = mysqli_real_escape_string($db,$_POST['password']); 
        }

        if(preg_match("/^(?:[a-zA-Z\d][a-zA-Z\d_-]{1,20}|[a-zA-Z\d._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})$/", $_POST['username'])){
          $username = mysqli_real_escape_string($db,$_POST['username']);
        } else {
          $count = 2;
        }

        if($count == 0){
          $sql = "SELECT `userName` FROM user WHERE `userName`='$username'";
          $result = mysqli_query($db,$sql);
          
          $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
          $num = mysqli_num_rows($result);

          // If result matched $username and $pw, table row must be 0 rows for creation
          if($num == 0) {
            $confirm_code = substr(md5(uniqid(rand(), true)), 8, 8);

            $_SESSION['login_user'] = $username;
            $_SESSION['login_pass'] = $password;
            $_SESSION['login_fname'] = $fname;
            $_SESSION['login_lname'] = $lname;
            $_SESSION['login_email'] = $email;
            // $_SESSION['login_zip'] = $zip;
            $_SESSION['confirm_code'] = $confirm_code; // random hash

            // Send the confirmation email
            //  $to = $email;
            //  $subject = 'Signup Verification: Party of Four'; // the subject line

            //  $message = "Welecome to Party of Four!
             
            //  Thank you for signing up with the username: '.$username.'

            //  Your confirmation code is: '.$confirm_code.'

            //  You must activate your account with the confirmation code to log in and access
            //  Party of Four. You were redirected to the confirmation page from the signup page.
            //  It can also be found here: https://www.pof4.com/submitted-user.php?'.$email.'&hash='.$confirm_code.'

            //  Thank you for signing up.

            //  -Graham Johnson, PoF Webmaster";

            // $headers = 'From:noreply.partyoffour@ygmail.com' . "\r\n"; // Set from headers
            // mail($to, $subject, $message, $headers); // Send our email

            header("location: submitted-user.php");
          } else {
            header("location: signup.php?error=err3");
          }

        } else {
          header("location: signup.php?error=err2");
        }

      } else {
        header("location: signup.php?error=err1");
      }
    }
  ?>

  <script type="text/javascript">
    function validate(num) {
      switch(num) {
        case 0:
          var re = /^([a-zA-Z]|-| )+$/
          if(!re.test(signup.fname.value)) {
            document.getElementById('fname-error').innerHTML="Please enter a valid first name.";
          } 
          else {
            document.getElementById("fname-error").innerHTML="";
          } break;
        case 1:
          var re = /^[a-zA-Z]+-?[a-zA-Z]+$/
          if(!re.test(signup.lname.value)) {
            document.getElementById('lname-error').innerHTML="Please enter a valid last name.";
          } else {
            document.getElementById("lname-error").innerHTML="";
          } break;
        case 2:
          re = /^\d{5}$/
          if( !re.test(signup.zip.value) ){
            document.getElementById('zip-error').innerHTML="Enter a valid zip code.";
          } else{
            document.getElementById('zip-error').innerHTML="";
          } break;
        case 3:
          re = /^(?:[a-zA-Z\d][a-zA-Z\d_-]{1,20}|[a-zA-Z\d._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})$/
          if(!re.test(signup.username.value)) {
            document.getElementById('uname-error').innerHTML="Invalid user name.";
          }
          else {
            document.getElementById('uname-error').innerHTML="";
          } break;
        case 4:
          var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          if( !re.test(signup.email.value)) {
            document.getElementById('email-error').innerHTML="Invalid email address.";
          }
          else {
            document.getElementById('email-error').innerHTML="";
          } break;
        case 5:
          var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,20}$/
          if( !re.test(signup.password.value) ) {
            document.getElementById('pw-error').innerHTML="Passwords must contain 6-20 characters, 1 uppercase, 1 lowercase, 1 number, and 1 special character ( $ @ $ ! % * ? & )";
          }
          else {
            document.getElementById('pw-error').innerHTML="";
          } break;
        case 6:
          if( signup.password.value !== signup.password2.value ) {
            document.getElementById('pw2-error').innerHTML="Error, passwords must match";
          }
          else {
            document.getElementById('pw2-error').innerHTML="";
          } break;
        default:
          break;
        }
      }
  </script>

  <body>

    <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
            <!-- <li><a href="index.php">Home</a></li> -->
            <li><a href="index.php">Cancel</a><li>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="index.php">Cancel</a></li>
                </div>                
              </div>
            </div>
          </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">
            <div class="wrap">
              <h1>User Signup Form</h1>
          </div>
        </section>

        <!--  ************************ MAIN ****************************************** -->
        <section class="main"> 
          <div class="wrap row">
            <?php
              if(isset($_GET['error'])){
                if($_GET['error'] == 'err1'){
                  echo "<p style='color:red'>Error, please fill in all values.</p>";
                } else if($_GET['error'] == 'err2'){
                  echo "<p style='color:red'>Error, check your inputs to ensure they are valid.</p>";
                } else if($_GET['error'] == 'err3'){
                  echo "<p style='color:red'>Error, please choose another username.</p>";
                }
              }
            ?>
            <form action="" method="post" name="signup">
              <div class="row">
                <div class="large-6 columns">
                  <label>First Name (*)</label>
                  <input type="text" placeholder="John" name="fname" onkeyup="validate(0)">
                  <small class="error" id="fname-error"></small>
                </div>
                <div class="large-6 columns">
                  <label>Last Name (*)</label>
                  <input type="text" placeholder="Doe" name="lname" onkeyup="validate(1)">
                  <small class="error" id="lname-error"></small>
                </div>
<!--                 <div class="large-4 columns">
                  <label>Zip Code (*)</label>
                  <input type="text" name="zip" placeholder="12345" onkeyup="validate(2)">
                  <small class="error" id="zip-error"></small>
                </div> -->
              </div>
              <div class="row">
                <div class="large-6 columns">
                  <label>Username (*)</label>
                  <input type="text" placeholder="example@email.com" name="username" onkeyup="validate(3)">
                  <small class="error" id="uname-error"></small>
                </div>
                <div class="large-6 columns">
                  <label>Email (*)</label>
                  <input type="text" placeholder="example@email.com" name="email" onkeyup="validate(4)">
                  <small class="error" id="email-error"></small>
                </div>
              </div>
              <div class="row">
                <div class="large-6 columns">
                  <label>Password (*)</label>
                  <input type="password" placeholder="secret" name="password" onkeyup="validate(5)">
                  <small class="error" id="pw-error"></small>
                </div>
                <div class="large-6 columns">
                  <label>Confirm Password</label>
                  <input type="password" name="password2" placeholder="secret" onkeyup="validate(6)">
                  <small class="error" id="pw2-error"></small>
                </div>
              </div>
              <input type="submit" class="button" value ="Submit"></input> 
            </form>
          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->
          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <a href="index.php">Home</a>
                <a href="about.php">Services</a>
                <a href="contact-us.php">Contact Us</a>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="#"><span>Email</span> example@gmail.com</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
