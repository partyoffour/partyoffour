<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Party of Four</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>
  <body>

<?php
  include("config.php"); // Configure to the server, uses the user db

  session_start();
   
  if($_SERVER["REQUEST_METHOD"] == "POST") {

    if(empty($_POST['username'])){
      header("location: failed_login.php");
    }
    if(empty($_POST['password'])){
      header("location: failed_login.php");
    }  
      $username = mysqli_real_escape_string($db,$_POST['username']);
      // $username = SanitizeForSQL($username); // sanitize the input
      $pw = mysqli_real_escape_string($db,$_POST['password']); 

      echo " " . $username . " "; echo $pw;
      
      // $sql = "SELECT id FROM admin WHERE username = '$username' and passcode = '$pw";
      $sql = "SELECT `User-userName` FROM passhashes WHERE `User-userName`='$username' and passSalt='$pw'";
      $result = mysqli_query($db,$sql);
      
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      $count = mysqli_num_rows($result);
      
      // If result matched $username and $pw, table row must be 1 row
      if($count == 1) {
         $_SESSION['login_user'] = $username;
         // echo "success";
         header("location: select-acct.php");
      }else {
         $error = "Your Login Name or Password is invalid";
         // header("location: failed_login.php");
         header("location: failed_login.php");
      }
   }
?>

<html>
   
   <!-- <head> -->
      <!-- <html class="no-js" lang="en" dir="ltr"> -->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Party of Four</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>

  <body>

  <div class="off-canvas-wrap">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

      <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
        <ul>
          <li><a href="index.php">Cancel Login</a></li>
        </ul>
      </div>

      <div class="off-canvas-content" data-off-canvas-content>

  <!-- ******************* MOBILE NAVIGATION  ************************************-->
  <div class="title-bar nav-mobile"> 
    <div class="title-bar-left">
      <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
      <span class="title-bar-title">Party of Four</span>
    </div>
  </div>
          
  <!-- **************************** DESKTOP NAVIGATION ************************* -->
      
  <div class="top-bar nav-desktop">
    <div class="wrap">
      <div class="top-bar-title">
        <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
          <button class="menu-icon dark" type="button" data-toggle></button>
        </span>
        <h3 class="site-logo">Party of Four</h3>
      </div>
      <div id="responsive-menu">
        <div class="top-bar-left">
          <ul class="dropdown menu" data-dropdown-menu>
            <li><a href="index.php">Cancel Login</a></li>
          </ul>
        </div>
        
      </div>
    </div>
  </div>

  <!-- ************************ HERO ***************************************** -->

  <section class="hero">
    <div class="wrap">
      <h1 color="red">Login</h1>
   </div>
 </section>


  <!--  ************************ MAIN ****************************************** -->

   <section class="main"> 
      <div class="wrap row">
        <div class="row">
          <div class="medium-6 medium-centered large-4 large-centered columns">
            <div class="row column log-in-form">
              
              <h4 class="text-center">Please enter your username and email.</h4>
              
              <form action="" method="post">   
                <label>Username</label>
                  <input type="text" name="username" placeholder="username">
                
                <label>Email</label>
                <input type="text" name="email" placeholder="email">
                
                <input type="submit" class="button expanded" value ="Submit"></input>
              </form>
                <!-- 
                <p class="text-center"><a href="forgotten-password.php">Forgot your password?</a></p>
                <p class="text-center"><a href="user-signup.php">Need to signup?</a></p> -->
              </div>

          </div>
        </div>
      </div>
    </section>
   
   <!-- <body bgcolor = "#FFFFFF"> -->
  
      <!-- <div align = "center">
         <div style = "width:300px; border: solid 1px #333333; " align = "left">
            <div style = "background-color:#333333; color:#FFFFFF; padding:3px;"><b>Login</b></div>
        
            <div style = "margin:30px">
               
               <form action = "" method = "post">
                  <label>UserName  :</label><input type = "text" name = "username" class = "box"/><br /><br />
                  <label>Password  :</label><input type = "password" name = "password" class = "box" /><br/><br />
                  <input type = "submit" value = " Submit "/><br />
               </form>
               
            </div>  
         </div>
      </div> -->

        <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <a href="index.php">Home</a>
                <a href="about.php">Services</a>
                <a href="contact-us.php">Contact Us</a>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="#"><span>Email</span> example@gmail.com</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>

          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>

   </body>

</html>