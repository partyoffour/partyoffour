<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Staff</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">

    <?php
      include('session.php');

    ?>

    <script type="text/javascript">
    </script>

  </head>
  <body>

    <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
            <li><a href="rest-home.php">Home</a></li>
            
            <li><a href="logout.php">Logout</a></li>
          </ul>
          <label>Welcome, Owner!</label>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="select-acct.php">My Accounts</a></li>
                    <li><a href="rest-home.php">Restaurant Home</a></li>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <li><a href="logout.php" type="button" class="button">Logout</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>


          <!-- ************************ HERO ***************************************** -->

          <section class="hero">

            <div class="wrap">

              <h1>Employee Management</h1>

          </div>

        </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">

            <?php
              if(isset($_GET['error'])){
                if($_GET['error'] == 'err1') {
                  echo "<p style='color:red'>Error, you do not have the required permissions to access that!</p>";
                }
              }
            ?>

            <?php
              include("configr.php");
              // include('session.php');
              $permission = $_SESSION['permissionID'];
              // echo $permission;
              // setup the table head
              echo"
              <table id='employees' class='hover'>
                <thead>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Position</th>
                  <th>Shift</th>
                  <th>Phone Number</th>
                  <th>Email Address</th>";
                  if($permission == 1){
                    echo "<th>Update?</th>";
                  }
                echo "</thead>
                <tbody>";
                $restID = $_SESSION['restID'];
                $sql = "SELECT staff.staffFName, staff.staffLName, job.jobName, staff.staffPhone, staff.staffEmail, staff.staffShift, staff.staffID FROM staff, job WHERE `Restaurant_restID` = '$restID' AND job.jobID = staff.`Job_jobID` GROUP BY staff.staffLName, staff.staffFName, staff.staffID";

                $result = mysqli_query($dbr,$sql);

                while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                  echo "
                  <tr>
                    <td>{$row['staffLName']}</td>
                    <td>{$row['staffFName']}</td>
                    <td>{$row['jobName']}</td>
                    <td>{$row['staffShift']}</td>
                    <td>{$row['staffPhone']}</td>
                    <td>{$row['staffEmail']}</td>";
                    if($permission == 1){
                      echo "<td><a href='update-staff.php?staffID={$row['staffID']}'>Update Staff</a></td>";
                    }
                  echo "</tr>
                  ";
                  // <td>{$row['']}</td>
                }

              echo "</tbody>
              </table>";

              if($permission == 1){
                echo '<a href="add-employee.php" class="button" name="add">Add Employee</a>
            <a href="remove-employee.php" class="button" name="delete">Remove Employee</a>';
              }
            ?>

          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <?php
                  $user = $_SESSION['login_user'];
                  echo'<a href="index.php?user=' . $user . '">Home</a>' .
                  '<a href="about.php?user=' . $user . '">Services</a>' .
                  '<a href="contact-us.php?user=' . $user . '">Contact Us</a>';
                ?>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto:c17graham.johnson@usafa.edu"><span>Email</span> c17graham.johnson@usafa.edu</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div> 
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>