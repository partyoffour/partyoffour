<?php
	// session.php
	// validates the session
   include('config.php');
   session_start();
   error_reporting(E_ERROR); // turn off mysql error reporting
   
   $user_check = $_SESSION['login_user'];
   $ip_check = $_SESSION['login_ip'];
   
   $ses_sql = mysqli_query($db,"select username from admin where username = '$user_check' ");

   $row = mysqli_fetch_array($ses_sql,MYSQLI_ASSOC);
   
   $login_session = $row['username'];
   
   if(!isset($_SESSION['login_user'])){
      session_destroy();
      header("location: login.php");
   } elseif (getRealIpAddr() != $ip_check) {
      session_destroy();
      header("location: login.php");
   }

   function getRealIpAddr()
   {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
   }
?>