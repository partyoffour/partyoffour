<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Staff</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">

    <?php
      include('session.php');
      include('configr.php');
      
      if(isset($_SESSION['permissions'])){
        if($_SESSION['permissions'] != 1){
          header("location: staff-management.php?error=err1");
        }
      }

      if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['second'])) {
        $error = 0; $updates = 0;

        $id = $_POST['id'];
        
        if(!empty($_POST['experience'])) {
          if(preg_match("/^[1-9]{1}\d?(\.\d{1})?$/", $_POST['experience'])){
            $xp = mysqli_real_escape_string($dbr,$_POST['experience']) . " years";
            if($dbr->query("CALL setStaffXP($id, '$xp')")){
              $updates += 1;
            } else{
              $error = 2;
            }
          } else {
            $error = 1;
          }
        }

        if(!empty($_POST['phone'])) {
          if(preg_match("/^\d{10}$/", $_POST['phone'])){
            $phone = mysqli_real_escape_string($dbr,$_POST['phone']);
            if($dbr->query("CALL setStaffPhone($id, '$phone')")){
              $updates += 1;
            } else{
              $error = 2;
            }
          } else {
            $error = 1;
          }
        }

        if(!empty($_POST['lname'])) {
          if(preg_match("/^[a-zA-Z]+-?[a-zA-Z]+$/", $_POST['lname'])){
            $lname = mysqli_real_escape_string($dbr,$_POST['lname']);
            if($dbr->query("CALL setStaffLName($id, '$lname')")){
              $updates += 1;
            } else{
              $error = 2;
            }
          } else {
            $error = 1;
          }
        }
        if(!empty($_POST['fname'])) {
          if(preg_match("/^([a-zA-Z])+$/", $_POST['fname'])){
            $fname = mysqli_real_escape_string($dbr,$_POST['fname']);
            if($dbr->query("CALL setStaffFName($id, '$fname')")){
              $updates += 1;
            } else{
              $error = 2;
            }
          } else {
            $error = 1;
          }
        }

        if(!empty($_POST['email'])) {
          if(preg_match('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $_POST['email'])){
            $email = mysqli_real_escape_string($dbr,$_POST['email']);
            if($dbr->query("CALL setStaffEmail($id, '$email')")){
              $updates += 1;
            } else{
              $error = 2;
            }
          } else {
            $error = 1;
          }
        }
        
        if(!empty($_POST['pay'])) {
          if(preg_match('/^\$\d+\.\d{2}$/', $_POST['pay'])){
            $pay = mysqli_real_escape_string($dbr,$_POST['pay']) . '/hr';
            if($dbr->query("CALL setStaffPay($id, '$pay')")){
              $updates += 1;
            } else{
              $error = 2;
            }
          } else {
            $error = 1;
          }
        }

        if(intval($_POST['job']) != 0) {
          $job = intval($_POST['job']);
          if($dbr->query("UPDATE staff SET Job_jobID = $job WHERE staffID = $id ")){
            $updates += 1;
          } else {
            $error = 2;
          }
        }

        if($_POST['shift'] != '0') {
          $shift = $_POST['shift'];
          if($dbr->query("CALL setStaffShift($id, '$shift')")){
            $updates += 1;
          } else {
            $error = 2;
          }
        }

        if($error == 0){
          if($updates == 0){
            header("location: update-staff.php?message=error3&staffID=$id");
          } else {
            header("location: update-staff.php?message=success&staffID=$id");
          }
        } else {
          if($error = 1){
            header("location: update-staff.php?message=error1&staffID=$id");
          } else {
            header("location: update-staff.php?message=error2&staffID=$id");
          }
        }
      }
    ?>

    <script type="text/javascript">

    function validate(num) {
      switch(num) {
        case 0:
          var re = /^[A-Z][a-zA-Z]+$/ // /^$/
          if(!re.test(signup.fname.value)) {
            document.getElementById('fname-error').innerHTML="Please enter a valid first name.";
          } 
          else {
            document.getElementById("fname-error").innerHTML="";
          } break;
        case 1:
          var re = /^[a-zA-Z]+-?[a-zA-Z]+$/
          if(!re.test(signup.lname.value)) {
            document.getElementById('lname-error').innerHTML="Please enter a valid last name.";
          } else {
            document.getElementById("lname-error").innerHTML="";
          } break;
        case 2:
          var re = /^\$\d+\.\d{2}$/
          if(!re.test(signup.pay.value)) {
            document.getElementById('pay-error').innerHTML="Enter a valid dollar amount format: $#+.##.";
          } else{
            document.getElementById('pay-error').innerHTML="";
          } break;
        case 3:
          var re = /^\d{10}$/
          if( !re.test(signup.phone.value) ) {
            document.getElementById('phone-error').innerHTML="Invalid phone number, format: XXXXXXXXXX.";
          }
          else {
            document.getElementById('phone-error').innerHTML="";
          } break;
        case 4:
          re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          if( !re.test(signup.email.value) ) {
              document.getElementById('email-error').innerHTML="Invalid email address.";
          }
          else {
            document.getElementById('email-error').innerHTML="";
          } break;
        case 5:
          re = /^[1-9]\d?(\.\d{1})?$/
          if(!re.test(signup.experience.value) ) {
            document.getElementById('experience-error').innerHTML="Please input a valid number for years (<100) format: X, XX or XX.X.";
          } else {
            document.getElementById('experience-error').innerHTML="";
          }
        default:
          break;
        }
      }

    </script>

  </head>
  <body>

   <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
            <li><a href="rest-home.php">Home</a></li>
            
            <li><a href="logout.php">Logout</a></li>
          </ul>
          <label>Welcome, Owner!</label>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="select-acct.php">My Accounts</a></li>
                    <li><a href="rest-home.php">Restaurant Home</a></li>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <li><a href="logout.php" type="button" class="button">Logout</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>


          <!-- ************************ HERO ***************************************** -->

          <section class="hero">

            <div class="wrap">
              <h1>Update Employee</h1>
          </div>

        </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">

          <?php
            if(isset($_GET['message'])){
              if($_GET['message'] == 'error1'){
                echo "<p style='color:red'>Error, bad input.</p>";
              } elseif ($_GET['message'] == 'success'){
                echo "<p>Success!</p>";
              } elseif($_GET['message'] == 'error2'){
                echo "<p style='color:red'>Error, an update failed.</p>";
              } elseif($_GET['message'] == 'error3'){
                echo "<p style='color:red'>Error, no values to update.</p>";
              }
            }
          ?>

            <?php
              // if($_SERVER["REQUEST_METHOD"] == "POST") {
            if ($_SESSION['permissionID'] != 1){
              header("location: staff-management.php?error=err1");
            }

            else {
                if(isset($_GET['staffID'])){
                  
                  $id = intval($_GET['staffID']); // the ID
                  $restID = $_SESSION['restID'];
                  // echo $id;
                  $sql = "SELECT staffLName, staffFName, jobName, staffPhone, staffPay, staffShift, staffEmail, staffXP FROM staff, job WHERE staffID='$id' AND `Restaurant_restID`=$restID AND job.jobID = (SELECT `Job_jobID` from staff WHERE staffID='$id' AND `Restaurant_restID`=$restID);";
                  $result = mysqli_query($dbr,$sql);
                  $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
                  $count = mysqli_num_rows($result);

                  if($count == 0){
                    echo "<p style='color:red'>Error, nothing to display.</p>";
                  }
                  else {
                    echo "<table class='hover'>
                    <thead>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Job Title</th>
                      <th>Phone Number</th>
                      <th>Email Address</th>
                      <th>Pay</th>
                      <th>Experience</th>
                      <th>Shift</th>
                    </thead>
                    <tbody>
                      <td>{$row['staffFName']}</td>
                      <td>{$row['staffLName']}</td>
                      <td>{$row['jobName']}</td>
                      <td>{$row['staffPhone']}</td>
                      <td>{$row['staffEmail']}</td>
                      <td>{$row['staffPay']}</td>
                      <td>{$row['staffXP']}</td>
                      <td>{$row['staffShift']}</td>
                    </tbody>
                    </table>";



                    echo "<form name='signup' method='POST'>
                      <fieldset>
                  <legend>Update Employee</legend>

                  <div class='row'>
                      <div class='large-6 medium-6 small-12 columns'>
                        <label color=>First Name (*)
                          <input type='text' name='fname' onkeyup='validate(0)'>
                        </label>
                        <small class='error' id='fname-error'></small>
                      </div>
                    
                      <div class='large-6 medium-6 small-12 columns'>
                        <label>Last Name (*)
                          <input type='text' placeholder='Doe' name='lname' onkeyup='validate(1)'>
                        </label>
                        <small class='error' id='lname-error'></small>
                      </div>
                  </div>

                  <div class='row'>
                    <div class='large-4 medium-4 columns'>
                      <label>Job Type (*)
                        <select name='job'>
                          <option value='0'>No Change</option>
                          <option value='1'>Dish Washer</option>
                          <option value='2'>Waiter/Waitress</option>
                          <option value='3'>Host/Hostess</option>
                          <option value='4'>Sous Chef</option>
                          <option value='5'>Chef</option>
                          <option value='6'>Personnel Manager</option>
                          <option value='7'>Janitor</option>
                          <option value='8'>Bus Boy</option>
                          <option value='9'>Marketing Manager</option>
                          <option value='10'>Logistics Manager</option>
                        </select>
                      </label>
                    </div>

                    <div class='large-4 medium-4 columns'>
                      <label>Experience (Years) (*)
                        <input type='text' placeholder='0' name='experience' onkeyup='validate(5)'>
                      </label>
                      <small class='error' id='experience-error'></small>
                    </div>

                    <div class='large-4 medium-4 columns'>
                      <label>Employee Pay per Hour (*)
                        <input type='text' placeholder='$8.00' name='pay' onkeyup='validate(2)'>
                      </label>
                      <small class='error' id='pay-error'></small>
                    </div>
                  </div>

                  <div class='row'>
                    <div class='large-6 medium-6 columns'>
                      <label>Phone Number (*)
                        <input type='text' placeholder='1234567890' name='phone' onkeyup='validate(3)'>
                      </label>
                      <small class='error' id='phone-error'></small>
                    </div>

                    <div class='large-6 medium-6 columns'>
                      <label>Email Address (*)
                        <input type='text' placeholder='mail@example.com' name='email' onkeyup='validate(4)'>
                      </label>
                      <small class='error' id='email-error'></small>
                    </div>
                  </div>

                  <div class='row'>
                    <div class='large-12 medium-12 columns'>
                      <label>Employee Work Preferences (*)</label>
                      <select name='shift'>
                        <option value='0'>No Change</option>
                        <option value='Shift 1'>Weekdays Daytime (Shift 1)</option>
                        <option value='Shift 2'>Weekdays Nighttime (Shift 2)</option>
                        <option value='Shift 3'>Weekends Daytime (Shift 3)</option>
                        <option value='SHift 4'>Weekends Nighttime (Shift 4)</option>
                      </select>
                    </div>
                  </div>

                  <input type='hidden' name='second' />
                  <input type='hidden' name='id' value='$id' />

                  <input type='submit' class='button' value ='Update Employee' />
                  <a href='staff-management.php' class='button'>Cancel</a>

                  </form>
                    ";
                  }
                } 
                else {
                  header("location: staff-management.php?error=err1");
                }
              }
            ?>

          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <?php
                  $user = $_SESSION['login_user'];
                  echo'<a href="index.php?user=' . $user . '">Home</a>' .
                  '<a href="about.php?user=' . $user . '">Services</a>' .
                  '<a href="contact-us.php?user=' . $user . '">Contact Us</a>';
                ?>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto:c17graham.johnson@usafa.edu"><span>Email</span> c17graham.johnson@usafa.edu</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>