<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Party of Four</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>
  <body>

  <?php
    include('session.php');

  ?>

    <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <a href="logout.php" class="button" type="button">Logout</a>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">

                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <a href="logout.php" class="button" type="button">Logout</a>
                    </ul>
                  </div>
                
              </div>
            </div>
          </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">

            <div class="wrap">

              <h1>Welcome back to Party of Four!</h1>

          </div>

        </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">
            <div class="small-12 medium-6 column">
              <p>Click here to access your personal account</p>
              <a href="user-home.php" class="button">User Home</a>
            </div>
          <!-- </div> -->
          <div class="small-12 medium-6 column">
           <p>Click here to access your restaurant account(s)</p>
            <?php
              include('config.php');
              $uname = $_SESSION['login_user'];
              $sql = "SELECT restID, permissions FROM permissions WHERE `User-userName` = '$uname'";
              $result = mysqli_query($db, $sql);
              while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                echo "<div class='small-6 column'>
                <a class='button' href='rest-home.php?restID=" . $row['restID'] ."&permissions=" . $row['permissions'] ."'>Go to Restaurant</a>
                </div>";
              }
            ?>
            <div class='small-6 column'>
              <a href="restaurant-signup.php" class="button success">Add A New Restaurant</a>
            </div>
          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <?php
                  $user = $_SESSION['login_user'];
                  echo'<a href="index.php?user=' . $user . '">Home</a>' .
                  '<a href="about.php?user=' . $user . '">Services</a>' .
                  '<a href="contact-us.php?user=' . $user . '">Contact Us</a>';
                ?>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto:c17graham.johnson@usafa.edu"><span>Email</span> c17graham.johnson@usafa.edu</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
