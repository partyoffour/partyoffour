<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Party</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>

  <?php
    include('session.php');
    include('configr.php');
    if($_SERVER["REQUEST_METHOD"] == "POST") {
      if(isset($_POST['menuItem'])) {
        $id = $_POST['menuItem'];
        $partyID = $_GET['partyID'];
        $sql = "CALL createNewOrderedItem($id, (SELECT ticketID FROM ticket WHERE Party_partyID = $partyID), (SELECT staffID FROM Staff LIMIT 1))";
        if(!$dbr->query($sql)) {
          // header('location: mangae-party.php?message=error2');
          $loc = "location: manage-party.php?partyID=" . $_GET['partyID'] . "&message=error1";
          header($loc);
        } else {
          $loc = "location: manage-party.php?partyID=" . $_GET['partyID'] . "&message=success";
          header($loc);
        }
      } else {
        $tableID = $_GET['tableID'];
        $size = mysqli_real_escape_string($dbr, $_POST['partySize']);
        $sql = "CALL createNewParty($size, $tableID)";
        if($dbr->query($sql)){
          $sql = "SELECT partyID FROM party WHERE Table_tableID = $tableID LIMIT 1";
          $result = mysqli_query($dbr, $sql)->fetch_assoc['partyID'];
          $loc = "location: manage-party.php?partyID=" . $result . "&message=success";
          header($loc);
        }
      }
    }
  ?>

  <script type="text/javascript">
    function addItem(){
      document.getElementById('addItem').hidden = !document.getElementById('addItem').hidden;
    }

    function validate(n){
      if(n==0){
        var re = /^[1-9]{1}\d?$/
        if(!re.test(myform.partySize.value)){
          document.getElementById('error').innerHTML = 'Input a valid party size format: X or XX';
        } else {
          document.getElementById('error').innerHTML = '';
        }
      }
    }
  </script>

  <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
            <li><a href="rest-home.php">Home</a></li>
            
            <li><a href="logout.php">Logout</a></li>
          </ul>
          <!-- <label>Welcome, Owner!</label> -->
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="select-acct.php">My Accounts</a></li>
                    <li><a href="rest-home.php">Restaurant Home</a></li>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <li><a href="logout.php" type="button" class="button">Logout</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">
            <div class="wrap">
              <h1>Manage Party</h1>
            </div>
          </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">



            <?php
              include("configr.php");

              if(isset($_GET['message'])){
                echo "<p>Success!</p>";
              }

              if(isset($_GET['partyID'])){
                $partyID = intval($_GET['partyID']);
                $sql = "SELECT ticketID, orderedItemID, itemName, itemPrice FROM Ticket, OrderedItem, MenuItem WHERE `Party_partyID`=$partyID AND ticketID = `Ticket_ticketID` AND `MenuItem_menuItemID` = menuItemID";
                $result = mysqli_query($dbr,$sql);
                $count = $result;
                // echo $count;
                echo "<table class='hover'>
                        <thead>
                          <th>Item Name</th>
                          <th>Item Price</th>
                          <th>Remove Item</th>
                        </thead>
                        <tbody>";
                while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                  echo "
                  <tr>
                    <td>{$row['itemName']}</td>
                    <td>{$row['itemPrice']}</td>
                    <td><a href='remove-item.php?orderedItemID=" . $row['orderedItemID'] . "&partyID=" . $partyID . "'>Remove Item</a></td>
                  </tr>";
                }
                echo "</tbody>
                </table>";

                echo "<a class='button' onclick='addItem()'>Add Item</a>";
                echo "<a class='button success' href='close-check.php?partyID=" . $partyID . "'>Close Check</a>";
              } else{

                echo "<form name='myform' method='post'>
                        <legend>New Party</legend>
                        <label> Party Size
                          <input type='text' name='partySize' onkeyup='validate(0)'>
                        </label>
                        <small class='error' id='error'></small>
                        <input type='submit' class='button' value='Create New Party' />
                      </form>";

              }
            ?>

            <form id="addItem" hidden="true" method="post">
              <?php
                include("configr.php");
                // setup the table head
                echo"
                <label>Item to remove.</label>
                <select name='menuItem'>";

                $sql = "SELECT menuItemID, itemName FROM menuItem WHERE `Restaurant_restID` = 1 GROUP BY itemName";

                $result = mysqli_query($dbr,$sql);

                while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                  echo "
                  <option value='{$row['menuItemID']}'>{$row['itemName']}</option>
                  ";
                } 
                
                echo "<input type='submit' class='button' value ='Add Item to Check' />
                <a class='button' onclick='addItem()'>Cancel</a>
                  </select>
                </form>"
              ?>
            </form>  

          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <?php
                  $user = $_SESSION['login_user'];
                  echo'<a href="index.php?user=' . $user . '">Home</a>' .
                  '<a href="about.php?user=' . $user . '">Services</a>' .
                  '<a href="contact-us.php?user=' . $user . '">Contact Us</a>';
                ?>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto:c17graham.johnson@usafa.edu"><span>Email</span> c17graham.johnson@usafa.edu</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>