<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Home</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>

  <?php
    include('session.php');
    if($_SERVER["REQUEST_METHOD"] == "POST") {

      if(isset($_POST['zip']) && preg_match("/^\d{5}$/", $_POST['zip'])) {
        $zip = $_POST['zip'];
        $loc = 'location: rest-search-results.php?zip=' . $zip;
        header($loc);
      } else {
        header('location: reserve-rest.php?message=error');
      }
    }
  ?>

  <body>

    <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
          <li><a href="select-acct.php">My Accounts</a></li>
          <li><a href="user-home.php">User Home</a></li>
          <li><a href="logout.php" type="button" class="button">Logout</a></li>
          </ul>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="select-acct.php">My Accounts</a></li>
                    <li><a href="user-home.php">User Home</a></li>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <li><a href="logout.php" type="button" class="button">Logout</a></li>
                    </ul>
                    
                  </div>
                
              </div>
            </div>
          </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">

            <div class="wrap">

              <h1>Create Reservation</h1>

          </div>

        </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">
            <div class="row">
              <div class="medium-6 medium-centered large-4 large-centered columns">
                <form method='post'>
                  <div class="row column log-in-form">
                    <h4 class="text-center">Enter Restaurant Preferences</h4>

                    <?php
                      if(isset($_GET['message'])){
                        echo "<p>Error</p>";
                      }
                    ?>
                    
                    
                <label>Zip
                  <input type="text" name="zip" placeholder="00000">
                </label>

                <label>Search Distance
                  <select>
                    <option value="=5">5 miles</option>
                    <option value="10">10 miles</option>
                    <option value="25">25 miles</option>
                    <option value="50">50 miles</option>
                  </select>
                </label>

                <label>Restaurant Type (Select at least one)</label>
                <table id="employees-table" class="hover">
                  <thead></thead>
                  <tbody>
                    <tr id="row1">
                      <td>American <input id="american" type="checkbox"></td>
                      <td>Barbeque <input id="bbq" type="checkbox"></td>
                      <td>Chinese <input id="chinese" type="checkbox"></td>
                    </tr>
                    <tr id="row2">
                      <td>Mexican <input id="mexican" type="checkbox"></td>
                      <td>Japanese <input id="japanese" type="checkbox"></td>
                      <td>Thai <input id="thai" type="checkbox"></td>
                    </tr>
                    <tr id="row3">
                      <td>Korean <input id="korean" type="checkbox"></td>
                      <td>Vegitarian <input id="vegitarian" type="checkbox"></td>
                      <td>Vegan <input id="vegan" type="checkbox"></td>
                    </tr>
                  </tbody>
                </table>


                <input type="submit" class="button expanded" value="Search">

                   
                  </div>
                </form>

              </div>
            </div>
          </div>
    
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <a href="index.php">Home</a>
                <a href="about.php">Services</a>
                <a href="contact-us.php">Contact Us</a>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="#"><span>Email</span> example@gmail.com</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>

          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
