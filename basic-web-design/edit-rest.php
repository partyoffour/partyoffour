<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Restaurant</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">

    <?php
      include('session.php');
      include('configr.php');

      if($_SERVER["REQUEST_METHOD"] == "POST") {
        $err = 0; $updates = 0;
        $restID = $_SESSION['restID']; // should get the rest id from the session

        if(!empty($_POST['addr']) || !empty($_POST['city']) || !empty($_POST['state']) || !empty($_POST['zip'])) {
          $sql = "SELECT restAddress FROM restaurant WHERE restID = $restID";
          $result = mysqli_query($dbr,$sql)->fetch_assoc()['restAddress'];
          $arr = explode(",", $result);
          $addr = $arr[0];
          $city = $arr[1];
          $state = preg_replace("/[0-9]/", "", $arr[2]);
          $zip = preg_replace("/([a-zA-Z]+|\s)+/", "", $arr[2]);
          
          if(!empty($_POST['addr'])) {
            if(preg_match("/^[1-9]\d+(\s\w+){2,}$/", $_POST['addr'])){
              $addr = $_POST['addr'];
            } else {
              $err = 2;
            }
          }

          if(!empty($_POST['city'])) {
            if(preg_match("/^[A-Z]([a-zA-Z]|\s)+$/", $_POST['city'])){
              $city = $_POST['city'];
            } else {
              $err = 2;
            }
          }

          if(!empty($_POST['state'])) {
            if(preg_match("/^[A-Z]([a-zA-Z]|\s)+$/", $_POST['state'])){
              $state = $_POST['state'];
            } else {
              $err = 2;
            }
          }

          if(!empty($_POST['zip'])) {
            if(preg_match("/^\d{5}$/", $_POST['zip'])){
              $zip = $_POST['zip'];
            } else {
              $err = 2;
            }
          }

          if($err == 0){
            $address = $addr . ', ' . $city . ', ' . $state . ' ' . $zip;
            $address = preg_replace("/^\s{2,}$/", ' ', $address); // remove excess spaces
            if(!$dbr->query("CALL setRestaurantAddress($restID, '$address')")){
              $err = 2;
            } else {
              $updates = $updates + 1;
            }
          }
        }

        if(isset($_POST['rname']) && !empty($_POST['rname'])) {
          if(preg_match("/^[A-Z]([a-zA-Z]|\s)+$/", $_POST['rname'])){
            $rname = mysqli_real_escape_string($db, $_POST['rname']);
            if(!$dbr->query("CALL setRestaurantName($restID, '$rname')")){
              $err = 1;
            } else {
              $updates = $updates + 1;
            }
          } else {
            $err = 2;
          }
        }

        if(isset($_POST['phone']) && !empty($_POST['phone'])) {
          if(preg_match("/^\d{10}$/", $_POST['phone'])){
            $phone = mysqli_real_escape_string($db, $_POST['phone']);
            if(!$dbr->query("CALL setRestaurantPhone($restID, '$phone')")){
              $err = 1;
            } else {
              $updates = $updates + 1;
            }
          } else {
            $err = 2;
          }
        }

        if($err == 0){
          if($updates > 0){
            header("location: edit-rest.php?message=success");
            // echo $address;
          }
        } 
        else {
          if($err == 1){
            header("location: edit-rest.php?message=error1");
            // echo $address;
          } else {
            header("location: edit-rest.php?message=error2");
            // echo $address;
          }
        }

      }
    ?>

  <script type="text/javascript">
    function validate(num) {
      // validates the input with regular expressions and JS.
      switch(num) {
        case 0:
          var re = /^[A-Z]([a-zA-Z]|\s)+$/
          if(!re.test(myform.rname.value)) {
            document.getElementById('rname-error').innerHTML="Please enter a valid restaurant name (Only letters and spaces).";
          } 
          else {
            document.getElementById("rname-error").innerHTML="";
          } break;
        case 1:
          var re = /^[1-9]\d+(\s\w+){2,}$/
          if(!re.test(myform.addr.value)) {
            document.getElementById('addr-error').innerHTML="Please enter a valid address, format: Street Number Street Name.";
          } else {
            document.getElementById("addr-error").innerHTML="";
          } break;
        case 4:
          var re = /^\d{5}$/
          if(!re.test(myform.zip.value)){
            document.getElementById('zip-error').innerHTML="Enter a valid zip code.";
          } else{
            document.getElementById('zip-error').innerHTML="";
          } break;
        case 2:
          var re = /^[A-Z]([a-zA-Z]|\s)+$/
          if(!re.test(myform.city.value)) {
            document.getElementById('city-error').innerHTML="Invalid city name (only letters and spaces).";
          }
          else {
            document.getElementById('city-error').innerHTML="";
          } break;
        case 3:
          /^[A-Z]([a-zA-Z]|\s)+$/
          if( !re.test(myform.state.value)) {
            document.getElementById('state-error').innerHTML="Invalid state name (only letters and spaces).";
          }
          else {
            document.getElementById('state-error').innerHTML="";
          } break;
        case 5:
          var re = /^\d{10}$/
          if( !re.test(myform.phone.value)) {
            document.getElementById('phone-error').innerHTML="Invalid phone number. Format: XXXXXXXXXX";
          }
          else {
            document.getElementById('phone-error').innerHTML="";
          } break;
        default:
          break;
        }
      }
  </script>

  </head>
  <body>

     <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
            <li><a href="select-acct.php">My Accounts</a></li>
            <li><a href="rest-home.php">Restaurant Home</a></li>
            <li><a href="logout.php" type="button" class="button">Logout</a></li>
          </ul>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="select-acct.php">My Accounts</a></li>
                    <li><a href="rest-home.php">Restaurant Home</a></li>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                      <li><a href="logout.php" type="button" class="button">Logout</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">

            <div class="wrap">
              <h1>Update Restaurant</h1>
          </div>

        </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">

          <?php
            if(isset($_GET['message'])){
              if($_GET['message'] == 'error1'){
                echo "<p style='color:red'>Error, something went wrong!</p>";
              } elseif ($_GET['message'] == 'success'){
                echo "<p>Success!</p>";
              } elseif ($_GET['message'] == 'error2') {
                echo "<p style='color:red'>Error, MySQL call failed!</p>";
              }
            }
          ?>

            <?php
              include("configr.php");
              $restID = $_SESSION['restID'];
             
              echo "
              <table class='hover'>
                <thead>
                  <th>Restaurant Name</th>
                  <th>Restaurant Address</th>
                  <th>Restaurant Phone</th>
                </thead>
                <tbody>";

              $sql = "SELECT restName, restAddress, restPhone FROM restaurant WHERE restID = $restID";
              $result = mysqli_query($dbr,$sql);

              while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                echo "
                  <tr>
                    <td>{$row['restName']}</td>
                    <td>{$row['restAddress']}</td>
                    <td>{$row['restPhone']}</td>
                  </tr>
                  ";
              }

              echo "</tbody>
              </table>";
            ?>

            <form method="POST" name="myform">
              <label> New Name
                <input type='text' name='rname' onkeyup="validate(0)">
                <small class="error" id='rname-error'></small>
              </label>

              <label> New Address
                <input type='text' name='addr' onkeyup="validate(1)">
                <small class="error" id='addr-error'></small>
              </label>

              <label> New City
                <input type='text' name='city' onkeyup="validate(2)">
                <small class="error" id='city-error'></small>
              </label>

              <label> New State (US State or Country)
                <input type='text' name='state' onkeyup="validate(3)">
                <small class="error" id='state-error'></small>
              </label>

              <label> New Zip
                <input type='text' name='zip' onkeyup="validate(4)">
                <small class="error" id='zip-error'></small>
              </label>

              <label> New Phone
                <input type='text' name='phone' onkeyup="validate(5)">
                <small class="error" id='phone-error'></small>
              </label>

              <input type="submit" name="submit" value="Submit" class="button">

            </form>


          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <?php
                  $user = $_SESSION['login_user'];
                  echo'<a href="index.php?user=' . $user . '">Home</a>' .
                  '<a href="about.php?user=' . $user . '">Services</a>' .
                  '<a href="contact-us.php?user=' . $user . '">Contact Us</a>';
                ?>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto:c17graham.johnson@usafa.edu"><span>Email</span> c17graham.johnson@usafa.edu</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>