<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Signup</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">

    <script type="text/javascript"></script>
  </head>

  <?php
    error_reporting(E_ERROR);

    include("config.php");
    
    session_start();

    if($_SERVER["REQUEST_METHOD"] == "POST") {

      if(empty($_POST['code'])){
        header("location: ssignup.php?error");
      }

      $code = mysqli_real_escape_string($db, $_POST['code']);

      if($code === $_SESSION['confirm_code']){
        // pull info from session
        $username = $_SESSION['login_user'];
        $password = $_SESSION['login_pass'];
        $fname = $_SESSION['login_fname'];
        $lname = $_SESSION['login_lname'];
        $email = $_SESSION['login_email'];
        $_SESSION['login_ip'] = getRealIpAddr();

        if(!$db->query("CALL createNewUser('$username', '$password', '$fname', '$lname', '$email')")) {
          // header("location: submitted-user.php?error=1");
          echo "CALL failed: (" . $db->errno . ") " . $db->error;
        } 
        else {
          header("location: select-acct.php");
        }
      }
      else {
        header("location: index.php");
      }

    }

    function getRealIpAddr()
       {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
          $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
          $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
          $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
       }
  ?>

  <body>

    <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
            <li><a href="index.php">Cancel</a><li>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <!-- <li><a href="index.php">Home</a.</li> -->
                    <li><a href="index.php">Cancel</a></li>
                </div>                
              </div>
            </div>
          </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">
            <div class="wrap">
              <h1>Account Created</h1>
            </div>
          </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
          <div class="wrap row">
            <div class="row">
              <div class="medium-6 medium-centered large-4 large-centered columns">
                <div class="row column log-in-form">
                  <h4 class="text-center">Please confirm your email address with the passcode sent to you.</h4>
                  <?php
                    $passcode = $_SESSION['confirm_code'];
                    echo "<p> Test, no email sent, the code is: '$passcode'";
                  ?>
                
                 <form action="" method="post">
                    <label>Code</label>
                    <input type="text" name="code" placeholder="code">
                    <input type="submit" class="button expanded" value ="Submit"></input>
                  </form>
                    <p class="text-center"><a href="user-signup.php">Send it Again?</a></p>
                </div>
              </div>
            </div>
          </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <?php
                  $user = $_SESSION['login_user'];
                  echo'<a href="index.php?user=' . $user . '">Home</a>' .
                  '<a href="about.php?user=' . $user . '">Services</a>' .
                  '<a href="contact-us.php?user=' . $user . '">Contact Us</a>';
                ?>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto:c17graham.johnson@usafa.edu"><span>Email</span> c17graham.johnson@usafa.edu</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>