
<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Party of Four</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>
  <body>

  <?php
    include('session.php');
    // test callback using POST

    include('session.php'); // include the rest's session
    include('configr.php'); // include the database

    if($_SERVER["REQUEST_METHOD"] == "POST"){
      $error = 0;
      if(empty($_POST['hours'])){
        // header('location: data-disp.php');
        $error = 1;
      }
      if(empty($_POST['partySize'])){
        // header('location: data-disp.php');
        $error = 1;
      }
      if(empty($_POST['minutes'])){
        // header('location: data-disp.php');
        $error = 1;
      }
      // validate the input, check type is int and the value is positive
      if(!is_numeric($_POST['hours'])) {
        // header('location: data-disp.php');
        $error = 2;
      }
      if(!is_numeric($_POST['partySize'])) {
        // header('location: data-disp.php');
        $error = 2;
      }
      if(!is_numeric($_POST['minutes'])) {
        // header('location: data-disp.php');
        $error = 2;
      }

      if($error == 0) {



        // validate the input, sanitize for injections
        $hours = mysqli_real_escape_string($dbr, $_POST['hours']);
        $minutes = mysqli_real_escape_string($dbr, $_POST['minutes']);
        $partySize = mysqli_real_escape_string($dbr, $_POST['partySize']);

        $pyscript = '"C:\\rests\\C17Austin.McWhirter\\Desktop\\PartyOfFour\\Austin Test Code\\web_callback.py"';
        $python = 'C:\\Python34\\python.exe';
        $args = "$val1,$val2,$val3";
        $cmd = "$python $pyscript $args";
        exec("$cmd", $results);

        // echo ($results[0]);
        // $_SESSION['callbackResult'] = $results; // pass the results back in the session
        header("location: data-disp.php?result='$results'");

      } 
      else {
        if ($error == 1){
          header("location: data-disp.php?error=1"); // head back to page with an error
        } else {
          header("location: data-disp.php?error=2"); // head back to page with an error
        }

      }
    }

  ?>

    <script type="text/javascript">
    </script>

    <div class="off-canvas-wrap">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
          <ul>
            <li><a href="rest-home.php">Restaurant Home</a></li>
          </ul>
          <li><a href="logout.php">Logout</a></li>
        </div>

        <div class="off-canvas-content" data-off-canvas-content>

    <!-- ******************* MOBILE NAVIGATION  ************************************-->
          <div class="title-bar nav-mobile"> 
            <div class="title-bar-left">
              <button class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
              <span class="title-bar-title">Party of Four</span>
            </div>
          </div>
          
    <!-- **************************** DESKTOP NAVIGATION ************************* -->
        
          <div class="top-bar nav-desktop">
            <div class="wrap">
              <div class="top-bar-title">
                <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
                  <button class="menu-icon dark" type="button" data-toggle></button>
                </span>
                <h3 class="site-logo">Party of Four</h3>
              </div>
              <div id="responsive-menu">
                <div class="top-bar-left">
                  <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="rest-home.php">Restaurant Home</a></li>
                    <li><a href="logout.php" type="button">Logout</a></li>
                  </ul>
                </div>
                  <div class="top-bar-right">
                    <ul class="menu menu-desktop">
                    </ul>
                  </div>        
              </div>
            </div>
          </div>

          <!-- ************************ HERO ***************************************** -->

          <section class="hero">
            <div class="wrap">
                <h1>Statistics</h1>
            </div>
          </section>

        <!--  ************************ MAIN ****************************************** -->

        <section class="main"> 
        <div class="wrap">

          <?php
              if(isset($_GET['error'])){
                if($_GET['error'] == '1'){
                  echo "<p style='color:red'>Error, please fill in all values.</p>";
                } else {
                  echo "<p style='color:red'>Error, at leadt one of your values wasn't numeric.</p>";
                }
              }
          ?>

          <form action="" method="post">
            <label> Hours
            <input type="text" name="hours" />
            </label>
            <label> Minutes
            <input type="text" name="minutes" />
            </label>
            <label> Party Size
            <input type="text" name="partySize" />
            </label>
            <input type="submit" name="Send" class="button" />
          </form>

        <?php
          if(isset($_GET['result']) && !empty($_GET['result'])) {
            echo "<p> Result from previous callback: " . $_GET['result'] .'</p>';
            // unset($_SESSION['callbackResult']); // unset the callback result
            
          }
        ?>
          
        </div>
        </section>

          <!-- ************************ FOOTER *************************************** -->

          <section class="footer">
            <div class="wrap row small-up-1 medium-up-3">
              <div class="medium-2 small-12 column">
                <h4>Site Map</h4>
                <hr>
                <a href="index.php">Home</a>
                <a href="about.php">Services</a>
                <a href="contact-us.php">Contact Us</a>
              </div>
              <div class="medium-8 small-12 column">
                <h4>Contact Info</h4>
                <hr>
                <a href="#"><span>Phone</span> 123 456 7890</a>
                <a href="mailto-example.php"><span>Email</span> example@gmail.com</a>
                <a href="#"><span>Address</span> 2360 Vandenberg Dr.</a>
              </div>
              <div class="medium-2 small-12 column">
                <h4>Social Media</h4>
                <hr>
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
              </div>
            </div>

          </section>
        </div>
      </div>
    </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
