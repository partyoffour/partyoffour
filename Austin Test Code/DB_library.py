#!/usr/bin/python
import tabulate
import MySQLdb as mdb
from shutil import copyfile
import datetime



class PBrainDB:

        def __init__(self,host,username):
                self.host = host
                self.username=username
                with open('password','r') as passwd:
                        try:
                                self.con = mdb.connect(host,username,passwd.readline(),'party_of_four_restaurant')
                                self.DBobj = self.con.cursor()
                        except:
                                raise
                        

        def printTickets(self):
                '''Prints all current open Tickets in the database
                Input: Database Object
                '''
                
                self.DBobj.execute("select * from Ticket")
                desc = self.DBobj.description
                for i in self.DBobj.description:
                        print(i[0], end='\t')
                print('\n')
                for i in self.DBobj.fetchall():
                        print(i[0],'\t',i[1].strftime("%Y-%m-%d"),'\t',i[2].strftime("%Y-%m-%d"))

        def printOrders(self):
                '''Prints all current orders in the database with relevant information
                Input: Database Object
                Output: Query Results
                '''
                    
                self.DBobj.execute("select t.*, o.orderTime,o.deliverTime,o.cookSTime,o.cookETime,m.itemEatTime,m.itemDeliverTime,m.itemCookTime,m.itemName from Ticket t inner join OrderedItem o on t.ticketID=o.Ticket_ticketID inner join menuItem m on o.MenuItem_menuItemID=m.menuItemID")
                headers=[text[0] for text in self.DBobj.description]
                formattedData = []
                    
                for i in self.DBobj.fetchall():
                    if None not in i:
                            data = ( i[0],i[1].strftime("%H:%M:%S"),i[2].strftime("%H:%M:%S"),i[3].strftime("%H:%M:%S"),i[4].strftime("%H:%M:%S"),i[5].strftime("%H:%M:%S"),i[6].strftime("%H:%M:%S"),self.timedelta_to_str(i[7]),self.timedelta_to_str(i[8]),self.timedelta_to_str(i[9]),i[10])
                            formattedData.append(data)
                            
                print(tabulate.tabulate(formattedData,headers=headers))
                return formattedData

        def getMaxCookTime(self,ticketID):
                '''Prints the maximum cook time out of all items on a ticket
                Input: Database Object, TicketID
                Output: Max cook time
                '''
                
                self.DBobj.execute('select max(i.itemCookTime) from OrderedItem o inner join MenuItem i on o.orderedItemID = i.menuItemID where o.Ticket_ticketID = ' + str(ticketID))
                result = self.timedelta_to_str(self.DBobj.fetchone()[0])
                print(result)
                return result

        def timedelta_to_str(self,obj):
            if obj is not None:
                    seconds = int(obj.seconds) % 60
                    minutes = int(obj.seconds / 60) % 60
                    hours = int(obj.seconds / 3600) % 24
                    return '%02d:%02d' % (minutes, seconds)
            else:
                    print("Cannot format None as time")

        def getMenuItemDeliverTime(self,item):
                '''Prints the time it takes to deliver an item
                Input: Database Object, item name
                Output: Delivery time
                '''
                
                self.DBobj.execute('Select itemDeliverTime from MenuItem where itemName like \'%'+item+'%\' limit 1')
                result= self.DBobj.fetchall()[0][0]
                print(result)
                return result

        def getCountItems(self, ticketID):
                '''Prints the number of ordered items on a ticket
                Input: Database Object, TicketID
                Output: Query result
                '''
                
                self.DBobj.execute('select max(i.itemCookTime) from OrderedItem o inner join MenuItem i on o.orderedItemID = i.menuItemID where o.Ticket_ticketID = '+str(ticketID))
                result= self.DBobj.fetchall()[0][0]
                print(result)
                return result

        def getMaxEatTime(self,ticketID):
                '''Prints the maximum predicted time a to eat all items on a ticket
                Input: Database Object, TicketID
                Output: Max eat time
                '''
                
                self.DBobj.execute('select max(i.itemEatTime) from OrderedItem o inner join MenuItem i on o.orderedItemID = i.menuItemID where o.Ticket_ticketID = ' + str(ticketID))
                result = self.timedelta_to_str(self.DBobj.fetchone()[0])
                print(result)
                return reversed

        def customQuery(self,query):
                '''Allows custom queries to the database ***SECURITY CONCERN - MAKE SURE ALL QUERIES ARE PROPERLY SANITIZED*** '''
                self.DBobj.execute(query)
                result = self.DBobj.fetchall()
                for i in result:
                        print(i)
                return result
        

        def createData(self):
                '''If we're creating a dataset for the server make sure we have a backup'''
                try:
                        copyfile('CurrentData.csv','DataBackup.csv')
                        copyfile('CurrentSimpleData.csv','SimpleDataBackup.csv')
                except:
                        print("Failed to make backups")
                self.DBobj.execute('call getData();')
                data = self.DBobj.fetchall()
                self.formatData(data)
                

        def formatData(self,data):
                
                with open('CurrentData.csv','w') as curFile:
                        with open('CurrentSimpleData.csv','w') as curSimpleFile:
                                curFile.write('TimeOfDay,LongestCookTime,PartySize,TicketActualTime\n')
                                curSimpleFile.write('TimeOfDay,PartySize,TicketActualTime\n')
                                
                                for i in data:
                                        line =''
                                        simpleline=''
                                        line+= "None" if i[1] is None else str(i[1].hour)
                                        line+=','
                                        simpleline+=line
                                        line+= "None" if i[3] is None else self.timedelta_to_str(i[3])[3:]
                                        line+=','
                                        line+="None" if i[4] is None else str(i[4])
                                        line+=','
                                        simpleline+="None" if i[4] is None else str(i[4])
                                        simpleline+=','
                                        line+="None" if i[2] is None or i[1] is None else str((i[2]-i[1]).seconds/float(60))
                                        line+='\n'
                                        if i[2] is None or i[1] is None :
                                                simpleline+="None"
                                        elif (i[2]-i[1]).seconds/float(60) == 0:
                                                simpleline+="None"
                                        else:
                                                simpleline+=str((i[2]-i[1]).seconds/float(60))   
                                        simpleline+='\n'
                                        curFile.write(line)
                                        curSimpleFile.write(simpleline)
                
        def addPrediction(self,ticketId,predictedTime):
                '''Add prediction to the database for supervised dataset'''
                #query = 'Select * from Ticket limit 10'
                #self.DBobj.execute(query)
                #results = self.DBobj.fetchall()
                #for i in results:
                #       print(i)

                #input()
                query = 'update Ticket set `ticketPredictedTime` = '+str(int(predictedTime)*60)+' where `ticketId`= ' + str(ticketId)+';'
                print(query)
                self.DBobj.execute(query)
                self.con.commit()
                

        def getWaitTime(self,arrive):
                time = arrive
                query = 'Select DATE_ADD(ticketSTime, INTERVAL ticketPredictedTime second),ticketPredictedTime from Ticket t left join Party p on p.PartyID = t.party_PartyID where t.ticketActualTime = 0 and t.ticketSTime <=\'' +str(time) + '\'  and t.ticketPredictedTime <> 0 and DATE(t.ticketSTime) = DATE(\''+str(time)+'\') and DATE_ADD(ticketSTime, INTERVAL ticketPredictedTime second) > \''+str(time)+'\' order by (t.ticketSTime+t.ticketPredictedTime) ASC limit 1'
                #print(query)

                results = self.DBobj.execute(query)
                if results > 0:
                        predtime =  self.DBobj.fetchall()[0]
                        
                        return predtime[0]
                else:
                        return 0
                
                
                
if __name__ == '__main__':


    print("library file")
