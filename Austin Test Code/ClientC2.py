#!/usr/bin/python
import NN_library
import sys
import json


def read_in():
    line = sys.stdin.readline()
    return(line.strip().split(','))



def main():
    try:
        net = NN_library.NNload(Op=True)
    except:
        print("Loading the Neural Network failed. Make sure you have a Current.pkl file")

        
    lines = read_in()
    
    return net.predict(lines)

    
if __name__=="__main__":
    main()
