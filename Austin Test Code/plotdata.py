from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import csv



fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

hFile = open("CurrentSimpleData.csv", 'r')
datfile = csv.reader(hFile)
x=[]
y=[]
z=[]

for row in datfile:
    if "None" not in row and row[0] != 'TimeOfDay':
        x.append(int(float(row[0])))
        y.append(int(float(row[1])))
        z.append(int(float(row[2])))


ax.scatter(x, y, z, c='r', marker='o')

ax.set_xlabel('Time Of Day')
ax.set_ylabel('Party Size')
ax.set_zlabel('Ticket Time')

plt.show()