import numpy as np
import pandas as pd
import math
import time
import pickle
import glob
import sys,os


from pybrain.tools.shortcuts import buildNetwork
from pybrain.structure import TanhLayer,SigmoidLayer,LinearLayer,FeedForwardNetwork,FullConnection
from pybrain.supervised.trainers import BackpropTrainer,RPropMinusTrainer
from pybrain.datasets import SupervisedDataSet
from pybrain.structure.modules import *
from pybrain.tools.neuralnets import NNregression,Trainer


def NNsave(net):
    filename = input("What filename would you like?:\n")
    if len(filename)> 25:
        print("Filename too long")
        
    else:
        try:
            filename=filename.split('.')[0].strip()
        except:
            pass

        for i in filename:
            if not i.isalnum():
                print("Filename contains invalid characters. Please only use alpha-numeric characters.")
                exit
        
        with open(filename+'.pkl', 'wb') as handle:
          pickle.dump(net, handle)
          return "Saved as: " + filename + ".pkl"


def NNdelete():
    filelist = glob.glob('./*.pkl')
    print("Which Neural Net would you like to delete? (just type the number or \"exit\")")
    for i in range(len(filelist)):
        print(str(i+1)+') ' + filelist[i][2:],end='\n')
    load = ''
    while load=='':
        load= input()
        if load=="exit" or load=="quit":
            sys.exit(0)
        else:    
            try:
                load=int(load)
            except ValueError:
                print("Please type a number")
                load=''
            if not (load=='') and (load <=0 or load>len(filelist)):
                print("Please select a valid number")
                raise ValueError
            try:
                answer = ''
                while answer=='':
                    answer = input("Are you sure you wish to delete " + filelist[load-1][2:]+" (Y/N)\n")
                    if answer.lower() == 'y':
                        os.remove(filelist[load-1])
                    else:
                        exit
                
            except:
                raise

def NNload():
    filelist = glob.glob('./*.pkl')
    print("Which Neural Net would you like to load? (just type the number or \"exit\")")
    for i in range(len(filelist)):
        print(str(i+1)+') ' + filelist[i][2:],end='\n')
    load = ''
    while load=='':
        load= input()
        if load=="exit" or load=="quit":
            sys.exit(0)
        else:
            try:
                load=int(load)
            except ValueError:
                print("Please type a number")
                load=''
            if not(load=='') and (load <=0 or load>len(filelist)):
                print("Please select a valid number")
                raise ValueError
            try:
                with open(filelist[load-1], 'rb') as handle:
                  net=pickle.load(handle)
            except:
                print("error")
                raise
    return net

answer =''
while answer == '':
    answer = input("Would you like to (Load) or (Delete) a Neural Network or (Exit)\n")
    if answer.lower() == 'load':
        try:
            net = NNload()
        except:
            print("Error loading the file. Please try again.")
            sys.exit(0)
    elif answer.lower() == 'delete':
        try:
            NNdelete()
            answer=''
        except:
            print("File not deleted. Please try again.")
            sys.exit(0)
    elif answer.lower() == 'exit' or answer.lower() == 'quit':
        sys.exit(0)
    else:
        answer=''

def main():  
    test=''
    print("\n\nNOTE: Type 'save' to save the AI or 'exit' to quit.")

    while(1):
        test=str(input("\n\nGive cylinders, horsepower and acceleration for a prediction:\n"))
        if test.lower() == "save":
            print(NNsave(net))
        elif test.lower() == "exit" or test.lower()=="quit":
            sys.exit(0)
        else:
            try:
                    testvals = test.split()

                    value = net.activate((testvals[0],testvals[1],testvals[2]))[0]
                    try:
                        truevalue=float(testvals[3])
                        print("Predicted Value: "+str(round(value,2)), "Percent difference: " +str(round((truevalue-value)/float(truevalue)*100,2)))
                    except:
                        print("Predicted Value: "+str(round(value,2)))
            except:
                print("Something went wrong, try again.")


if __name__ == "__main__":
    main()
