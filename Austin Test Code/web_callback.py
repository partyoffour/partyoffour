#!/usr/bin/python
import NN_library
import DB_library
import sys
import json
import socket
from urllib.parse import urlparse
from urllib.parse import parse_qs
import datetime

DEBUG = True
def main():
    HOST = 'localhost'
    PORT = 2017

    DB = DB_library.PBrainDB('localhost','root')

    try:
        net = NN_library.NNload(Op=True)
    except:
        print("Loading the Neural Network failed. Make sure you have a Current.pkl file")
        ex(0)

    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

    try:
        s.bind((HOST,PORT))
        print("Server bound to " +HOST+":" + str(PORT)+"\n")
        print("Server using: \n"+str(net))
    except:
        print("Failed to bind")
        ex(0)
    
    s.listen(10)
    while(1):
        try:
            conn,addr = s.accept()
            
            if addr[0] != "127.0.0.1":
                response = ("HTTP/1.1 403 Access Denied\n"+"Content-Type: text/html\n"
             +"\n"
             +"<h1>Access Denied</h1>")
                conn.send(bytes(response,'utf8'))
            else:
                
                lines = conn.recv(1024)
                output = urlparse(str(lines))
                params = parse_qs(output.query[0:output.query.index(' ')])

                val1 = params.get('var1')
                val2 = params.get('var2')
                val3 = params.get('var3')
                if None in (val1,val2,val3):
                    predict = "Invalid Inputs"
                else:
                    values = val1[0]+","+val2[0]+","+val3[0]
                    predict = net.predict(values)
                    lastTicketTime = DB.getSTime()
                    
                    now = datetime.datetime.now()
                    time = now.hour,now.minute,now.second
                    timePassed = (time[0]-lastTicketTime[0])*60 + (time[1]-lastTicketTime[1]) + float(time[2]-lastTicketTime[2])/float(60)
                try:
                    response = ("HTTP/1.1 200 OK\n"+"Content-Type: text/html\n"
                     +"\n"
                     +str(round(float(predict)-timePassed,2)))
                except:
                    response = ("HTTP/1.1 200 OK\n"+"Content-Type: text/html\n"
                     +"\n"
                     +str(predict))
                conn.send(bytes(response,'utf8'))

            conn.close()

            if DEBUG:
                break
        except:
            try:
                conn.close()
            except:
                pass
            
    print("unbinding server")
    s.close()
    return       

    
if __name__=="__main__":
    
    main()

