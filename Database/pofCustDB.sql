SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=`TRADITIONAL`;

CREATE SCHEMA IF NOT EXISTS `party_of_four_customer`;
USE `party_of_four_customer`;

# ------------------------------------------------------------------------------------------------------------------------------------- #
#														Create all tables																#
# ------------------------------------------------------------------------------------------------------------------------------------- #

# ----------------------------------------------------#
#	Table `party_of_four`.`User`					  #
# ----------------------------------------------------#
DROP TABLE IF EXISTS `party_of_four_customer`.`User`;
CREATE TABLE IF NOT EXISTS `party_of_four_customer`.`User` (
	`userName` VARCHAR(30) UNIQUE NOT NULL,
    `userFName` VARCHAR(50),
    `userLName` VARCHAR(50),
    `userEmail` VARCHAR(250),
    PRIMARY KEY (`userName`))
ENGINE = InnoDB;

# ----------------------------------------------------#
#	Table `party_of_four`.`Permissions`				  #
# ----------------------------------------------------#
DROP TABLE IF EXISTS `party_of_four_customer`.`Permissions`;
CREATE TABLE IF NOT EXISTS `party_of_four_customer`.`Permissions` (
	`User-userName` VARCHAR(30) UNIQUE NOT NULL,
    `restID` INT NOT NULL,
    `permissions` INT NOT NULL,
    PRIMARY KEY (`User-userName`, `restID`),
     INDEX `fk_User-Permissions` (`User-userName` ASC),
    CONSTRAINT `fk_User-Permissions`
		FOREIGN KEY(`User-userName`)
        REFERENCES `party_of_four_customer`.`User` (`userName`)
		ON DELETE CASCADE
        ON UPDATE CASCADE)
ENGINE = InnoDB;

# ----------------------------------------------------#
#	Table `party_of_four`.`PassHashes`				  #
# ----------------------------------------------------#
DROP TABLE IF EXISTS `party_of_four_customer`.`PassHashes`;
CREATE TABLE IF NOT EXISTS `party_of_four_customer`.`PassHashes` (
	`User-userName` VARCHAR(30) UNIQUE NOT NULL,
    `passSalt` VARCHAR(32),
	`passHash` VARCHAR(73) UNIQUE NOT NULL,
    PRIMARY KEY (`passHash`),
    INDEX `fk_User-PassHashes` (`User-userName` ASC),
    CONSTRAINT `fk_User-PassHashes`
		FOREIGN KEY(`User-userName`)
        REFERENCES `party_of_four_customer`.`User` (`userName`)
		ON DELETE CASCADE
        ON UPDATE CASCADE)
ENGINE = InnoDB;

# ------------------------------------------------------------------------------------------------------------------------------------- #
#														Populate all tables	with example data											#
# ------------------------------------------------------------------------------------------------------------------------------------- #

# ----------------------------------------------------#
#	Populate User with example data					  #
# ----------------------------------------------------#

/*All User table information must be populated through the createNewUser procedure in order to properly
  populate PassHashes as well*/
  
# ----------------------------------------------------#
#	Populate PassHashes with example data			  #
# ----------------------------------------------------#

/*Same as populating User*/

# ------------------------------------------------------------------------------------------------------------------------------------- #
#														Create all procedures&functions													#
# ------------------------------------------------------------------------------------------------------------------------------------- #

# ------------------------------------------------------------------------------------------#
#					Procedures&functions for User											#
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Function makeSalt								  #
#		Creates new 32 character salt for hashing 	  #
# ----------------------------------------------------#

DELIMITER //
CREATE FUNCTION makeSalt()
	RETURNS VARCHAR(32)
	BEGIN
		DECLARE salt VARCHAR(32);
        SET salt=concat(
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1),
              substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1), substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0abcdefghijklmnopqrstuvwxyz123456789', floor(rand()*62+1), 1)
             );
		RETURN salt;
	END//
DELIMITER		
		
# ----------------------------------------------------#
#	Procedure createNewUser							  #
#		Creates new user						 	  #
# ----------------------------------------------------#

DELIMITER //
CREATE PROCEDURE createNewUser(newName VARCHAR(30), newPass VARCHAR(50), newFName VARCHAR(50), newLName VARCHAR(50), newEmail VARCHAR(250))
	BEGIN
		DECLARE mySalt VARCHAR(40);
		SET @mySalt := (makeSalt());
		INSERT INTO User (`userName`, `userFName`, `userLName`, `userEmail`)
		VALUES (newName, newFName, newLName, newEmail);
        INSERT INTO PassHashes (`User-userName`, `passSalt`, `passHash`)
        VALUES (newName, @mySalt, REPLACE(PASSWORD(concat(@mySalt, newPass)), '*', ''));
	END//
DELIMITER ; 

# ----------------------------------------------------#
#	Procedure deleteUser							  #
#		Deletes existing user					 	  #
# ----------------------------------------------------#

DELIMITER //
CREATE PROCEDURE deleteUser(loginName VARCHAR(30))
	BEGIN
		DELETE FROM User WHERE `userName` = loginName;
        DELETE FROM PassHashes WHERE `User-userName` = loginName;
	END//
DELIMITER ;

# ----------------------------------------------------#
#	Procedure setUserFName							  #
#		Updates user first name					 	  #
# ----------------------------------------------------#

CREATE PROCEDURE setUserFName(loginName VARCHAR(30), newFName VARCHAR(50))
	UPDATE User SET `userFName` = newFName WHERE `userName` = loginName;

# ----------------------------------------------------#
#	Function getUserFName							  #
#		Gets user first name					 	  #
# ----------------------------------------------------#

CREATE FUNCTION getUserFName(loginName VARCHAR(30))
	RETURNS VARCHAR(50)
		
    RETURN (SELECT `userFName` FROM User WHERE `userName` = loginName);

# ----------------------------------------------------#
#	Procedure setUserLName							  #
#		Updates user last name					 	  #
# ----------------------------------------------------#

CREATE PROCEDURE setUserLName(loginName VARCHAR(30), newLName VARCHAR(50))
	UPDATE User SET `userLName` = newLName WHERE `userName` = loginName;

# ----------------------------------------------------#
#	Function getUserLName							  #
#		Gets user last name						 	  #
# ----------------------------------------------------#

CREATE FUNCTION getUserLName(loginName VARCHAR(30))
	RETURNS VARCHAR(50)
		
    RETURN (SELECT `userLName` FROM User WHERE `userName` = loginName);

# ----------------------------------------------------#
#	Procedure setUserEmail							  #
#		Updates user email address				 	  #
# ----------------------------------------------------#

CREATE PROCEDURE setUserEmail(loginName VARCHAR(30), newEmail VARCHAR(250))
	UPDATE User SET `userEmail` = newEmail WHERE `userName` = loginName;

# ----------------------------------------------------#
#	Function getUserEmail							  #
#		Gets user email address					 	  #
# ----------------------------------------------------#

CREATE FUNCTION getUserEmail(loginName VARCHAR(30))
	RETURNS VARCHAR(250)
		
    RETURN (SELECT `userEmail` FROM User WHERE `userName` = loginName);

# ------------------------------------------------------------------------------------------#
#					Procedures&functions for PassHashes										#
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Function validateLogin							  #
#		Validates password of user				 	  #
# ----------------------------------------------------#

DELIMITER //
CREATE FUNCTION validateLogin(loginName VARCHAR(30), loginPass VARCHAR(50))
	RETURNS BOOL
    BEGIN
		DECLARE myPassHash VARCHAR(72);
        DECLARE myCompare VARCHAR(72);
		SET @myPassHash := (SELECT `passHash` FROM PassHashes WHERE `User-userName` = loginName);
        SET @myCompare := REPLACE(PASSWORD(concat((SELECT `passSalt` FROM PassHashes WHERE `User-userName` = loginName), loginPass)), '*', '');
		IF  @myPassHash = @myCompare THEN
			RETURN TRUE;
		ELSE
			RETURN FALSE;
		END IF;
	END//
DELIMITER ;

# ----------------------------------------------------#
#	Function changePassword							  #
#		Creates new password					 	  #
# ----------------------------------------------------#

DELIMITER //
CREATE PROCEDURE changePassword(loginName VARCHAR(30), loginPass VARCHAR(50), newPass VARCHAR(50))
    BEGIN
		IF  validateLogin(loginName, loginPass) THEN
			SET @mySalt := (makeSalt());
			UPDATE PassHashes SET `passSalt` = @mySalt WHERE `User-userName` = loginName;
            UPDATE PassHashes SET `passHash` = REPLACE(PASSWORD(concat(@mySalt, newPass)), '*', '') WHERE `User-userName` = loginName;
		ELSE
			SELECT 'Incorrect Password!';
        END IF;
	END//
DELIMITER ;

# ------------------------------------------------------------------------------------------------------------------------------------- #
#														Create all Test Cases															#
# ------------------------------------------------------------------------------------------------------------------------------------- #

# ------------------------------------------------------------------------------------------#
#					Test Cases for User														#
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Function makeSalt Test Case						  #
#		Should return unique 32 character salt	 	  #
# ----------------------------------------------------#

#Call the function and display it as variable 'mySalt'
/*SELECT makeSalt() as mySalt;
SELECT makeSalt() as mySalt;
SELECT makeSalt() as mySalt;
SELECT makeSalt() as mySalt;
SELECT makeSalt() as mySalt;*/

# ----------------------------------------------------#
#	Procedure createNewUser	Test Case				  #
#		Should add user info to `User` and hash the	  #
#		password and put the hash and salt into		  #
#		`passHashes`							 	  #
# ----------------------------------------------------#
CALL createNewUser('gm@mycal.net', 'Graham@17', 'Graham', 'Johnson', 'gm@mycal.net');
#Call procedure createNewUser with several different user inputs
/*CALL createNewUser('RSPownage', 'Password', 'Graham', 'Johnson', 'IAmGraham@gmail.com');
CALL createNewUser('SCRUMLord3000', 'myPassword', 'Kristopher', 'Fortier', 'KristmasCameEarly@gmail.com');
CALL createNewUser('dannyboy17', '123456', 'Daniel', 'Kohn', 'dannyboy17@gmail.com');
CALL createNewUser('ImaAskewAFavor', '09876', 'Casen', 'Askew', 'CasenPoint@gmail.com');
CALL createNewUser('McWhirterIBarelyKnowHer', '2017RULES', 'Austin', 'McWhirter', 'McWhirterAI@gmail.com');*/

#Display data in `User` and `PassHashes`; should include all info from procedure calls on lines 267-271
#Additionally, should include all info in `PassHashes`
/*SELECT * FROM User;
SELECT * FROM PassHashes;*/

#MARK!!!!!: Because the username is the primary key, attempting to create new users
#with existing usernames throws an error; note that only userName 
#remains the same in the call below (must run line 267 as well)
/*CALL createNewUser('RSPownage', 'newPassword', 'gajkaGraham', '3415Johnson', 'gaadIAmGraham@gmail.com');*/


# ----------------------------------------------------#
#	Procedure deleteUser Test Case					  #
#		Should delete existing users,				  #
#		including info in `PassHashes`				  #
# ----------------------------------------------------#

#Call the procedure using primary key, userName
/*CALL deleteUser('McWhirterIBarelyKnowHer');*/

#Display data from `User` and `PassHashes`, should not include info for
#'McWhirterIBarelyKnowHer' anymore; note that line 271 must be called first
/*SELECT * FROM User;
SELECT * FROM PassHashes;*/

#MARK!!!!!: Trying to delete info for a user that does not exist 
#does NOT break the program; just use same call as above
/*CALL deleteUser('McWhirterIBarelyKnowHer');
SELECT * FROM User*/

# ----------------------------------------------------#
#	Procedure setUserFName Test Case	 			  #
#		Should update user first name of user	 	  #
# ----------------------------------------------------#

#Call the procedure using the userName and new first name
/*CALL setUserFName('RSPownage', 'Graaam');*/

#Display data from `User`, should have new name from 'RSPownage';
#note that line 267 must be called first
/*SELECT * FROM User;*/

#MARK!!!!!: Trying to change the first name of a non-existent
#user does NOT break the program
/*CALL setUserFName('IDontExist!', 'OhNo!');*/

# ----------------------------------------------------#
#	Function getUserFName Test Case					  #
#		Returns user first name based on user name 	  #
# ----------------------------------------------------#

#Call the function using the username;
#MARK!!!!!: note that getting the first name
#of non-existent user does NOT break the program
/*SELECT getUserFName('dannyboy17') as name;
SELECT getUserFName('INeverExisted') as name;*/

# ----------------------------------------------------#
#	Procedure setUserLName Test Case	 			  #
#		Should update user last name of user	 	  #
# ----------------------------------------------------#

#Call the procedure using the userName and new last name
/*CALL setUserLName('RSPownage', 'Smith');*/

#Display data from `User`, should have new name from 'RSPownage';
#note that line 267 must be called first
/*SELECT * FROM User;*/

#MARK!!!!!: Trying to change the last name of a non-existent
#user does NOT break the program
/*CALL setUserLName('IDontExist!', 'OhNo!');*/

# ----------------------------------------------------#
#	Function getUserLName Test Case					  #
#		Returns user last name based on user name 	  #
# ----------------------------------------------------#

#Call the function using the username;
#MARK!!!!!: note that getting the last name
#of non-existent user does NOT break the program
/*SELECT getUserLName('dannyboy17') as name;
SELECT getUserLName('INeverExisted') as name;*/

# ----------------------------------------------------#
#	Procedure setUserEmail Test Case	 			  #
#		Should update user email of user		 	  #
# ----------------------------------------------------#

#Call the procedure using the userName and new email
/*CALL setUserEmail('RSPownage', 'myNewEmail@yahoo.com');*/

#Display data from `User`, should have new email from 'RSPownage';
#note that line 267 must be called first
/*SELECT * FROM User;*/

#MARK!!!!!: Trying to change the email of a non-existent
#user does NOT break the program
/*CALL setUserFName('IDontExist!', 'OhNo!@msn.com');*/

# ----------------------------------------------------#
#	Function getUserEmail Test Case					  #
#		Returns user email based on user name	 	  #
# ----------------------------------------------------#

#Call the function using the username; 
#MARK!!!!!: note that getting the email 
#of non-existent user does NOT break the program
/*SELECT getUserEmail('dannyboy17') as email;
SELECT getUserEmail('INeverExisted') as email;*/

# ------------------------------------------------------------------------------------------#
#					Test Cases for PassHashes												#
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Function validateLogin Test Case				  #
#		Returns bool determined by password input 	  #
# ----------------------------------------------------#

#Call function using username and correct password
/*SELECT validateLogin('dannyboy17', '123456') as valid;*/

#Call function using username and incorrect password
/*SELECT validateLogin('dannyboy17', 'wrongPass') as valid;*/

#MARK!!!!!: note that validating the login of a non-existent user
#does NOT break the program and returns FALSE
/*SELECT validateLogin('ISwearIExist', 'passWord') as valid;*/

# ----------------------------------------------------#
#	Procedure changePassword Test Case				  #
#		Changes the password given username, 		  #
#		correct password and new password		 	  #
# ----------------------------------------------------#

#Call function using username, correct password, and new password
/*CALL changePassword('dannyboy17', '123456', '654321');*/

#Ensure new password is implemented
/*SELECT validateLogin('dannyboy17', '654321') as valid;*/

#Call function using username, incorrect password, and new password
/*CALL changePassword('dannyboy17', '12345', '54321');*/

#Ensure new password is not implemented
/*SELECT validateLogin('dannyboy17', '54321') as valid;*/

#MARK!!!!!: note that changing the password of a non-existent user
#does not pass the validateLogin inherent in changePassword
/*CALL changePassword('nonExistentUser', '123456', '654321');*/
