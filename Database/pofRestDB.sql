SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=`TRADITIONAL`;

CREATE SCHEMA IF NOT EXISTS `party_of_four_restaurant`;
USE `party_of_four_restaurant`;

# ------------------------------------------------------------------------------------------------------------------------------------- #
#														Create all tables																#
# ------------------------------------------------------------------------------------------------------------------------------------- #

# ----------------------------------------------------#
#	Table `party_of_four_restaurant`.`Restaurant`	  #
# ----------------------------------------------------#

CREATE TABLE IF NOT EXISTS `party_of_four_restaurant`.`Restaurant` (
	`restID` INT NOT NULL UNIQUE AUTO_INCREMENT,
    `restName` VARCHAR(100),
    `restAddress` VARCHAR(120),
    `restPhone` VARCHAR(10),
    PRIMARY KEY (`restID`))
ENGINE = InnoDB;

# ----------------------------------------------------#
#	Table `party_of_four_restaurant`.`MenuItem`		  #
# ----------------------------------------------------#

CREATE TABLE IF NOT EXISTS `party_of_four_restaurant`.`MenuItem` (
	`menuItemID` INT NOT NULL UNIQUE AUTO_INCREMENT,
    `itemName` VARCHAR(45),
    `itemDescript` VARCHAR(120),
    `itemPrice` VARCHAR(10),
    `itemCalories` INT,
    `itemIngredients` VARCHAR(200),
	`itemEatTime` TIME(6),
    `itemDeliverTime` TIME(6),
    `itemCookTime` TIME(6),
    `Restaurant_restID` INT NOT NULL,
    PRIMARY KEY (`menuItemID`),
    INDEX `fk_MenuItem-Restaurant` (`Restaurant_restID` ASC),
    CONSTRAINT `fk_MenuItem-Restaurant`
		FOREIGN KEY(`Restaurant_restID`)
        REFERENCES `party_of_four_restaurant`.`Restaurant` (`restID`)
		ON DELETE CASCADE
        ON UPDATE CASCADE)
ENGINE = InnoDB;

# ----------------------------------------------------#
#	Table `party_of_four_restaurant`.`OrderedItem`	  #
# ----------------------------------------------------#

CREATE TABLE IF NOT EXISTS `party_of_four_restaurant`.`OrderedItem` (
	`orderedItemID` INT NOT NULL UNIQUE AUTO_INCREMENT,
	`orderTime` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `deliverTime` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `cookSTime` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `cookETime` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `MenuItem_menuItemID` INT,
	`Ticket_ticketID` INT NOT NULL,
    `Staff_staffID` INT,
    PRIMARY KEY (`orderedItemID`),
    INDEX `fk_OrderedItem-MenuItem` (`MenuItem_menuItemID` ASC),
    INDEX `fk_OrderedItem-Ticket` (`Ticket_ticketID` ASC),
    INDEX `fk_OrderedItem-Staff` (`Staff_staffID` ASC),
    CONSTRAINT `fk_OrderedItem-MenuItem`
		FOREIGN KEY(`MenuItem_menuItemID`)
        REFERENCES `party_of_four_restaurant`.`MenuItem` (`menuItemID`)
		ON DELETE CASCADE
        ON UPDATE CASCADE,
	CONSTRAINT `fk_OrderedItem-Ticket`
		FOREIGN KEY(`Ticket_ticketID`)
        REFERENCES `party_of_four_restaurant`.`Ticket` (`ticketID`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
	CONSTRAINT `fk_OrderedItem-Staff`
		FOREIGN KEY(`Staff_staffID`)
        REFERENCES `party_of_four_restaurant`.`Staff` (`staffID`)
        ON DELETE CASCADE
        ON UPDATE CASCADE)
ENGINE = InnoDB;

# ----------------------------------------------------#
#	Table `party_of_four_restaurant`.`Ticket`		  #
# ----------------------------------------------------#

CREATE TABLE IF NOT EXISTS `party_of_four_restaurant`.`Ticket` (
	`ticketID` INT NOT NULL UNIQUE AUTO_INCREMENT,
    `ticketSTime` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `ticketETime` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `ticketPredictedTime` INT,
    `ticketActualTime` INT,
    `Party_partyID` INT NOT NULL,
    PRIMARY KEY (`ticketID`),
    INDEX `fk_Ticket-Party` (`Party_partyID` ASC),
    FOREIGN KEY(`Party_partyID`)
        REFERENCES `party_of_four_restaurant`.`Party` (`partyID`)
		ON DELETE CASCADE
        ON UPDATE CASCADE)
ENGINE = InnoDB;

# ----------------------------------------------------#
#	Table `party_of_four_restaurant`.`Staff`		  #
# ----------------------------------------------------#

CREATE TABLE IF NOT EXISTS `party_of_four_restaurant`.`Staff` (
	`staffID` INT NOT NULL UNIQUE AUTO_INCREMENT,
	`staffLName` VARCHAR(45),
    `staffFName` VARCHAR(45),
    `staffXP` VARCHAR(45),
    `staffPhone` VARCHAR(10),
    `staffEmail` VARCHAR(100),
    `staffShift` VARCHAR(45),
    `staffPay` VARCHAR(45),
    `Job_jobID` INT NOT NULL,
    `Section_sectionID` INT,
    `Restaurant_restID` INT NOT NULL,
    PRIMARY KEY (`staffID`),
    INDEX `fk_Staff-Job` (`Job_jobID` ASC),
    INDEX `fk_Staff-Section` (`Section_sectionID` ASC),
    INDEX `fk_Staff-Restaurant` (`Restaurant_restID` ASC),
    FOREIGN KEY(`Job_jobID`)
        REFERENCES `party_of_four_restaurant`.`Job` (`jobID`)
		ON DELETE CASCADE
        ON UPDATE CASCADE,
	FOREIGN KEY(`Section_sectionID`)
        REFERENCES `party_of_four_restaurant`.`Section` (`sectionID`)
		ON DELETE CASCADE
        ON UPDATE CASCADE,
        FOREIGN KEY(`Restaurant_restID`)
        REFERENCES `party_of_four_restaurant`.`Restaurant` (`restID`)
		ON DELETE CASCADE
        ON UPDATE CASCADE)
ENGINE = InnoDB;

# ----------------------------------------------------#
#	Table `party_of_four_restaurant`.`Party`		  #
# ----------------------------------------------------#

CREATE TABLE IF NOT EXISTS `party_of_four_restaurant`.`Party` (
	`partyID` INT NOT NULL UNIQUE AUTO_INCREMENT,
	`partySize` VARCHAR(3),
    `Table_tableID` INT NOT NULL,
    PRIMARY KEY (`partyID`),
    INDEX `fk_Party-Table` (`Table_tableID` ASC),
    FOREIGN KEY(`Table_tableID`)
        REFERENCES `party_of_four_restaurant`.`Table` (`tableID`)
		ON DELETE CASCADE
        ON UPDATE CASCADE)
ENGINE = InnoDB;

# ----------------------------------------------------#
#	Table `party_of_four_restaurant`.`Table`		  #
# ----------------------------------------------------#

CREATE TABLE IF NOT EXISTS `party_of_four_restaurant`.`Table` (
	`tableID` INT NOT NULL UNIQUE AUTO_INCREMENT,
	`numSeats` VARCHAR(2),
    `Section_sectionID` INT NOT NULL,
    PRIMARY KEY (`tableID`),
    INDEX `fk_Table-Section` (`Section_sectionID` ASC),
    FOREIGN KEY(`Section_sectionID`)
        REFERENCES `party_of_four_restaurant`.`Section` (`sectionID`)
		ON DELETE CASCADE
        ON UPDATE CASCADE)
ENGINE = InnoDB;

# ----------------------------------------------------#
#	Table `party_of_four_restaurant`.`Section`		  #
# ----------------------------------------------------#

CREATE TABLE IF NOT EXISTS `party_of_four_restaurant`.`Section` (
	`sectionID` INT NOT NULL UNIQUE AUTO_INCREMENT,
    PRIMARY KEY (`sectionID`))
ENGINE = InnoDB;

# ----------------------------------------------------#
#	Table `party_of_four_restaurant`.`Job`			  #
# ----------------------------------------------------#

CREATE TABLE IF NOT EXISTS `party_of_four_restaurant`.`Job` (
	`jobID` INT NOT NULL UNIQUE AUTO_INCREMENT,
    `jobName` VARCHAR(50),
    PRIMARY KEY (`jobID`))
ENGINE = InnoDB;

# ------------------------------------------------------------------------------------------------------------------------------------- #
#														Populate all tables	with example data											#
# ------------------------------------------------------------------------------------------------------------------------------------- #

# ----------------------------------------------------#
#	Populate Restaurant with example data			  #
# ----------------------------------------------------#

/*Data added below in test cases*/

# ----------------------------------------------------#
#	Populate MenuItem with example data				  #
# ----------------------------------------------------#

/*Data added below in test cases*/
  
# ----------------------------------------------------#
#	Populate Staff with example data				  #
# ----------------------------------------------------#

/*Data added below in test cases*/

# ----------------------------------------------------#
#	Populate Ticket with example data				  #
# ----------------------------------------------------#

/*Data added below in test cases*/
    
# ----------------------------------------------------#
#	Populate OrderedItem with example data			  #
# ----------------------------------------------------#

/*Data added below in test cases*/
   
# ----------------------------------------------------#
#	Populate Party with example data				  #
# ----------------------------------------------------#

/*Data added below in test cases*/

# ----------------------------------------------------#
#	Populate Table with example data				  #
# ----------------------------------------------------#

/*Data added below in test cases*/

# ----------------------------------------------------#
#	Populate Section with example data				  #
# ----------------------------------------------------#

/*Data added below in test cases*/

# ----------------------------------------------------#
#	Populate Job with example data					  #
# ----------------------------------------------------#

/*Data added below in test cases*/

# ------------------------------------------------------------------------------------------------------------------------------------- #
#														Create all procedures															#
# ------------------------------------------------------------------------------------------------------------------------------------- #

# ------------------------------------------------------------------------------------------#
#					Procedures/Functions for Restaurant								        #
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Procedure createNewRestaurant					  #
#		Creates new menu item with random, unique PK  #
# ----------------------------------------------------#

DELIMITER //
CREATE PROCEDURE createNewRestaurant(restName VARCHAR(100), restAddress VARCHAR(120), restPhone VARCHAR(10))
	BEGIN
		INSERT INTO Restaurant (`restName`, `restAddress`, `restPhone`)
		VALUES (restName, restAddress, restPhone);
	END//
DELIMITER ;

# ----------------------------------------------------#
#	Procedure deleteRestaurant						  #
#		Deletes existing restaurants based on PK	  #
# ----------------------------------------------------#

CREATE PROCEDURE deleteRestaurant(rest INT)
	DELETE FROM Restaurant WHERE `restID` = rest;

# ----------------------------------------------------#
#	Procedure setRestaurantName					  	  #
#		Change existing restaurant name				  #
# ----------------------------------------------------#

CREATE PROCEDURE setRestaurantName(rest INT, newRestName VARCHAR(100))
	UPDATE Restaurant SET `restName` = newRestName WHERE `restID` = rest;

# ----------------------------------------------------#
#	Function getRestaurantName					  	  #
#		Get existing restaurant name			  	  #
# ----------------------------------------------------#

CREATE FUNCTION getRestaurantName(rest INT)
	RETURNS VARCHAR(100)
    
    RETURN (SELECT `restName` FROM Restaurant WHERE `restID` = rest);

# ----------------------------------------------------#
#	Procedure setRestaurantAddress				  	  #
#		Change existing restaurant adress			  #
# ----------------------------------------------------#

CREATE PROCEDURE setRestaurantAddress(rest INT, newRestAddress VARCHAR(120))
	UPDATE Restaurant SET `restAddress` = newRestAddress WHERE `restID` = rest;

# ----------------------------------------------------#
#	Function getRestaurantAddress				  	  #
#		Get existing restaurant address			  	  #
# ----------------------------------------------------#

CREATE FUNCTION getRestaurantAddress(rest INT)
	RETURNS VARCHAR(120)
    
    RETURN (SELECT `restAddress` FROM Restaurant WHERE `restID` = rest);
    
# ----------------------------------------------------#
#	Procedure setRestaurantPhone				  	  #
#		Change existing restaurant phone			  #
# ----------------------------------------------------#

CREATE PROCEDURE setRestaurantPhone(rest INT, newRestPhone VARCHAR(10))
	UPDATE Restaurant SET `restPhone` = newRestPhone WHERE `restID` = rest;

# ----------------------------------------------------#
#	Function getRestaurantPhone					  	  #
#		Get existing restaurant phone			  	  #
# ----------------------------------------------------#

CREATE FUNCTION getRestaurantPhone(rest INT)
	RETURNS VARCHAR(10)
    
    RETURN (SELECT `restPhone` FROM Restaurant WHERE `restID` = rest);
    
# ------------------------------------------------------------------------------------------#
#					Procedures/Functions for MenuItem								        #
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Procedure createNewMenuItem						  #
#		Creates new menu item with random, unique PK  #
# ----------------------------------------------------#

DELIMITER //
CREATE PROCEDURE createNewMenuItem(itemName VARCHAR(45), itemDescript VARCHAR(120), itemPrice VARCHAR(10), itemCalories INT, itemIngredients VARCHAR(200), itemEatTime TIME(6), itemDeliverTime TIME(6), itemCookTime TIME(6), rest INT)
	BEGIN
		INSERT INTO MenuItem (`itemName`, `itemDescript`, `itemPrice`, `itemCalories`, `itemIngredients`, `itemEatTime`, `itemDeliverTime`, `itemCookTime`, `Restaurant_restID`)
		VALUES (itemName, itemDescript, itemPrice, itemCalories, itemIngredients, itemEatTime, itemDeliverTime, itemCookTime, rest);
	END//
DELIMITER ;

# ----------------------------------------------------#
#	Procedure deleteMenuItem						  #
#		Deletes existing menu items based on PK		  #
# ----------------------------------------------------#

CREATE PROCEDURE deleteMenuItem(menuItem INT)
	DELETE FROM MenuItem WHERE `menuItemID` = menuItem;

# ----------------------------------------------------#
#	Procedure setMenuItemName					  	  #
#		Change existing menu item name				  #
# ----------------------------------------------------#

CREATE PROCEDURE setMenuItemName(menuItem INT, newItemName VARCHAR(45))
	UPDATE MenuItem SET `itemName` = newItemName WHERE `menuItemID` = menuItem;

# ----------------------------------------------------#
#	Function getMenuItemName					  	  #
#		Get existing menu item name				  	  #
# ----------------------------------------------------#

CREATE FUNCTION getMenuItemName(itemID INT)
	RETURNS VARCHAR(45)
    
    RETURN (SELECT `itemName` FROM MenuItem WHERE `menuItemID` = itemID);

# ----------------------------------------------------#
#	Procedure setMenuItemPrice					  	  #
#		Change existing menu item price				  #
# ----------------------------------------------------#

CREATE PROCEDURE setMenuItemPrice(menuItem INT, newItemPrice VARCHAR(10))
	UPDATE MenuItem SET `itemPrice` = newItemPrice WHERE `menuItemID` = menuItem;

# ----------------------------------------------------#
#	Function getMenuItemPrice					  	  #
#		Get existing menu item price			  	  #
# ----------------------------------------------------#

CREATE FUNCTION getMenuItemPrice(menuItem INT)
	RETURNS VARCHAR(45)
    
    RETURN (SELECT `itemPrice` FROM MenuItem WHERE `menuItemID` = menuItem);

# ----------------------------------------------------#
#	Procedure setMenuItemDescript				  	  #
#		Change existing menu item description		  #
# ----------------------------------------------------#

CREATE PROCEDURE setMenuItemDescript(menuItem INT, newItemDescript VARCHAR(120))
	UPDATE MenuItem SET `itemDescript` = newItemDescript WHERE `menuItemID` = menuItem;

# ----------------------------------------------------#
#	Function getMenuItemDescript				  	  #
#		Get existing menu item description		  	  #
# ----------------------------------------------------#

CREATE FUNCTION getMenuItemDescript(menuItem INT)
	RETURNS VARCHAR(120)
    
    RETURN (SELECT `itemDescript` FROM MenuItem WHERE `menuItemID` = menuItem);

# ----------------------------------------------------#
#	Procedure setMenuItemCalories				  	  #
#		Change existing menu item calories			  #
# ----------------------------------------------------#

CREATE PROCEDURE setMenuItemCalories(menuItem INT, newItemCalories INT)
	UPDATE MenuItem SET `itemCalories` = newItemCalories WHERE `menuItemID` = menuItem;

# ----------------------------------------------------#
#	Function getMenuItemCalories				  	  #
#		Get existing menu item calories			  	  #
# ----------------------------------------------------#

CREATE FUNCTION getMenuItemCalories(menuItem INT)
	RETURNS INT
    
    RETURN (SELECT `itemCalories` FROM MenuItem WHERE `menuItemID` = menuItem);

# ----------------------------------------------------#
#	Procedure setMenuItemIngredients			  	  #
#		Change existing menu item ingredients		  #
# ----------------------------------------------------#

CREATE PROCEDURE setMenuItemIngredients(menuItem INT, newItemIngredients VARCHAR(200))
	UPDATE MenuItem SET `itemIngredients` = newItemIngredients WHERE `menuItemID` = menuItem;

# ----------------------------------------------------#
#	Function getMenuItemIngredients				  	  #
#		Get existing menu item ingredients		  	  #
# ----------------------------------------------------#

CREATE FUNCTION getMenuItemIngredients(menuItem INT)
	RETURNS VARCHAR(200)
    
    RETURN (SELECT `itemIngredients` FROM MenuItem WHERE `menuItemID` = menuItem);

# ----------------------------------------------------#
#	Procedure setMenuItemEatTime					  #
#		Change existing menu item eat time			  #
# ----------------------------------------------------#

CREATE PROCEDURE setMenuItemEatTime(menuItem INT, newItemEatTime TIME(6))
	UPDATE MenuItem SET `itemEatTime` = newItemEatTime WHERE `menuItemID` = menuItem;

# ----------------------------------------------------#
#	Function getMenuItemEatTime					  	  #
#		Get existing menu item eat time			  	  #
# ----------------------------------------------------#

CREATE FUNCTION getMenuItemEatTime(menuItem INT)
	RETURNS TIME(6)
    
    RETURN (SELECT `itemEatTime` FROM MenuItem WHERE `menuItemID` = menuItem);

# ----------------------------------------------------#
#	Procedure setMenuItemCookTime				  	  #
#		Change existing menu item cook time			  #
# ----------------------------------------------------#

CREATE PROCEDURE setMenuItemCookTime(menuItem INT, newItemCookTime TIME(6))
	UPDATE MenuItem SET `itemCookTime` = newItemCookTime WHERE `menuItemID` = menuItem;

# ----------------------------------------------------#
#	Function getMenuItemCookTime				  	  #
#		Get existing menu item cook time		  	  #
# ----------------------------------------------------#

CREATE FUNCTION getMenuItemCookTime(menuItem INT)
	RETURNS TIME(6)
    
    RETURN (SELECT `itemCookTime` FROM MenuItem WHERE `menuItemID` = menuItem);

# ----------------------------------------------------#
#	Procedure setMenuItemDeliverTime				  #
#		Change existing menu item deliver time		  #
# ----------------------------------------------------#

CREATE PROCEDURE setMenuItemDeliverTime(menuItem INT, newItemDeliverTime TIME(6))
	UPDATE MenuItem SET `itemDeliverTime` = newItemDeliverTime WHERE `menuItemID` = menuItem;

# ----------------------------------------------------#
#	Function getMenuItemDeliverTime				  	  #
#		Get existing menu item deliver time		  	  #
# ----------------------------------------------------#

CREATE FUNCTION getMenuItemDeliverTime(menuItem INT)
	RETURNS TIME(6)
    
    RETURN (SELECT `itemDeliverTime` FROM MenuItem WHERE `menuItemID` = menuItem);

# ------------------------------------------------------------------------------------------#
#					Procedures/Functions for Job									        #
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Procedure createNewJob							  #
#		Creates new job with random, unique PK 		  #
# ----------------------------------------------------#

DELIMITER //
CREATE PROCEDURE createNewJob(jobName VARCHAR(50))
	BEGIN
		INSERT INTO Job (`jobName`)
		VALUES (jobName); 
	END//
DELIMITER ;

# ----------------------------------------------------#
#	Procedure deleteJob								  #
#		Deletes existing job based on PK			  #
# ----------------------------------------------------#

CREATE PROCEDURE deleteJob(job INT)
	DELETE FROM Job WHERE `jobID` = job;

# ----------------------------------------------------#
#	Procedure setJobName						  	  #
#		Change existing job name					  #
# ----------------------------------------------------#

CREATE PROCEDURE setJobName(job INT, newJobName VARCHAR(50))
	UPDATE Job SET `jobName` = newJobName WHERE `jobID` = job;

# ----------------------------------------------------#
#	Function getJobName							  	  #
#		Get existing job name					  	  #
# ----------------------------------------------------#

CREATE FUNCTION getJobName(job INT)
	RETURNS VARCHAR(50)
    
    RETURN (SELECT `jobName` FROM Job WHERE `jobID` = job);
    
# ------------------------------------------------------------------------------------------#
#					Procedures/Functions for OrderedItem							        #
# ------------------------------------------------------------------------------------------#

# --------------------------------------------------------------#
#	Procedure newOrderedItem						  		 	#
#		Creates new ordered item with random, unique PK		  	#
#		NOTE: Only set `orderItemID`, menuItemID and ticketID	#
# --------------------------------------------------------------#

DELIMITER //
CREATE PROCEDURE createNewOrderedItem(menuItemID INT, ticketID INT, staffID INT)
	BEGIN
		INSERT INTO OrderedItem(`MenuItem_menuItemID`, `Ticket_ticketID`, `Staff_staffID`)
		VALUES (menuItemID, ticketID, staffID);
	END//
DELIMITER ;

# ----------------------------------------------------#
#	Procedure deleteOrderedItem						  #
#		Delete existing ordered item			      #
# ----------------------------------------------------#

CREATE PROCEDURE deleteOrderedItem(OrderedItem INT)
	DELETE FROM OrderedItem WHERE `OrderedItemID` = OrderedItem;

# ----------------------------------------------------#
#	Function getOrderedItemMenuItemName			  	  #
#		Get existing ordered item menu item name  	  #
# ----------------------------------------------------#

CREATE FUNCTION getOrderedItemMenuItemName(orderedItem INT)
	RETURNS VARCHAR(45)
    
    RETURN (SELECT MI.itemName 
			FROM MenuItem MI
            INNER JOIN OrderedItem OI
            ON OI.MenuItem_menuItemID = MI.menuItemID
            AND OI.MenuItem_menuItemID = orderedItem LIMIT 1);

# ----------------------------------------------------#
#	Function getOrderedItemOrderTime			  	  #
#		Get existing ordered item order time	  	  #
# ----------------------------------------------------#

CREATE FUNCTION getOrderedItemOrderTime(orderedItem INT)
	RETURNS TIME(6)
    
    RETURN (SELECT `orderTime` FROM OrderedItem WHERE `orderedItemID` = orderedItem);

# ----------------------------------------------------#
#	Procedure setOrderedItemDeliverTime			   	  #
#		Change existing ordered item deliver time	  #
# ----------------------------------------------------#

CREATE PROCEDURE setOrderedItemDeliverTime(orderedItem INT)
	UPDATE OrderedItem SET `deliverTime` = CURRENT_TIME WHERE `orderedItemID` = orderedItem;
    
# ----------------------------------------------------#
#	Function getOrderedItemDeliverTime			  	  #
#		Get existing ordered item deliver time	  	  #
# ----------------------------------------------------#

CREATE FUNCTION getOrderedItemDeliverTime(orderedItem INT)
	RETURNS TIME(6)
    
    RETURN (SELECT `deliverTime` FROM OrderedItem WHERE `orderedItemID` = orderedItem);

# ----------------------------------------------------#
#	Procedure setOrderedItemCookSTime			  	  #
#		Change existing ordered item cook start time  #
# ----------------------------------------------------#

CREATE PROCEDURE setOrderedItemCookSTime(orderedItem INT)
	UPDATE OrderedItem SET `cookSTime` = CURRENT_TIME WHERE `orderedItemID` = orderedItem;
              
# ----------------------------------------------------#
#	Function getOrderedItemCookSTime			  	  #
#		Get existing ordered item cook start time  	  #
# ----------------------------------------------------#

CREATE FUNCTION getOrderedItemCookSTime(orderedItem INT)
	RETURNS TIME(6)
    
    RETURN (SELECT `CookSTime` FROM OrderedItem WHERE `orderedItemID` = orderedItem);

# ----------------------------------------------------#
#	Procedure setOrderedItemCookETime			  	  #
#		Change existing ordered item cook end time    #
# ----------------------------------------------------#

CREATE PROCEDURE setOrderedItemCookETime(orderedItem INT)
	UPDATE OrderedItem SET `cookETime` = CURRENT_TIME WHERE `orderedItemID` = orderedItem;          

# ----------------------------------------------------#
#	Function getOrderedItemCookETime			  	  #
#		Get existing ordered item cook end time  	  #
# ----------------------------------------------------#

CREATE FUNCTION getOrderedItemCookETime(orderedItem INT)
	RETURNS TIME(6)
    
    RETURN (SELECT `CookETime` FROM OrderedItem WHERE `orderedItemID` = orderedItem);

# ------------------------------------------------------------------------------------------#
#					Procedures/Functions for Staff									        #
# ------------------------------------------------------------------------------------------#

# --------------------------------------------------------------#
#	Procedure newStaff								  		 	#
#		Creates new staff member with random, unique PK		  	#
# --------------------------------------------------------------#

DELIMITER //
CREATE PROCEDURE createNewStaff(staffLName VARCHAR(45), staffFName VARCHAR(45), staffXP VARCHAR(45), staffPhone VARCHAR(10), staffEmail VARCHAR(100), staffShift VARCHAR(45), staffPay VARCHAR(45), job INT, section INT, rest INT)
	BEGIN
		INSERT INTO Staff (`staffLName`, `staffFName`, `staffXP`, `staffPhone`, `staffEmail`, `staffShift`, `staffPay`,`Job_jobID`, `Section_sectionID`, `Restaurant_restID`)
		VALUES (staffLName, staffFName, staffXP, staffPhone, staffEmail, staffShift, staffPay, job, section, rest);
	END//
DELIMITER ;

# ----------------------------------------------------#
#	Procedure deleteStaff							  #
#		Delete existing staff member			      #
# ----------------------------------------------------#

CREATE PROCEDURE deleteStaff(staff INT)
	DELETE FROM Staff WHERE `staffID` = staff;

# ----------------------------------------------------#
#	Function setStaffLName						  	  #
#		Set existing staff member's last name	  	  #
# ----------------------------------------------------#

CREATE PROCEDURE setStaffLName(staff INT, LName VARCHAR(45))
	UPDATE Staff SET `staffLName` = LName WHERE `staffID` = staff;

# ----------------------------------------------------#
#	Function getStaffLName						  	  #
#		Get existing staff member's last name	  	  #
# ----------------------------------------------------#

CREATE FUNCTION getStaffLName(staff INT)
	RETURNS VARCHAR(45)
    
    RETURN (SELECT `staffLName` FROM Staff WHERE `staffID` = staff);

# ----------------------------------------------------#
#	Procedure setStaffFName						   	  #
#		Change existing staff member's first name	  #
# ----------------------------------------------------#

CREATE PROCEDURE setStaffFName(staff INT, FName VARCHAR(45))
	UPDATE Staff SET `staffFName` = FName WHERE `staffID` = staff;
    
# ----------------------------------------------------#
#	Function getStaffFName						  	  #
#		Get existing staff member's last name	  	  #
# ----------------------------------------------------#

CREATE FUNCTION getStaffFName(staff INT)
	RETURNS VARCHAR(45)
    
    RETURN (SELECT `staffFName` FROM Staff WHERE `staffID` = staff);

# ----------------------------------------------------#
#	Procedure setStaffXP						  	  #
#		Change existing staff experience			  #
# ----------------------------------------------------#

CREATE PROCEDURE setStaffXP(staff INT, XP VARCHAR(45))
	UPDATE Staff SET `staffXP` = XP WHERE `staffID` = staff;
              
# ----------------------------------------------------#
#	Function getOrderedItemCookSTime			  	  #
#		Get existing ordered item cook start time  	  #
# ----------------------------------------------------#

CREATE FUNCTION getStaffXP(staff INT)
	RETURNS VARCHAR(45)
    
    RETURN (SELECT `staffXP` FROM Staff WHERE `staffID` = staff);

# ----------------------------------------------------#
#	Procedure setStaffPhone						  	  #
#		Change existing staff member's phone number   #
# ----------------------------------------------------#

CREATE PROCEDURE setStaffPhone(staff INT, phone VARCHAR(45))
	UPDATE Staff SET `staffPhone` = phone WHERE `staffID` = staff;          

# ----------------------------------------------------#
#	Function getStaffPhone						  	  #
#		Get existing staff phone number			 	  #
# ----------------------------------------------------#

CREATE FUNCTION getStaffPhone(staff INT)
	RETURNS VARCHAR(45)
    
    RETURN (SELECT `staffPhone` FROM Staff WHERE `staffID` = staff);

# ----------------------------------------------------#
#	Procedure setStaffEmail						  	  #
#		Change existing staff member's email address  #
# ----------------------------------------------------#

CREATE PROCEDURE setStaffEmail(staff INT, email VARCHAR(100))
	UPDATE Staff SET `staffEmail` = email WHERE `staffID` = staff;          

# ----------------------------------------------------#
#	Function getStaffEmail						  	  #
#		Get existing staff email address		 	  #
# ----------------------------------------------------#

CREATE FUNCTION getStaffEmail(staff INT)
	RETURNS VARCHAR(100)
    
    RETURN (SELECT `staffEmail` FROM Staff WHERE `staffID` = staff);
    
# ----------------------------------------------------#
#	Procedure setStaffShift						  	  #
#		Change existing staff member's shift	      #
# ----------------------------------------------------#

CREATE PROCEDURE setStaffShift(staff INT, shift VARCHAR(45))
	UPDATE Staff SET `staffShift` = shift WHERE `staffID` = staff;          

# ----------------------------------------------------#
#	Function getStaffShift						  	  #
#		Get existing staff shift				 	  #
# ----------------------------------------------------#

CREATE FUNCTION getStaffShift(staff INT)
	RETURNS VARCHAR(45)
    
    RETURN (SELECT `staffShift` FROM Staff WHERE `staffID` = staff);
    
# ----------------------------------------------------#
#	Procedure setStaffPay						  	  #
#		Change existing staff member's pay		      #
# ----------------------------------------------------#

CREATE PROCEDURE setStaffPay(staff INT, pay VARCHAR(45))
	UPDATE Staff SET `staffPay` = pay WHERE `staffID` = staff;          

# ----------------------------------------------------#
#	Function getStaffPay						  	  #
#		Get existing staff pay					 	  #
# ----------------------------------------------------#

CREATE FUNCTION getStaffPay(staff INT)
	RETURNS VARCHAR(45)
    
    RETURN (SELECT `staffPay` FROM Staff WHERE `staffID` = staff);
    
# ------------------------------------------------------------------------------------------#
#					Procedures/Functions for Ticket									        #
# ------------------------------------------------------------------------------------------#

# --------------------------------------------------------------#
#	Procedure createNewTicket				  		 			#
#		Creates new ticket with random, unique PK		  		#
#		NOTE: Only set `ticketID`								#
# --------------------------------------------------------------#

DELIMITER //
CREATE PROCEDURE createNewTicket(party INT)
	BEGIN
		INSERT INTO Ticket(`Party_partyID`)
		VALUES (party);
	END//
DELIMITER ;

# ----------------------------------------------------#
#	Procedure deleteTicket							  #
#		Delete existing ticket					      #
# ----------------------------------------------------#

CREATE PROCEDURE deleteTicket(ticket INT)
	DELETE FROM Ticket WHERE `ticketID` = ticket;

# ----------------------------------------------------#
#	Procedure setTicketETime					   	  #
#		Change existing ticket end time	  			  #
# ----------------------------------------------------#

CREATE PROCEDURE setTicketETime(ticket INT)
	UPDATE Ticket SET `ticketETime` = CURRENT_TIME WHERE `ticketID` = ticket;

# ----------------------------------------------------#
#	Function getTicketSTime			  	 			  #
#		Get existing ticket start time			  	  #
# ----------------------------------------------------#

CREATE FUNCTION getTicketSTime(ticket INT)
	RETURNS TIME(6)
    
    RETURN (SELECT `ticketSTime` FROM Ticket WHERE `ticketID` = ticket);

# ----------------------------------------------------#
#	Function getTicketETime			  				  #
#		Get existing ticket end time			  	  #
# ----------------------------------------------------#

CREATE FUNCTION getTicketETime(ticket INT)
	RETURNS TIME(6)
    
    RETURN (SELECT `ticketETime` FROM Ticket WHERE `ticketID` = ticket);

# ----------------------------------------------------#
#	Function getTicketPredictedTime	  	 			  #
#		Get existing ticket predicted time		  	  #
# ----------------------------------------------------#

CREATE FUNCTION getTicketPredictedTime(ticket INT)
	RETURNS TIME(6)
    
    RETURN (SELECT `ticketPredictedTime` FROM Ticket WHERE `ticketID` = ticket);

# ----------------------------------------------------#
#	Procedure setTicketActualTime				   	  #
#		Change existing ticket actual time 			  #
# ----------------------------------------------------#

CREATE PROCEDURE setTicketActualTime(ticket INT)
	UPDATE Ticket SET `ticketActualTime` = (`TicketETime` - `TicketSTime`) WHERE `ticketID` = ticket;

# ----------------------------------------------------#
#	Function getTicketActualTime	  	 			  #
#		Get existing ticket actual time			  	  #
# ----------------------------------------------------#

CREATE FUNCTION getTicketActualTime(ticket INT)
	RETURNS TIME(6)
    
    RETURN (SELECT `ticketActualTime` FROM Ticket WHERE `ticketID` = ticket);

# ------------------------------------------------------------------------------------------#
#					Procedures/Functions for Party									        #
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Procedure createParty							  #
#		Creates new party with random, unique PK 	  #
# ----------------------------------------------------#

DELIMITER //
CREATE PROCEDURE createNewParty(partySize VARCHAR(3), tableID INT)
	BEGIN
		INSERT INTO Party (`partySize`, `Table_tableID`)
		VALUES (partySize, tableID);
	END//
DELIMITER ;

# ----------------------------------------------------#
#	Procedure deleteParty							  #
#		Deletes existing party based on PK			  #
# ----------------------------------------------------#

CREATE PROCEDURE deleteParty(party INT)
	DELETE FROM Party WHERE `partyID` = party;

# ----------------------------------------------------#
#	Procedure setPartyPartySize					   	  #
#		Change existing party party size			  #
# ----------------------------------------------------#

CREATE PROCEDURE setPartyPartySize(party INT, partySize VARCHAR(3))
	UPDATE Party SET `partySize` = partySize WHERE `partyID` = party;

# ----------------------------------------------------#
#	Function getPartyPartySize		  	 			  #
#		Get existing party party size			  	  #
# ----------------------------------------------------#

CREATE FUNCTION getPartyPartySize(party INT)
	RETURNS VARCHAR(3)
    
    RETURN (SELECT `partySize` FROM Party WHERE `partyID` = party);

# ------------------------------------------------------------------------------------------#
#					Procedures/Functions for Table									        #
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Procedure createTable							  #
#		Creates new table with random, unique PK  	  #
# ----------------------------------------------------#

DELIMITER //
CREATE PROCEDURE createNewTable(numSeats VARCHAR(3), sectionID INT)
	BEGIN
		INSERT INTO `Table` (`numSeats`, `Section_sectionID`)
		VALUES (numSeats, sectionID);
	END//
DELIMITER ;

# ----------------------------------------------------#
#	Procedure deleteTable							  #
#		Deletes existing table based on PK			  #
# ----------------------------------------------------#

CREATE PROCEDURE deleteTable(myTable INT)
	DELETE FROM `Table` WHERE `tableID` = myTable;
    
# ----------------------------------------------------#
#	Procedure setTableNumSeats					   	  #
#		Change existing table numSeats				  #
# ----------------------------------------------------#

CREATE PROCEDURE setTableNumSeats(myTable INT, numSeats VARCHAR(2))
	UPDATE `Table` SET `numSeats` = numSeats WHERE `tableID` = myTable;

# ----------------------------------------------------#
#	Function getTableNumSeats		  	 			  #
#		Get existing table numSeats				  	  #
# ----------------------------------------------------#

CREATE FUNCTION getTableNumSeats(myTable INT)
	RETURNS VARCHAR(3)
    
    RETURN (SELECT `numSeats` FROM `Table` WHERE `tableID` = myTable);
    
# ------------------------------------------------------------------------------------------#
#					Procedures/Functions for Section								        #
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Procedure createSection							  #
#		Creates new section with random, unique PK 	  #
# ----------------------------------------------------#

DELIMITER //
CREATE PROCEDURE createNewSection()
	BEGIN
		INSERT INTO Section ()
		VALUES ();
	END//
DELIMITER ;

# ----------------------------------------------------#
#	Procedure deleteSection							  #
#		Deletes existing section based on PK		  #
# ----------------------------------------------------#

CREATE PROCEDURE deleteSection(section INT)
	DELETE FROM Section WHERE `sectionID` = section;
    
#----------------------------------------------------#
#	The McWhirter Function						  	 #
#		What can't it do?							 #
#----------------------------------------------------#

DELIMITER //
CREATE PROCEDURE getData()
	BEGIN
		select T.ticketID, T.ticketSTime, T.ticketETime, (select case when MI.itemCookTime is NULL 
														  then NULL else max(MI.itemCookTime) end 
                                                          as longTime 
		from Ticket ti
		left join OrderedItem OI on OI.ticket_ticketID = ti.ticketID
		left join MenuItem MI on MI.menuItemID = OI.menuItem_menuItemID
		where ti.ticketID=T.ticketID
		group by ti.ticketID) as longestCookTime,
		P.PartySize,
		T.ticketActualTime 
		from Ticket T 
		inner join Party P 
		on T.Party_partyID = P.partyID;
	END//
DELIMITER ;

#----------------------------------------------------#
#	updateTicketTimes							  	 #
#		Add number of days for current data			 #
#----------------------------------------------------#

CREATE PROCEDURE updateTicketTimes(numDays INT)
	UPDATE Ticket SET ticketSTime = DATE_ADD(ticketSTime, INTERVAL numdays DAY), ticketETime = DATE_ADD(ticketSTime, INTERVAL numdays DAY);

# ------------------------------------------------------------------------------------------------------------------------------------- #
#														Create all Test Cases															#
# ------------------------------------------------------------------------------------------------------------------------------------- #

# ------------------------------------------------------------------------------------------#
#					Test Cases for Restaurant												#
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Procedure createNewRestaurant test case			  #
#		Should create a new restaurant				  #
# ----------------------------------------------------#
    
#Call createNewRestaurant with all info and display it
CALL createNewRestaurant('Taco Gong', '290488 Taco Drive, Mexico City, Mexico 358904', '0003334860');
CALL createNewRestaurant('PartyOfFour', '2360 Vandenberg Drive, USAF Academy, Colorado 80841', '7193334860');
SELECT * FROM Restaurant;

# ----------------------------------------------------#
#	Procedure deleteRestaurant test cases			  #
#		Deletes the Restaurant based on the PK		  #
# ----------------------------------------------------#

#Call deleteRestaurant using primary key and display all info in table
/*CALL deleteRestaurant((SELECT restID FROM Restaurant LIMIT 1));
SELECT * FROM Restaurant;*/

#MARK!!!!!: Trying to delete info for a Restaurant that does not exist 
#does NOT break the program
/*CALL deleteRestaurant('-1');
SELECT * FROM Restaurant;*/

# ----------------------------------------------------#
#	Procedure setRestaurantName test cases		  	  #
#		Should update a restaurant name based on	  #
#		primary key									  #
# ----------------------------------------------------#

#Call setRestaurantName using primary key and new restaurant name
/*CALL setRestaurantName((SELECT restID FROM Restaurant LIMIT 1), 'new name');*/

#Display data from `Restaurant`, should have new name for 'PartyOfFour'
/*SELECT * FROM Restaurant;*/

#MARK!!!!!: Trying to change the name of a non-existent
#restaurant does NOT break the program
/*CALL setRestaurantName('-1', 'OhNo!');*/

#----------------------------------------------------#
#	Function getRestaurantName test cases		  	 #
#		Should return the restaurant name using		 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the name
#of non-existent restaurant does NOT break the program
/*SELECT getRestaurantName((SELECT restID FROM Restaurant LIMIT 1)) as restName;
SELECT getRestaurantName('-1') as restName;*/

# ----------------------------------------------------#
#	Procedure setRestaurantAddress test cases	  	  #
#		Should update a restaurant address based on	  #
#		primary key									  #
# ----------------------------------------------------#

#Call setRestaurantAddress using primary key and new restaurant address
/*CALL setRestaurantAddress((SELECT restID FROM Restaurant LIMIT 1), 'new address');*/

#Display data from `Restaurant`, should have new address for 'PartyOfFour'
/*SELECT * FROM Restaurant;*/

#MARK!!!!!: Trying to change the address of a non-existent
#restaurant does NOT break the program
/*CALL setRestaurantAddress('-1', 'OhNo!');*/

#----------------------------------------------------#
#	Function getRestaurantAddress test cases	  	 #
#		Should return the restaurant name using		 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the address
#of non-existent restaurant does NOT break the program
/*SELECT getRestaurantAddress((SELECT restID FROM Restaurant LIMIT 1)) as restAddress;
SELECT getRestaurantAddress('-1') as restAddress;*/

# ----------------------------------------------------#
#	Procedure setRestaurantPhone test cases		  	  #
#		Should update a restaurant phone based on	  #
#		primary key									  #
# ----------------------------------------------------#

#Call setRestaurantPhone using primary key and new restaurant phone
/*CALL setRestaurantPhone((SELECT restID FROM Restaurant LIMIT 1), 'new phone');*/

#Display data from `Restaurant`, should have new phone for 'PartyOfFour'
/*SELECT * FROM Restaurant;*/

#MARK!!!!!: Trying to change the phone of a non-existent
#restaurant does NOT break the program
/*CALL setRestaurantPhone('-1', 'OhNo!');*/

#----------------------------------------------------#
#	Function getRestaurantPhone test cases		  	 #
#		Should return the restaurant phone using	 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the phone
#of non-existent restaurant does NOT break the program
/*SELECT getRestaurantPhone((SELECT restID FROM Restaurant LIMIT 1)) as restPhone;
SELECT getRestaurantPhone('-1') as restPhone;*/

# ------------------------------------------------------------------------------------------#
#					Test Cases for MenuItem													#
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Procedure createNewMenuItem	test case			  #
#		Should create a new menu item				  #
# ----------------------------------------------------#
 /*   
#Call createNewMenuItem with all info and display it
CALL createNewMenuItem('Pretzel Bites', 'Bite size soft pretzels with beer mustard sauce', '5.99', '300', 'Pretzels, mustard', '0:12:26.887654', '0:15:26.000005', '0:12:27.450099', (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewMenuItem('Chicken Wings', '10 traditional wings in Buffalo sauce', '10.99', '450', 'Chicken, sauce', '0:18:01.545345', '0:13:26.321732', '0:10:22.645535', (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewMenuItem('Small Drink', 'Small Mountain Dew', '2.99', '120', 'Pop', '0:08:26.543254', '0:5:15.066005', '0:00:00', (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewMenuItem('Double Cheese Burger', 'Burger with cheese, pickel, lettuce, tomato and ketchup', '12.99', '635', 'Hamburger, cheese, pickel, lettuce, tomato, ketchup', '0:20:26.887444', '0:17:30.998705', '0:15:10.411154', (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewMenuItem('French Fries', 'God bless the French', '4.99', '350', 'Potatoes', '0:22:26.887654', '0:15:26.000005', '0:12:27.450099', (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewMenuItem('Molten Chocolate Lava Cake', 'Chocolatey, creamy goodness with vanilla ice cream', '8.99', '750', 'Chocolate, cake, vanilla ice cream, caramel sauce', '0:22:26.887654', '0:15:26.000005', '0:12:27.450099', (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewMenuItem('Fried Rice', 'A side of rice, but fried!', '4.99', '300', 'Rice', '0:08:58.088765', '0:09:26.887654', '0:06:44.665799', (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewMenuItem('Alfredo Pasta', 'Italian dish with noodles and Alfredo Sauce', '13.99', '700', 'Pasta, alfredo sauce', '0:22:26.887654', '0:17:17.171717', '0:14:27.450099', (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewMenuItem('12oz Blue Moon', 'Daniels choice of alcohol', '4.99', '290', 'Beer', '0:12:09.143966', '0:04:11.777643', '0:00:00', (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewMenuItem('Kids MAC N Cheese', 'Kids Macaroni and Cheese', '4.99', '170', 'Pasta, cheese', '0:11:26.887654', '0:08:10.001105', '0:05:00.000009', (SELECT restID FROM Restaurant LIMIT 1));
SELECT * FROM MenuItem;

# ----------------------------------------------------#
#	Procedure deleteMenuItem test cases				  #
#		Deletes the MenuItem based on the PK		  #
# ----------------------------------------------------#

#Call deleteMenuItem using primary key and display all info in table
/*CALL deleteMenuItem((SELECT menuItemID FROM MenuItem LIMIT 1));
SELECT * FROM MenuItem;*/

#MARK!!!!!: Trying to delete info for a MenuItem that does not exist 
#does NOT break the program
/*CALL deleteMenuItem('0042');
SELECT * FROM MenuItem;*/

# ----------------------------------------------------#
#	Procedure setMenuItemName test cases		  	  #
#		Should update a menu item name based on		  #
#		primary key									  #
# ----------------------------------------------------#

#Call setMenuItemName using primary key and new item name
/*CALL setMenuItemName((SELECT menuItemID FROM MenuItem LIMIT 1), 'new name');*/

#Display data from `MenuItem`, should have new name for 'Pretzel Bites'
/*SELECT * FROM MenuItem;*/

#MARK!!!!!: Trying to change the name of a non-existent
#menu item does NOT break the program
/*CALL setMenuItemName('-1', 'OhNo!');*/

#----------------------------------------------------#
#	Function getMenuItemName test cases			  	 #
#		Should return the menu item name using		 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the name
#of non-existent menu item does NOT break the program
/*SELECT getMenuItemName((SELECT menuItemID FROM MenuItem LIMIT 1)) as name;
SELECT getMenuItemName('0042') as name;*/

# ----------------------------------------------------#
#	Procedure setMenuItemPrice test cases		  	  #
#		Should update a menu item price based on	  #
#		primary key									  #
# ----------------------------------------------------#

#Call setMenuItemPrice using primary key and new item price
/*CALL setMenuItemPrice((SELECT menuItemID FROM MenuItem LIMIT 1), '$1 million');*/

#Display data from `MenuItem`, should have new price for first item
/*SELECT * FROM MenuItem;*/

#MARK!!!!!: Trying to change the price of a non-existent
#menu item does NOT break the program
/*CALL setMenuItemPrice('0042', '$6');*/

#----------------------------------------------------#
#	Function getMenuItemPrice test cases		  	 #
#		Should return the menu item price using		 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the price
#of non-existent menu item returns NULL
/*SELECT getMenuItemPrice((SELECT menuItemID FROM MenuItem LIMIT 1)) as price;
SELECT getMenuItemPrice('-1') as price;*/

# ----------------------------------------------------#
#	Procedure setMenuItemDescript test cases	  	  #
#		Should update a menu item description 		  #
#		based on primary key						  #
# ----------------------------------------------------#

#Call setMenuItemDescript using primary key and new item description
/*CALL setMenuItemDescript((SELECT menuItemID FROM MenuItem LIMIT 1), 'A new description');*/

#Display data from `MenuItem`, should have new description for first item
/*SELECT * FROM MenuItem;*/

#MARK!!!!!: Trying to change the description of a non-existent
#menu item does NOT break the program
/*CALL setMenuItemDescript('-1', 'This should not work');*/

#----------------------------------------------------#
#	Function getMenuItemDescript test cases		  	 #
#		Should return the menu item descript using	 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the description
#of non-existent menu item returns NULL
/*SELECT getMenuItemDescript((SELECT menuItemID FROM MenuItem LIMIT 1)) as descript;
SELECT getMenuItemDescript('-1') as descript;*/

# ----------------------------------------------------#
#	Procedure setMenuItemCalories test cases	  	  #
#		Should update a menu item calories	 		  #
#		based on primary key						  #
# ----------------------------------------------------#

#Call setMenuItemCalories using primary key and new item calories
/*CALL setMenuItemCalories((SELECT menuItemID FROM MenuItem LIMIT 1), '2017');*/

#Display data from `MenuItem`, should have new calories for first item
/*SELECT * FROM MenuItem;*/

#MARK!!!!!: Trying to change the calories of a non-existent
#menu item does NOT break the program
/*CALL setMenuItemCalories('0042', '2199');*/

#----------------------------------------------------#
#	Function getMenuItemCalories test cases		  	 #
#		Should return the menu item calories using	 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the calories
#of non-existent menu item returns NULL
/*SELECT getMenuItemCalories((SELECT menuItemID FROM MenuItem LIMIT 1)) as calories;
SELECT getMenuItemCalories('0042') as calories;*/

# ----------------------------------------------------#
#	Procedure setMenuItemIngredients test cases	  	  #
#		Should update a menu item ingredients 		  #
#		based on primary key						  #
# ----------------------------------------------------#

#Call setMenuItemIngredients using primary key and new item ingredients
/*CALL setMenuItemIngredients((SELECT menuItemID FROM MenuItem LIMIT 1), 'new ingredients');*/

#Display data from `MenuItem`, should have new ingredients for first item
/*SELECT * FROM MenuItem;*/

#MARK!!!!!: Trying to change the ingredients of a non-existent
#menu item does NOT break the program
/*CALL setMenuItemIngredients('0042', 'should not work');*/

#----------------------------------------------------#
#	Function getMenuItemIngredients test cases	  	 #
#		Should return the menu item ingredients using#
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the ingredients
#of non-existent menu item returns NULL
/*SELECT getMenuItemIngredients((SELECT menuItemID FROM MenuItem LIMIT 1)) as ingredients;
SELECT getMenuItemIngredients('0042') as ingredients;*/

# ----------------------------------------------------#
#	Procedure setMenuItemEatTime test cases		  	  #
#		Should update a menu item eat time			  #
#		based on primary key						  #
# ----------------------------------------------------#

#Call setMenuItemEatTime using primary key and new item eat time
/*CALL setMenuItemEatTime((SELECT menuItemID FROM MenuItem LIMIT 1), '0:01:58.088765');*/

#Display data from `MenuItem`, should have new eat time for first item
/*SELECT * FROM MenuItem;*/

#MARK!!!!!: Trying to change the eat time of a non-existent
#menu item does NOT break the program
/*CALL setMenuItemEatTime('-1', '0:01:58.088765');*/

#----------------------------------------------------#
#	Function getMenuItemEatTime test cases	  		 #
#		Should return the menu item eat time using	 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the eat time
#of non-existent menu item returns NULL
/*SELECT getMenuItemEatTime((SELECT menuItemID FROM MenuItem LIMIT 1)) as eatTime;
SELECT getMenuItemEatTime('0042') as eatTime;*/

# ----------------------------------------------------#
#	Procedure setMenuItemCookTime test cases		  #
#		Should update a menu item cook time			  #
#		based on primary key						  #
# ----------------------------------------------------#

#Call setMenuItemCookTime using primary key and new item cook time
/*CALL setMenuItemCookTime((SELECT menuItemID FROM MenuItem LIMIT 1), '0:01:58.088765');*/

#Display data from `MenuItem`, should have new cook time for first item
/*SELECT * FROM MenuItem;*/

#MARK!!!!!: Trying to change the cook time of a non-existent
#menu item does NOT break the program
/*CALL setMenuItemCookTime('0042', '0:01:58.088765');*/

#----------------------------------------------------#
#	Function getMenuItemCookTime test cases	  		 #
#		Should return the menu item cook time using	 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the cook time
#of non-existent menu item returns NULL
/*SELECT getMenuItemCookTime((SELECT menuItemID FROM MenuItem LIMIT 1)) as cookTime;
SELECT getMenuItemCookTime('0042') as cookTime;*/

# ----------------------------------------------------#
#	Procedure setMenuItemDeliverTime test cases		  #
#		Should update a menu item deliver time		  #
#		based on primary key						  #
# ----------------------------------------------------#

#Call setMenuItemDeliverTime using primary key and new item deliver time
/*CALL setMenuItemDeliverTime((SELECT menuItemID FROM MenuItem LIMIT 1), '0:01:58.088765');*/

#Display data from `MenuItem`, should have new deliver time for first item
/*SELECT * FROM MenuItem;*/

#MARK!!!!!: Trying to change the deliver time of a non-existent
#menu item does NOT break the program
/*CALL setMenuItemDeliverTime('0042', '0:01:58.088765');*/

#----------------------------------------------------#
#	Function getMenuItemDeliverTime test cases 		 #
#		Should return the menu item deliver time 	 #
#		using primary keys							 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the deliver time
#of non-existent menu item returns NULL
/*SELECT getMenuItemDeliverTime((SELECT menuItemID FROM MenuItem LIMIT 1)) as deliverTime;
SELECT getMenuItemDeliverTime('0042') as deliverTime;*/

# ------------------------------------------------------------------------------------------#
#					Test Cases for Job														#
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Procedure createNewJob	test case				  #
#		Should create a new job						  #
# ----------------------------------------------------#
/*
#Call createNewMenuItem with all info and display it
CALL createNewJob('Dish Washer');
CALL createNewJob('Waiter/Waitress');
CALL createNewJob('Host/Hostess');
CALL createNewJob('Sous Chef');
CALL createNewJob('Chef');
CALL createNewJob('Personnel Manager');
CALL createNewJob('Janitor');
CALL createNewJob('Bus Boy');
CALL createNewJob('Marketing Manager');
CALL createNewJob('Logistics Manager');
SELECT * FROM Job;

# ----------------------------------------------------#
#	Procedure deleteJob test cases					  #
#		Deletes the Job based on the PK				  #
# ----------------------------------------------------#

#Call deleteJob using primary key and display all info in table
/*CALL deleteJob((SELECT jobID FROM Job LIMIT 1));
SELECT * FROM Job;*/

#MARK!!!!!: Trying to delete info for a Job that does not exist 
#does NOT break the program
/*CALL deleteJob('-1');
SELECT * FROM Job;*/

# ----------------------------------------------------#
#	Procedure setJobName test cases				  	  #
#		Should update a job name based on			  #
#		primary key									  #
# ----------------------------------------------------#

#Call setJobName using primary key and new job name
/*CALL setJobName((SELECT jobID FROM Job LIMIT 1), 'new name');*/

#Display data from `Job`, should have new name for 'Dish Washer'
/*SELECT * FROM Job;*/

#MARK!!!!!: Trying to change the name of a non-existent
#job does NOT break the program
/*CALL setMenuItemName('-1', 'OhNo!');*/

#----------------------------------------------------#
#	Function getJobName test cases				  	 #
#		Should return the job name using			 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the name
#of non-existent job does NOT break the program
/*SELECT getJobName((SELECT jobID FROM Job LIMIT 1)) as jobName;
SELECT getJobName('-1') as jobName;*/

# ------------------------------------------------------------------------------------------#
#					Test Cases for Section													#
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Procedure createNewSection test case			  #
#		Should create a new Section					  #
# ----------------------------------------------------#
/*
#Call createNewSection with all info and display it
CALL createNewSection();
CALL createNewSection();
CALL createNewSection();
CALL createNewSection();
CALL createNewSection();
CALL createNewSection();
CALL createNewSection();
CALL createNewSection();
CALL createNewSection();
CALL createNewSection();
SELECT * FROM Section;

# ----------------------------------------------------#
#	Procedure deleteSection test cases				  #
#		Deletes the Section based on the PK			  #
# ----------------------------------------------------#

#Call deleteSection using primary key and display all info in Section
/*CALL deleteSection((SELECT sectionID FROM Section LIMIT 1));
SELECT * FROM Section;*/

#MARK!!!!!: Trying to delete info for a MenuItem that does not exist 
#does NOT break the program
/*CALL deleteSection('-1');
SELECT * FROM Section;*/

# ------------------------------------------------------------------------------------------#
#					Test Cases for Table													#
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Procedure createNewTable test case				  #
#		Should create a new Party					  #
# ----------------------------------------------------#
/*
#Call createNewParty with all info and display it
CALL createNewTable('4', (SELECT sectionID FROM Section LIMIT 1));
CALL createNewTable('2', (SELECT sectionID FROM Section LIMIT 1,1));
CALL createNewTable('8', (SELECT sectionID FROM Section LIMIT 2,1));
CALL createNewTable('6', (SELECT sectionID FROM Section LIMIT 3,1));
CALL createNewTable('4', (SELECT sectionID FROM Section LIMIT 4,1));
CALL createNewTable('4', (SELECT sectionID FROM Section LIMIT 5,1));
CALL createNewTable('2', (SELECT sectionID FROM Section LIMIT 6,1));
CALL createNewTable('4', (SELECT sectionID FROM Section LIMIT 7,1));
CALL createNewTable('8', (SELECT sectionID FROM Section LIMIT 8,1));
CALL createNewTable('4', (SELECT sectionID FROM Section LIMIT 9,1));
SELECT * FROM `Table`;

# ----------------------------------------------------#
#	Procedure deleteTable test cases				  #
#		Deletes the table based on the PK			  #
# ----------------------------------------------------#

#Call deleteTable using primary key and display all info in Table
/*CALL deleteTable((SELECT tableID FROM `Table` LIMIT 1));
SELECT * FROM `Table`;*/

#MARK!!!!!: Trying to delete info for a tabe that does not exist 
#does NOT break the program
/*CALL deleteTable('-1');
SELECT * FROM `Table`;*/

# ----------------------------------------------------#
#	Procedure setTableNumSeats test cases		  	  #
#		Should update a table's numSeats based on	  #
#		primary key									  #
# ----------------------------------------------------#

#Call setTableNumSeats using primary key and new numSeats
/*CALL setTableNumSeats((SELECT tableID FROM `Table` LIMIT 1), '10');*/

#Display data from `Table`, should have new numSeats for first table
/*SELECT * FROM `Table`;*/

#MARK!!!!!: Trying to change the numSeats of a non-existent
#table does NOT break the program
/*CALL setTableNumSeats('-1', '22');*/

#----------------------------------------------------#
#	Function getTableNumSeats test cases		  	 #
#		Should return the numSeats using			 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the numSeats
#of non-existent party does NOT break the program
/*SELECT getTableNumSeats((SELECT tableID FROM `Table` LIMIT 1)) as numSeats;
SELECT getTableNumSeats('-1') as numSeats;*/

# ------------------------------------------------------------------------------------------#
#					Test Cases for Party													#
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Procedure createNewParty test case				  #
#		Should create a new Party					  #
# ----------------------------------------------------#
/*
#Call createNewParty with all info and display it
CALL createNewParty('12', (SELECT tableID FROM `Table` LIMIT 1));
CALL createNewParty('4', (SELECT tableID FROM `Table` LIMIT 1,1));
CALL createNewParty('4', (SELECT tableID FROM `Table` LIMIT 2,1));
CALL createNewParty('3', (SELECT tableID FROM `Table` LIMIT 3,1));
CALL createNewParty('1', (SELECT tableID FROM `Table` LIMIT 4,1));
CALL createNewParty('7', (SELECT tableID FROM `Table` LIMIT 5,1));
CALL createNewParty('4', (SELECT tableID FROM `Table` LIMIT 6,1));
CALL createNewParty('10', (SELECT tableID FROM `Table` LIMIT 7,1));
CALL createNewParty('3', (SELECT tableID FROM `Table` LIMIT 8,1));
CALL createNewParty('3', (SELECT tableID FROM `Table` LIMIT 9,1));
SELECT * FROM Party;

# ----------------------------------------------------#
#	Procedure deleteParty test cases				  #
#		Deletes the party based on the PK			  #
# ----------------------------------------------------#

#Call deleteParty using primary key and display all info in Party
/*CALL deleteParty((SELECT partyID FROM Party LIMIT 1));
SELECT * FROM Party;*/

#MARK!!!!!: Trying to delete info for a Party that does not exist 
#does NOT break the program
/*CALL deleteParty('-1');
SELECT * FROM Party;*/

# ----------------------------------------------------#
#	Procedure setPartyPartySize test cases		  	  #
#		Should update a party party size based on	  #
#		primary key									  #
# ----------------------------------------------------#

#Call setPartyPartySize using primary key and new size
/*CALL setPartyPartySize((SELECT partyID FROM Party LIMIT 1), '10');*/

#Display data from `Party`, should have new size for first party
/*SELECT * FROM Party;*/

#MARK!!!!!: Trying to change the size of a non-existent
#party does NOT break the program
/*CALL setPartyPartySize('-2', '22');*/

#----------------------------------------------------#
#	Function getPartyPartySize test cases		  	 #
#		Should return the party size using			 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the size
#of non-existent party does NOT break the program
/*SELECT getPartyPartySize((SELECT partyID FROM Party LIMIT 1)) as size;
SELECT getPartyPartySize('-1') as size;*/

# ------------------------------------------------------------------------------------------#
#					Test Cases for Staff													#
# ------------------------------------------------------------------------------------------#

# ----------------------------------------------------#
#	Procedure createStaff test case					  #
#		Should create a staff member				  #
# ----------------------------------------------------#
/*
#Call createNewMenuItem with all info and display it
CALL createNewStaff('McWhirter', 'Austin', '2 years', '1234567890', 'McWhirterAI@gmail.com', 'Shift 3', '$7.50/hr', (SELECT jobID FROM Job LIMIT 1), NULL, (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewStaff('Fortier', 'Kris', '1 year', '8773649924', 'KristmasCameEarly@gmail.com', 'Shift 3', '$7.50/hr',  (SELECT jobID FROM Job LIMIT 1,1), (SELECT sectionID FROM Section LIMIT 1), (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewStaff('Johnson', 'Graham', '8 years', '4857725933', 'IAmGraham@gmail.com', 'Shift 1', '$12.85/hr',  (SELECT jobID FROM Job LIMIT 2,1), NULL, (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewStaff('Askew', 'Casen', '0.5 years', '9458490043', 'CasenPoint@gmail.com', 'Shift 2', '$9.00/hr',  (SELECT jobID FROM Job LIMIT 3,1), NULL, (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewStaff('Kohn', 'Daniel', '0 years', '4411845743', 'dannyboy17@gmail.com', 'Shift 4', '$5.50/hr',  (SELECT jobID FROM Job LIMIT 4,1), NULL, (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewStaff('Turley', 'Garrett', '17 years', '7777777777', 'garrett.turley@gmail.com', 'Shift 12', '$18.00/hr',  (SELECT jobID FROM Job LIMIT 5,1), NULL, (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewStaff('Born', 'Casey', '1.5 years', '3958377483', 'Born2Ride@gmail.com', 'Shift 6', '$7.50/hr',  (SELECT jobID FROM Job LIMIT 6,1), NULL, (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewStaff('Ververis', 'Alex', '69 years', '6969696969', 'BBV@gmail.com', 'Shift 69', '$69.69/hr',  (SELECT jobID FROM Job LIMIT 7,1), (SELECT sectionID FROM Section LIMIT 1,1), (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewStaff('Fulton', 'Perry', '1.7 years', '4572480782', 'PlatupusLov3r@gmail.com', 'Shift 1', '$8.23/hr',  (SELECT jobID FROM Job LIMIT 8,1), NULL, (SELECT restID FROM Restaurant LIMIT 1));
CALL createNewStaff('Sparkman', 'Colonel', '19 years', '5478902454', 'THECol@gmail.com', 'Shift 0', '$20.50/hr',  (SELECT jobID FROM Job LIMIT 9,1), NULL, (SELECT restID FROM Restaurant LIMIT 1));
SELECT * FROM Staff;

# ----------------------------------------------------#
#	Procedure deleteStaff test cases				  #
#		Deletes the MenuItem based on the PK		  #
# ----------------------------------------------------#

#Call deleteStaff using primary key and display all info in table
/*CALL deleteStaff((SELECT staffID FROM Staff LIMIT 1));
SELECT * FROM Staff;*/

#MARK!!!!!: Trying to delete info for a staff member that does not exist 
#does NOT break the program
/*CALL deleteStaff('-1');
SELECT * FROM Staff;*/

# ----------------------------------------------------#
#	Procedure setStaffLName test cases			  	  #
#		Should update a staff member first name based #
#		on primary key								  #
# ----------------------------------------------------#

#Call setStaff using primary key and new staff FName
/*CALL setStaffLName((SELECT staffID FROM Staff LIMIT 1), 'new name');*/

#Display data from `Staff`, should have new name for 'McWhirter'
/*SELECT * FROM Staff;*/

#MARK!!!!!: Trying to change the last name of a non-existent
#staff member does NOT break the program
/*CALL setStaffLName('-1', 'OhNo!');*/

#----------------------------------------------------#
#	Function getStaffLName test cases			  	 #
#		Should return the staff member last name 	 #
#		using primary keys							 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the last name
#of non-existent staff member item does NOT break the program
/*SELECT getStaffLName((SELECT staffID FROM Staff LIMIT 1)) as LName;
SELECT getStaffLName('-1') as LName;*/

# ----------------------------------------------------#
#	Procedure setStaffFName test cases			  	  #
#		Should update a staff member first name based #
#		on primary key								  #
# ----------------------------------------------------#

#Call setStaffFName using primary key and new staff FName
/*CALL setStaffFName((SELECT staffID FROM Staff LIMIT 1), 'another new name');*/

#Display data from `Staff`, should have new name for first name
/*SELECT * FROM Staff;*/

#MARK!!!!!: Trying to change the FName of a non-existent
#staff member does NOT break the program
/*CALL setStaffFName('-1', 'Glenn');*/

#----------------------------------------------------#
#	Function getStaffFName test cases			  	 #
#		Should return the staff member first name	 #
#		using primary keys							 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the first name
#of non-existent staff member returns NULL
/*SELECT getStaffFName((SELECT staffID FROM Staff LIMIT 1)) as FName;
SELECT getMenuItemPrice('-1') as FName;*/

# ----------------------------------------------------#
#	Procedure setStaffXP test cases					  #
#		Should update a staff member's experience	  #
#		based on primary key						  #
# ----------------------------------------------------#

#Call setStaffXP using primary key and new staff XP
/*CALL setStaffXP((SELECT staffID FROM Staff LIMIT 1), '.00001 year');*/

#Display data from `Staff`, should have new XP for first staff member
/*SELECT * FROM Staff;*/

#MARK!!!!!: Trying to change the XP of a non-existent
#staff member does NOT break the program
/*CALL setStaffXP('-1', 'lots of years');*/

#----------------------------------------------------#
#	Function getStaffXP test cases				  	 #
#		Should return the staff member XP using		 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the XP
#of non-existent staff member returns NULL
/*SELECT getStaffXP((SELECT staffID FROM Staff LIMIT 1)) as XP;
SELECT getStaffXP('-1') as XP;*/

# ----------------------------------------------------#
#	Procedure setStaffPhone test cases	  	 		  #
#		Should update a staff member phone	 		  #
#		number based on primary key					  #
# ----------------------------------------------------#

#Call setStaffPhone using primary key and new phone number
/*CALL setStaffPhone((SELECT staffID FROM Staff LIMIT 1), '5867477111');*/

#Display data from `Staff`, should have new phone for first staff member
/*SELECT * FROM Staff;*/

#MARK!!!!!: Trying to change the phone number of a non-existent
#staff member does NOT break the program
/*CALL setStaffPhone('-1', '7198675309');*/

#----------------------------------------------------#
#	Function getStaffPhone test cases			  	 #
#		Should return the phone number of staff 	 #
#		member using primary keys					 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the phone number
#of non-existent staff member returns NULL
/*SELECT getStaffPhone((SELECT staffID FROM Staff LIMIT 1)) as phone;
SELECT getStaffPhone('-1') as phone;*/

# ----------------------------------------------------#
#	Procedure setStaffEmail test cases	  	 		  #
#		Should update a staff email address	 		  #
#		number based on primary key					  #
# ----------------------------------------------------#

#Call setStaffEmail using primary key and new email address
/*CALL setStaffEmail((SELECT staffID FROM Staff LIMIT 1), 'newOne@gmail.com');*/

#Display data from `Staff`, should have new phone for first staff member
/*SELECT * FROM Staff;*/

#MARK!!!!!: Trying to change the email of a non-existent
#staff member does NOT break the program
/*CALL setStaffPhone('-1', 'newBroken?@gmail.com');*/

#----------------------------------------------------#
#	Function getStaffEmail test cases			  	 #
#		Should return the email address of staff 	 #
#		member using primary keys					 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the email address
#of non-existent staff member returns NULL
/*SELECT getStaffEmail((SELECT staffID FROM Staff LIMIT 1)) as email;
SELECT getStaffEmail('-1') as email;*/

# ----------------------------------------------------#
#	Procedure setStaffShift test cases	  	 		  #
#		Should update a staff member shift	 		  #
#		based on primary key						  #
# ----------------------------------------------------#

#Call setStaffShift using primary key and new shift
/*CALL setStaffShift((SELECT staffID FROM Staff LIMIT 1), 'Shift 0.5');*/

#Display data from `Staff`, should have new shift for first staff member
/*SELECT * FROM Staff;*/

#MARK!!!!!: Trying to change the shift of a non-existent
#staff member does NOT break the program
/*CALL setStaffShift('-1', 'should not work');*/

#----------------------------------------------------#
#	Function getStaffShift test cases			  	 #
#		Should return the staff member shift using	 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the shift
#of non-existent staff member returns NULL
/*SELECT getStaffShift((SELECT staffID FROM Staff LIMIT 1)) as shift;
SELECT getStaffShift('-1') as shift;*/

# ----------------------------------------------------#
#	Procedure setStaffPay test cases			  	  #
#		Should update a staff members pay			  #
#		based on primary key						  #
# ----------------------------------------------------#

#Call setStaffPay using primary key and new staff member pay
/*CALL setStaffPay((SELECT staffID FROM Staff LIMIT 1), '$1.79/hr');*/

#Display data from `Staff`, should have new pay for first staff member
/*SELECT * FROM Staff;*/

#MARK!!!!!: Trying to change the pay of a non-existent
#staff member does NOT break the program
/*CALL setStaffPay('-1', 'many dollars');*/

#----------------------------------------------------#
#	Function getStaffPay test cases				  	 #
#		Should return the staff member pay using	 #
#		primary keys								 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the pay
#of non-existent staff member returns NULL
/*SELECT getStaffPay((SELECT staffID FROM Staff LIMIT 1)) as pay;
SELECT getStaffPay('-1') as pay;*/

# ------------------------------------------------------------------------------------------#
#					Test Cases for Ticket													#
# ------------------------------------------------------------------------------------------#

# --------------------------------------------------------------#
#	Procedure newTicket test cases					  		 	#
#		Should create a new ticket							  	#
# --------------------------------------------------------------#
/*
#Call createNewTicket with all info and display it
CALL createNewTicket((SELECT partyID FROM Party LIMIT 1));
CALL createNewTicket((SELECT partyID FROM Party LIMIT 1,1));
CALL createNewTicket((SELECT partyID FROM Party LIMIT 2,1));
CALL createNewTicket((SELECT partyID FROM Party LIMIT 3,1));
CALL createNewTicket((SELECT partyID FROM Party LIMIT 4,1));
CALL createNewTicket((SELECT partyID FROM Party LIMIT 5,1));
CALL createNewTicket((SELECT partyID FROM Party LIMIT 6,1));
CALL createNewTicket((SELECT partyID FROM Party LIMIT 7,1));
CALL createNewTicket((SELECT partyID FROM Party LIMIT 8,1));
CALL createNewTicket((SELECT partyID FROM Party LIMIT 9,1));
SELECT * FROM Ticket;

# ----------------------------------------------------#
#	Procedure deleteTicket test cases				  #
#		Deletes the ticket based on the PK			  #
# ----------------------------------------------------#

#Call deleteTicket using primary key and display all info in table
/*CALL deleteTicket((SELECT ticketID FROM Ticket LIMIT 1));
SELECT * FROM Ticket;*/

#MARK!!!!!: Trying to delete info for a Ticket that does not exist 
#does NOT break the program
/*CALL deleteTicket('-1');
SELECT * FROM Ticket;*/

# ----------------------------------------------------#
#	Procedure setTicketETime test cases			   	  #
#		Should change existing ticket end time		  #
# ----------------------------------------------------#

#Call setTicketETime using primary key and current time
/*CALL setTicketETime((SELECT ticketID FROM Ticket LIMIT 1));*/

#Display data from Ticket, should have new end time for first item
/*SELECT * FROM Ticket;*/

#MARK!!!!!: Trying to change the end time of a non-existent
#ticket does NOT break the program
/*CALL setTicketETime('-1');*/

#----------------------------------------------------#
#	Function getTicketETime test cases		 		 #
#		Should return the ticket end time 			 #
#		using primary keys							 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the end time
#of non-existent ticket returns NULL
/*SELECT getTicketETime((SELECT ticketID FROM Ticket LIMIT 1)) as endTime;
SELECT getTicketETime('-1') as endTime;*/

#----------------------------------------------------#
#	Function getTicketSTime test cases		 		 #
#		Should return the ticket start time 		 #
#		using primary keys							 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the start time
#of non-existent ticket returns NULL
/*SELECT getTicketSTime((SELECT ticketID FROM Ticket LIMIT 1)) as startTime;
SELECT getTicketSTime('-1') as startTime;*/

# ----------------------------------------------------#
#	Procedure setTicketActualTime test cases	   	  #
#		Should change existing ticket actual time	  #
# ----------------------------------------------------#

#Call setTicketActualTime using primary key
/*CALL setTicketActualTime((SELECT ticketID FROM Ticket LIMIT 1));*/

#Display data from Ticket, should have new actual time for first item
/*SELECT * FROM Ticket;*/

#MARK!!!!!: Trying to change the actual time of a non-existent
#ticket does NOT break the program
/*CALL setTicketETime('-1');*/

#----------------------------------------------------#
#	Function getTicketActualTime test cases	 		 #
#		Should return the ticket actual time 		 #
#		using primary keys							 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the actual time
#of non-existent ticket returns NULL
/*SELECT getTicketActualTime((SELECT ticketID FROM Ticket LIMIT 1)) as ActualTime;
SELECT getTicketActualTime('-1') as ActualTime;*/

#----------------------------------------------------#
#	Function getTicketPredictedTime test cases	 	 #
#		Should return the ticket predicted time		 #
#		using primary keys							 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the actual time
#of non-existent ticket returns NULL
/*SELECT getTicketPredictedTime((SELECT ticketID FROM Ticket LIMIT 1)) as PredictedTime;
SELECT getTicketPredictedTime('-1') as PredictedTime;*/

# ------------------------------------------------------------------------------------------#
#					Test Cases for OrderedItem												#
# ------------------------------------------------------------------------------------------#

# --------------------------------------------------------------#
#	Procedure newOrderedItem test cases				  		 	#
#		Should create new ordered item using ticket number		#
#		and menu item number									#
# --------------------------------------------------------------#
/*
#Call createNewOrderedItem using PKs from Ticket and MenuItem; display it
CALL createNewOrderedItem((SELECT menuItemID FROM MenuItem LIMIT 1), (SELECT ticketID FROM Ticket LIMIT 1), (SELECT staffID FROM Staff LIMIT 1));
CALL createNewOrderedItem((SELECT menuItemID FROM MenuItem LIMIT 1,1), (SELECT ticketID FROM Ticket LIMIT 1), (SELECT staffID FROM Staff LIMIT 1));
CALL createNewOrderedItem((SELECT menuItemID FROM MenuItem LIMIT 2,1), (SELECT ticketID FROM Ticket LIMIT 1), (SELECT staffID FROM Staff LIMIT 1));
CALL createNewOrderedItem((SELECT menuItemID FROM MenuItem LIMIT 3,1), (SELECT ticketID FROM Ticket LIMIT 1), (SELECT staffID FROM Staff LIMIT 1));
CALL createNewOrderedItem((SELECT menuItemID FROM MenuItem LIMIT 4,1), (SELECT ticketID FROM Ticket LIMIT 1), (SELECT staffID FROM Staff LIMIT 1));
CALL createNewOrderedItem((SELECT menuItemID FROM MenuItem LIMIT 5,1), (SELECT ticketID FROM Ticket LIMIT 1), (SELECT staffID FROM Staff LIMIT 1));
CALL createNewOrderedItem((SELECT menuItemID FROM MenuItem LIMIT 6,1), (SELECT ticketID FROM Ticket LIMIT 1), (SELECT staffID FROM Staff LIMIT 1));
CALL createNewOrderedItem((SELECT menuItemID FROM MenuItem LIMIT 7,1), (SELECT ticketID FROM Ticket LIMIT 1), (SELECT staffID FROM Staff LIMIT 1));
CALL createNewOrderedItem((SELECT menuItemID FROM MenuItem LIMIT 8,1), (SELECT ticketID FROM Ticket LIMIT 1), (SELECT staffID FROM Staff LIMIT 1));
CALL createNewOrderedItem((SELECT menuItemID FROM MenuItem LIMIT 9,1), (SELECT ticketID FROM Ticket LIMIT 1), (SELECT staffID FROM Staff LIMIT 1));
CALL createNewOrderedItem((SELECT menuItemID FROM MenuItem LIMIT 9,1), (SELECT ticketID FROM Ticket LIMIT 1), (SELECT staffID FROM Staff LIMIT 1));
SELECT * FROM OrderedItem;

# ----------------------------------------------------#
#	Procedure deleteOrderedItem test cases			  #
#		Deletes the ordered item based on the PK	  #
# ----------------------------------------------------#

#Call deleteOrderedItem using primary key and display all info in table
/*CALL deleteOrderedItem((SELECT orderedItemID FROM orderedItem LIMIT 1));
SELECT * FROM orderedItem;*/

#MARK!!!!!: Trying to delete info for an ordered item that does not exist 
#does NOT break the program
/*CALL deleteOrderedItem('0042');
SELECT * FROM orderedItem;*/

#----------------------------------------------------#
#	Function getOrderedItemMenuItemName test cases 	 #
#		Should return the menu item name 			 #
#		based on the ordered item PK				 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the menu item name
#of non-existent ordered item returns NULL
/*SELECT getOrderedItemMenuItemName((SELECT MenuItem_menuItemID FROM orderedItem LIMIT 1)) as MenuItemName;
SELECT getOrderedItemMenuItemName('-1') as MenuItemName;*/

#----------------------------------------------------#
#	Function getOrderedItemOrderTime test cases		 #
#		Should return the order time of ordered item #
#		using primary key							 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the order time
#of non-existent ordered item returns NULL
/*SELECT getOrderedItemOrderTime((SELECT orderedItemID FROM orderedItem LIMIT 1)) as orderTime;
SELECT getOrderedItemOrderTime('-1') as orderTime;*/

# ----------------------------------------------------#
#	Procedure setOrderedItemDeliverTime test cases	  #
#		Should update an ordered item deliver time    #
#		based on primary key						  #
# ----------------------------------------------------#

#Call setOrderedItemDeliverTime using primary key
/*CALL setOrderedItemDeliverTime((SELECT orderedItemID FROM orderedItem LIMIT 1));*/

#Display data from orderedItem, should have new deliver time for first item
/*SELECT * FROM orderedItem;*/

#MARK!!!!!: Trying to change the deliver time of a non-existent
#ordered item does NOT break the program
/*CALL setOrderedItemDeliverTime('-1');*/

#----------------------------------------------------#
#	Function getOrderedItemDeliverTime test cases	 #
#		Should return the ordered item deliver time	 #
#		using primary keys							 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the deliver time
#of non-existent ordered item returns NULL
/*SELECT getOrderedItemDeliverTime((SELECT orderedItemID FROM orderedItem LIMIT 1)) as deliverTime;
SELECT getOrderedItemDeliverTime('-1') as deliverTime;*/

# ----------------------------------------------------#
#	Procedure setOrderedItemCookSTime test cases	  #
#		Should update an ordered item cook start time #
#		based on primary key						  #
# ----------------------------------------------------#

#Call setOrderedItemCookSTime using primary key
/*CALL setOrderedItemCookSTime((SELECT orderedItemID FROM orderedItem LIMIT 1));*/

#Display data from orderedItem, should have new cook start time for first item
/*SELECT * FROM orderedItem;*/

#MARK!!!!!: Trying to change the deliver time of a non-existent
#ordered item does NOT break the program
/*CALL setOrderedItemCookSTime('-1');*/

#----------------------------------------------------#
#	Function getOrderedItemCookSTime test cases		 #
#		Should return the ordered item cook starttime#
#		using primary keys							 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the cook start time
#of non-existent ordered item returns NULL
/*SELECT getOrderedItemCookSTime((SELECT orderedItemID FROM orderedItem LIMIT 1)) as cookSTime;
SELECT getOrderedItemCookSTime('0042') as cookSTime;*/

# ----------------------------------------------------#
#	Procedure setOrderedItemCookETime test cases	  #
#		Should update an ordered item cook end time	  #
#		based on primary key						  #
# ----------------------------------------------------#

#Call setOrderedItemCookETime using primary key
/*CALL setOrderedItemCookETime((SELECT orderedItemID FROM orderedItem LIMIT 1));*/

#Display data from orderedItem, should have new cook end time for first item
/*SELECT * FROM orderedItem;*/

#MARK!!!!!: Trying to change the cook end time of a non-existent
#ordered item does NOT break the program
/*CALL setOrderedItemCookETime('-1');*/

#----------------------------------------------------#
#	Function getOrderedItemCookETime test cases		 #
#		Should return the ordered item cook end time #
#		using primary keys							 #
#----------------------------------------------------#

#Call the function using the primary key;
#MARK!!!!!: note that getting the cook end time
#of non-existent ordered item returns NULL
/*SELECT getOrderedItemCookETime((SELECT orderedItemID FROM orderedItem LIMIT 1)) as cookETime;
SELECT getOrderedItemCookETime('-1') as cookETime;*/
