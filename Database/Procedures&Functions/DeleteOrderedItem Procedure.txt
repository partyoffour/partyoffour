# ----------------------------------------------------#

#   Procedure deleteOrderedItem			      #

#	Delete existing ordered item		      #

# ----------------------------------------------------#


DROP PROCEDURE IF EXISTS deleteOrderedItem;

CREATE PROCEDURE deleteOrderedItem(OrderedItem INT)

	DELETE FROM OrderedItem WHERE `OrderedItemID` = OrderedItem;