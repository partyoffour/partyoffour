﻿# Party Of Four #


### Pitch ###
* Walking into a restaurant on a Friday night, you’re greeted by a smiling 16 year old hostess. She takes your name and your party size, looks at her map of the restaurant, and, based on her own experience, decides where your party should be seated.

* This familiar scene is one that plays out every moment of every day in nearly every restaurant across America, but this tried and true method of operation has and will always have one major flaw: with it, restaurant owners place the efficiency of their businesses on the shoulders of inexperienced, low-wage workers.

* PartyOfFour was designed with this inefficiency in mind. PartyOfFour is a tablet-based managerial system that utilizes artificial intelligence to take the guesswork out of the restaurant industry. With optimal seating suggestions, accurate wait time predictions, and real-time analysis of restaurant data, PartyOfFour minimizes human error while empowering restaurant staff, ultimately increasing restaurant efficiency and net profits.

### Requirements ###

* Registration
    * Registration Screen
        * Registration Screen Confirmation
    * Forgotten Password/Username
* Menu Screen


### Current Work ###

* Work

### Future Considerations ###

* Future 1