#!/usr/bin/python
import NN_library
import DB_library
import datetime
import web_callback
import time
from sys import exit as ex
import threading
import socket
from urllib.parse import urlparse
import datetime

DEBUG = True

def server_control(DB):
    
    # Continuous loop to keep the server running
    while(1):
        restart = threading.Event() # Mutex to signal the server to restart and use a new Neural Network object
        server_thread = threading.Thread(target=server,args=(restart,DB)) # Start the server that will serve predictions
        server_thread.start()
        currDay = datetime.datetime.today().day # currDay and checkDay are used to compare if a new day has passed
        checkDay = datetime.datetime.today().day
        while currDay == checkDay and not DEBUG: # Wait in intervals of 1 hour to see if there is a new day
            print("Waiting 1 hour")
            time.sleep(3600)
            checkDay = datetime.datetime.today().day
        
        try: # After a new day has passed
            DB.createData() # Create a new data set with all the data from the previous day (Saved as CurrentData.csv, a backup is created)
            Net = NN_library.PBrain(2,1) # Create a new Neural Network object
            Net.defineServerData()

            try:
                Net.train(50,5000) # Train the new Neural Net object with the new data - limit to 5000 epochs to ensure that the server is not down for too long
                Net.save() # Save as Current.pkl for the server to use
                #print(Net)
                restart.set() # Signal the server to restart
                server_thread.join() # Wait for the server to close
                
                del Net # Delete the objects we no longer need
                del server_thread
                del restart

                if DEBUG:
                    print("Exiting")
                    ex(0)
            except:
                print( "Failed to train the neural network.")
        except:
            print("Creating the dataset failed")
 
        if DEBUG:
            print("Exiting")
            ex(0)

def server(event,DB):
    HOST = 'localhost'
    PORT = 2017 

    try:
        net = NN_library.NNload(Op=True) # Load the Current.pkl file which should contain a trained Neural Net object
    except:
        print("Loading the Neural Network failed. Make sure you have a Current.pkl file")
        ex(0)

    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM) # custom, light weight socket server

    try:
        s.bind((HOST,PORT)) # bind to localhost:2017
        print("Server bound to " +HOST+":" + str(PORT)+"\n")
        print("Server using: \n"+str(net)) # Shows the current Neural Net
    except:
        print("Failed to bind")
        ex(0)
    
    s.listen(10) # Listen for connections
    while(1 and not event.isSet()): # handles closing the loop if the mutex is set
        conn,addr = s.accept()
        
        if addr[0] != "127.0.0.1": # We only want to accept connections from our localhost
            response = ("HTTP/1.1 403 Access Denied\n"+"Content-Type: text/html\n"
             +"\n"
             +"<h1>Access Denied</h1>")
            conn.send(bytes(response,'utf8'))

        else:
            
            lines = conn.recv(1024) # Recieve the GET request
            output = urlparse(str(lines)) 
            params = parse_qs(output.query[0:output.query.index(' ')]) # Parse out the parameters

            val1 = params.get('var1')
            val2 = params.get('var2')
            val3 = params.get('var3')
            if None in (val1,val2,val3): # make sure all of the values exist
                    predict = "Invalid Inputs"
            else:
                    values = val1[0]+","+val2[0]+","+val3[0] # create the list that the Neural Net expects for prediction
                    predict = net.predict(values)
                    lastTicketTime = DB.getSTime()
                    
                    now = datetime.datetime.now()
                    time = now.hour,now.minute,now.second
                    timePassed = (time[0]-lastTicketTime[0])*60 + (time[1]-lastTicketTime[1]) + float(time[2]-lastTicketTime[2])/float(60)
                    
            try:
                    response = ("HTTP/1.1 200 OK\n"+"Content-Type: text/html\n"
                     +"\n"
                     +str(round(float(predict)-timePassed,2)))
            except:
                    response = ("HTTP/1.1 200 OK\n"+"Content-Type: text/html\n"
                     +"\n"
                     +str(predict))

        conn.close()

        if DEBUG:
            break
    print("unbinding server")
    s.close()
    return

def main():
    print("Connecting to the Database")
    try:
        DB = DB_library.PBrainDB('localhost','root') # If we can't connect to the database we cannot update the Nueral Network
    except:
        print("Failed to connect to DB")
        ex(0)
    print("Connection successful") 
    try:
        server_control(DB)
    except Exception as e:
        print("The server C2 failed to work correctly")
        print(e)   
        ex(0)

if __name__ == '__main__':
    main()

