#!/usr/bin/python
import tabulate
import MySQLdb as mdb
from shutil import copyfile



class PBrainDB:

	def __init__(self,host,username):
		self.host = host
		self.username=username
		with open('password','r') as passwd:
			try:
				con = mdb.connect(host,username,passwd.readline(),'party_of_four_restaurant')
				self.DBobj = con.cursor()
			except:
				raise
			

	def printTickets(self):
		'''Prints all current open Tickets in the database
		Input: Database Object
		'''
		
		self.DBobj.execute("select * from Ticket")
		desc = self.DBobj.description
		for i in self.DBobj.description:
			print(i[0], end='\t')
		print('\n')
		for i in self.DBobj.fetchall():
			print(i[0],'\t',i[1].strftime("%Y-%m-%d"),'\t',i[2].strftime("%Y-%m-%d"))

	def printOrders(self):
		'''Prints all current orders in the database with relevant information
		Input: Database Object
		Output: Query Results
		'''
		    
		self.DBobj.execute("select t.*, o.orderTime,o.deliverTime,o.cookSTime,o.cookETime,m.itemEatTime,m.itemDeliverTime,m.itemCookTime,m.itemName from Ticket t inner join OrderedItem o on t.ticketID=o.Ticket_ticketID inner join menuItem m on o.MenuItem_menuItemID=m.menuItemID")
		headers=[text[0] for text in self.DBobj.description]
		formattedData = []
		    
		for i in self.DBobj.fetchall():
		    if None not in i:
			    data = ( i[0],i[1].strftime("%H:%M:%S"),i[2].strftime("%H:%M:%S"),i[3].strftime("%H:%M:%S"),i[4].strftime("%H:%M:%S"),i[5].strftime("%H:%M:%S"),i[6].strftime("%H:%M:%S"),self.timedelta_to_str(i[7]),self.timedelta_to_str(i[8]),self.timedelta_to_str(i[9]),i[10])
			    formattedData.append(data)
			    
		print(tabulate.tabulate(formattedData,headers=headers))
		return formattedData

	def getMaxCookTime(self,ticketID):
		'''Prints the maximum cook time out of all items on a ticket
		Input: Database Object, TicketID
		Output: Max cook time
		'''
		
		self.DBobj.execute('select max(i.itemCookTime) from OrderedItem o inner join MenuItem i on o.orderedItemID = i.menuItemID where o.Ticket_ticketID = ' + str(ticketID))
		result = self.timedelta_to_str(self.DBobj.fetchone()[0])
		print(result)
		return result

	def timedelta_to_str(self,obj):
	    if obj is not None:
		    seconds = int(obj.seconds) % 60
		    minutes = int(obj.seconds / 60) % 60
		    hours = int(obj.seconds / 3600) % 24
		    return '%02d:%02d' % (minutes, seconds)
	    else:
		    print("Cannot format None as time")

	def getMenuItemDeliverTime(self,item):
		'''Prints the time it takes to deliver an item
		Input: Database Object, item name
		Output: Delivery time
		'''
		
		self.DBobj.execute('Select itemDeliverTime from MenuItem where itemName like \'%'+item+'%\' limit 1')
		result= self.DBobj.fetchall()[0][0]
		print(result)
		return result

	def getCountItems(self, ticketID):
		'''Prints the number of ordered items on a ticket
		Input: Database Object, TicketID
		Output: Query result
		'''
		
		self.DBobj.execute('select max(i.itemCookTime) from OrderedItem o inner join MenuItem i on o.orderedItemID = i.menuItemID where o.Ticket_ticketID = '+str(ticketID))
		result= self.DBobj.fetchall()[0][0]
		print(result)
		return result

	def getMaxEatTime(self,ticketID):
		'''Prints the maximum predicted time a to eat all items on a ticket
		Input: Database Object, TicketID
		Output: Max eat time
		'''
		
		self.DBobj.execute('select max(i.itemEatTime) from OrderedItem o inner join MenuItem i on o.orderedItemID = i.menuItemID where o.Ticket_ticketID = ' + str(ticketID))
		result = self.timedelta_to_str(self.DBobj.fetchone()[0])
		print(result)
		return reversed

	def customQuery(self,query):
		'''Allows custom queries to the database ***SECURITY CONCERN - MAKE SURE ALL QUERIES ARE PROPERLY SANITIZED*** '''
		result = self.DBobj.execute(query)
		return result
	

	def createData(self):
		'''If we're creating a dataset for the server make sure we have a backup'''
		try:
			copyfile('CurrentData.csv','DataBackup.csv')
		except:
			print("Failed to copy CurrentData to DataBackup")
		self.DBobj.execute('call getData();')
		data = self.DBobj.fetchall()
		self.formatData(data)
		

	def formatData(self,data):
		
		with open('CurrentData.csv','w') as curFile:
			curFile.write('LongestCookTime,PartySize,TicketActualTime\n')
			for i in data:
				if i[5] is None:
					if i[3] is None:
						curFile.write('None,'+str(i[4])+',1085.313\n')
					else:
						curFile.write(str(i[3].total_seconds())+','+str(i[4])+',1085.313\n')
				else:
					curFile.write(self.timedelta_to_str(i[3])+','+str(i[4])+','+str(i[5].total_seconds())+'\n')
		
	def addPrediction(self,ticketId,predictedTime):
		'''Add prediction to the database for supervised dataset'''
		self.DBobj.execute('update Ticket set `ticketPredictedTime` = '+predictedTime+' where `ticketId`= ' + str(ticketId)+';')

	def getSTime(self):

                
                #results = self.DBobj.execute('Select * from Ticket')
                results = self.DBobj.execute('Select ticketSTime from Ticket where ticketETime = ticketSTime order by ticketSTime limit 1')
                if results > 0:
                        #print()
                        time =  self.DBobj.fetchall()[0][0]
                        return time.hour,time.minute,time.second
                else:
                        return [0]
                
		#self.DBobj.execute('Select ticketSTime from Ticket where ticketETime is Null order by ticketSTime limit 1')
		
if __name__ == '__main__':


    print("library file")
