#!/usr/bin/python
import numpy as np
import pandas as pd
import math
import time
import pickle
import glob
import sys,os
import tabulate
from shutil import copyfile

from pybrain.tools.shortcuts import buildNetwork
from pybrain.structure import TanhLayer,SigmoidLayer,LinearLayer,FeedForwardNetwork,FullConnection
from pybrain.supervised.trainers import BackpropTrainer,RPropMinusTrainer
from pybrain.datasets import SupervisedDataSet
from pybrain.structure.modules import *
from pybrain.tools.neuralnets import NNregression,Trainer

DEBUG = False

class PBrain:
    '''Neural Network Class predefined with several methods to make defining your own custom Neural Network easier
'''
    def __init__(self,numInputs,numOutputs,hiddenLayers=100):
        self.numInputs = numInputs
        self.numOutputs = numOutputs
        self.hiddenLayers = hiddenLayers
        self.sourceHeaders = False
        self.server = False
        self.dataPoints = 0
        
        self.NN = buildNetwork(self.numInputs,
                   self.hiddenLayers, # number of hidden units
                   self.numOutputs,
                   bias = True, # Through testing, including a bias reduced error
                   hiddenclass = TanhLayer,
                   outclass = LinearLayer
                   )
        
        self.trained = False

    def __str__(self):
        return "Neural Net Properties:\n\tNumber of Inputs: " + str(self.numInputs) + "\n\tNumber of Outputs: " + str(self.numOutputs) + "\n\tNumber of Hidden Layers: " + str(self.hiddenLayers) +"\n\tData Source: " + self.source + "\n\tData Points: " + str(self.dataPoints) + "\n\tTrained: " + ("True" if self.trained else "False")

    def defineData(self,src = None):
        '''param: src - full path to a csv file with or without headers
       '''
        
    
        if DEBUG:
                    self.source = 'testdata/auto-mpg2.csv'
                    self.sourceHeaders=True
        else:
                    self.source = input("Input the path to your data source:\n")
                    headers = input("Does your data have headers? (Y/N):\n")
                    if (headers.lower() =='y'):
                        self.sourceHeaders = True
                    
        try:
                    self.buildData(self.source,header=self.sourceHeaders)
        except Exception as e:
                    print(e)
                    print("\nSomething went wrong loading the data\n")
                    sys.exit(0)

        print("\nNeural Network has been fully defined\n")

    def defineServerData(self):
        '''Automatically reads data in "Current.pkl" and sets the Neural Network into server mode which automates several tasks'''
    
        self.server=True
        self.sourceHeaders=True
        self.source = 'CurrentData.csv'
        self.buildData(self.source,header=self.sourceHeaders)
                
    def menu(self):
        '''Provides a user-input based menu for interacting with a Neural Network'''
        
        while(1):
            test=str(input("\n\nGive inputs for prediction or type train, save, delete or exit:\n"))

            if test.lower() == "train":
                data = input("Current data is from \"" + self.source + "\"\nDo you want to define a new data set? (Y/N)\n")
                if data[0].lower() == 'y':
                    source = input("Input the path to your data source:\n")
                    headers = input("Does your data have column headers? (Y/N)\n")
                    if headers[0].lower()=='y':
                        self.buildData(source,header=True)
                    else:
                        self.buildData(source,header=False)
                elif data[0].lower() == 'n':
                    self.train(30,1000,verbose=True)
            elif test.lower() == "save":
                print(self.save())
            elif test.lower()=="delete":
                self.delete()
            elif test.lower() == "exit" or test.lower()=="quit":
                sys.exit(0)
            else:
                try:
                        testvals = test.strip().split()
                        value = self.predict(testvals)[0]
                except Exception as e:
                    print("Something went wrong, try again.")
                    

        
    def predict(self,inputs):
        '''Param: predict - expects a comma separated list to use as inputs to predict values from the Neural Network'''
        
        if self.trained: # The Neural Network has to be trained to make predictions
            try:
                try:
                    inputs = inputs.split(",")
                except:
                    pass
                for i in inputs:
                    try:
                        i = int(i) # make sure all inputs are numbers
                    except:
                        return "Invalid Input"
                if len(inputs) == self.numInputs:
                    value = self.NN.activate([i for i in inputs])
                else:
                    return "Wrong number of inputs"
                
            except Exception as e:
                print(e)
            
            value =str(round(value[0],2))
            return value
        else:
            print("The Neural Network has not been trained. Please train before making predictions")
        
    def buildData(self, source, **kwargs):
        '''Builds data sets from csv files.
        Input: CSV file
        Keywords: header = true if the source data has header labels
        Output: Supervised Dataset'''
        try:
            header=bool(kwargs['header'])
        except:
            header=False
        
        
        if header:
            df = pd.read_csv(source,header=0)
            if not self.server:
                print("Data contains:\n")
                i = 1
                for j in df.columns.values:
                        print(str(i)+")  "+j)
                        i+=1
            numInputs=self.numInputs
            numOutputs = self.numOutputs
            if numInputs > len(df.columns.values) or numInputs < 1:
                print("Out of range")
                raise ValueError
            if numOutputs > len(df.columns.values) or numOutputs < 1:
                print("Out of range")
                raise ValueError
        else:
            df = pd.read_csv(source,header=None)

            # Create a 2D array for the tabulate library
            print("Sample Data:\n")
            i = 1
            table = []
            add = []
            for j in df.columns.values:
                    add.append("Column " +str(i)+"\t")
                    i+=1
            table.append(add)
            add = []
            for i in df.iloc[0]:
                add.append(i)
            table.append(add)
            
            print(tabulate.tabulate(table))
            
            numInputs=self.numInputs
            numOutputs =self.numOutputs
            if numInputs > len(df.iloc[0]) or numInputs < 1:
                print("Out of range")
                raise ValueError
            if numOutputs > len(df.iloc[0]) or numOutputs < 1:
                print("Out of range")
                raise ValueError

        self.ds=SupervisedDataSet(numInputs,numOutputs)

        sampleIndices = []
        targetIndices = []

        if not self.server: # If we're in server mode we want to automate input selection
            i=0    
            while i<numInputs:
                
                if header:
                    print("What would you like input " + str(i+1) + " to be?")
                    answer = input('Input ' + str(i+1)+': ')
                    if answer not in df.columns.values and int(answer) not in range(0,len(df.columns.values)):
                        print("Please pick a valid column")
                    else:
                        try:
                            sampleIndices.append(df.columns.get_loc(answer))

                        except:
                            sampleIndices.append(df.columns.get_loc(df.columns.values[int(answer)-1]))
                        i+=1
                else:
                    print("Which column would you like to use for input " + str(i+1) + "?")
                    answer = input('Input ' + str(i+1)+': ')
                    if int(answer) < 1 or int(answer)>len(df.iloc[0]):
                        print("Please pick a valid column")
                    else:
                        sampleIndices.append(int(answer)+1)
                        i+=1
                    
            print("\n\n*****OUTPUTS*****")
            i=0    
            while i<numOutputs:
                if header:
                    print("What would you like output " + str(i+1) + " to be?:\n")
                    answer = input('\nOutput ' + str(i+1)+': ')
                    if answer not in df.columns.values and int(answer) not in range(0,len(df.columns.values)):
                        print("Please pick a valid column")
                    else:
                        try:
                            targetIndices.append(df.columns.get_loc(answer))
                        except:
                            targetIndices.append(df.columns.get_loc(df.columns.values[int(answer)-1]))
                        i+=1
                else:
                    print("What would you like output " + str(i+1) + " to be?:\n")
                    answer = input('\nOutput ' + str(i+1)+': ')
                    if int(answer)<1 or int(answer)>len(df.iloc[0]):
                        print("Please pick a valid column")
                    else:
                        targetIndices.append(int(answer)+1)
                        i+=1
        else: # Statically define which data the Neural Network should use if in server mode
            sampleIndices.append(df.columns.get_loc('LongestCookTime'))
            sampleIndices.append(df.columns.get_loc('PartySize'))
            targetIndices.append(df.columns.get_loc('TicketActualTime'))
                
        samples = df.iloc[0:,sampleIndices]
        target = df.iloc[0:,targetIndices]

        for i in range(len(samples)): # Remnant of using incomplete data sets, but still useful for making sure no gaps are in the data
            if not '?' in str(samples.iloc[i]) and not 'None' in str(samples.iloc[i]):
                self.ds.addSample((samples.iloc[i]),(target.iloc[i]))
        self.dataPoints = len(self.ds)

        
    def train(self,continueEpochs=50,maxEpochs=1000,verbose=False):
        '''Trains the Neural Network
Input: continueEpochs - how many epochs should be use to ensure the data has actually converged
    maxEpochs - limit the number of epochs in case the data never converges
    verbose - complete list of errors'''
        self.trainer = RPropMinusTrainer(self.NN,dataset=self.ds,verbose=verbose,batchlearning=True)
        self.trainer.trainUntilConvergence(continueEpochs=continueEpochs,maxEpochs=maxEpochs)
        self.trained = True

    def save(self):
        '''Saves a Neural Network for future use.
        Input: Neural Network Object
        Output: pickle file
        '''
        filelist = glob.glob('./*.pkl')
        if self.server:
            filename = 'Current.pkl'
        else:
            
            filename = input("What filename would you like?:\n")
            if len(filename)> 25:
                print("Filename too long")
            
        try:
            filename=filename.split('.')[0].strip()
        except:
            pass

        for i in filename:
            if not i.isalnum():
                print("Filename contains invalid characters. Please only use alpha-numeric characters.")
                exit
        overwrite=''
        if (".\\"+filename+".pkl") in filelist:
            if self.server:
                copyfile('Current.pkl','Backup.pkl')
                overwrite = 'y'
            else:
                overwrite=input("File already exists, do you want to overwrite it? (Y/N)\n")
            if overwrite.lower()=='y':
                with open(filename+'.pkl', 'wb') as handle:
                  pickle.dump(self, handle)
                  return "Saved as: " + filename + ".pkl"
            else:
                return "File not saved"
        else:
            with open(filename+'.pkl', 'wb') as handle:
                  pickle.dump(self, handle)
                  return "Saved as: " + filename + ".pkl"


    def delete(self):
        '''Deletes a Neural Network pickle file.
        '''
        filelist = glob.glob('./*.pkl')
        print("Which Neural Net would you like to delete? (just type the number or \"exit\")")
        for i in range(len(filelist)):
            print(str(i+1)+') ' + filelist[i][2:],end='\n')
        load = ''
        while load=='':
            load= input()
            if load=="exit" or load=="quit":
                sys.exit(0)
            else:    
                try:
                    load=int(load)
                except ValueError:
                    print("Please type a number")
                    load=''
                if not (load=='') and (load <=0 or load>len(filelist)):
                    print("Please select a valid number")
                    load=''
                    continue
                try:
                    answer = ''
                    while answer=='':
                        answer = input("Are you sure you wish to delete " + filelist[load-1][2:]+" (Y/N)\n")
                        if answer.lower() == 'y':
                            os.remove(filelist[load-1])
                        else:
                            exit
                    
                except:
                    raise

def NNload(Op = False):
        '''Load a Neural Network from a file and return the object
        Output: Neural Net Object
        '''
        if(not Op):
            filelist = glob.glob('./*.pkl')
            print("Which Neural Net would you like to load? (just type the number or \"exit\")")
            for i in range(len(filelist)):
                print(str(i+1)+') ' + filelist[i][2:],end='\n')
            load = ''
            while load=='':
                load= input()
                if load=="exit" or load=="quit":
                    sys.exit(0)
                else:
                    try:
                        load=int(load)
                    except ValueError:
                        print("Please type a number")
                        load=''
                    if not(load=='') and (load <=0 or load>len(filelist)):
                        print("Please select a valid number!")
                        load =''
                        continue
                    try:
                        with open(filelist[load-1], 'rb') as handle:
                          PBrain=pickle.load(handle)
                    except:
                        print("error")
                        raise
        else:
            with open('C:\\Users\\C17Austin.McWhirter\\Desktop\\PartyOfFour\\Austin Test Code\\Current.pkl', 'rb') as handle:
                          PBrain=pickle.load(handle)
        
        return PBrain

def main():
    print("Library file")
    
if __name__ == "__main__":
    main()
